<?php

$id = $_GET['id'];

include 'id_check.php';


if (! $proceed ) {

	$file = 'error.txt';
	include 'template.php';
} else {

	$folder = "article_files/" . substr($id, 0,1) . '/' . substr($id, 1,1) .  '/' . $id . '/';


	$amount_lines = count(file("info.csv"));
	$fileopen = fopen('info.csv',"r");


	$line_found = False;
	for ($i=1; $i < $amount_lines+1; $i++) {
		$line_of_text = fgetcsv( $fileopen, 1024, "\t" );
		if ($id == $line_of_text[0]) {
			$line_found = True;
			$matched_line = $line_of_text;


		}

	}


	if (!$line_found) {
		$file = '404.php';

	}

	else {

		$namefile = $folder . 'title.txt';

		$handle = file_exists($namefile);

		if ($handle) {

			$name_file_open = fopen($namefile,"r");
			$name = fgets($name_file_open);
			fclose($name_file_open);

		}
		else {
		    $name = "undefined title";
		}

		
		$file = 'article_process.php';

	}
}


include 'template.php';

?>
