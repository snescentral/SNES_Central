<?php
	$outputhtml  = $outputhtml .  '<p class="name">' . 'Known Dumped prototypes' . '</p>';
	$outputhtml  = $outputhtml . "<p>This is listing of dumped prototypes that are documented on SNES Central and differ from the final version. To see undumped prototypes, or prototypes that are identical to final, please go to the specific game page. I'm including information here so that it may eventually be used for emulators such as Higan. This page also includes dumped unreleased games, but I recommend going to the unreleased games section for more information. </p>
<p>Here is a guide for the table below. For the \"dump status\", games that have been dumped by someone known are set as \"verified\", known hacked ROM images are set as \"hacked\", incomplete or damaged (bad) dumps are listed as such, and \"unverified\" means that I don't know the original dumper and cannot verify that the game has not been tampered with. Unfortunately, many prototypes were leaked many years ago on BBSes or on the early Internet and the original cart may be long gone. The board type is the PCB that should give the correct memory mapping for emulation. In general, I set it to be the same as the commercial release. Note that if you use incorrect memory mapping, the game may not work correctly if the internal header information is incorrect. For instance, if you try playing the prototype of <a href=\"review.php?id=0354&num=0&fancy=yes&article=proto\">The Flintstones: Treasure of Sierra Madrock</a> with an emulator, it will automatically map it to have a battery, which will trigger copy protection that makes the game nearly unplayable.</p>
<p>The SHA-256 hashes below were generated using <a href=\"http://nsrt.nachsoftware.org/files.php\">NSRT 3.4</a>. The hashes may differ from what you would get from other SHA256 generators, since NSRT heuristically determines the structure of the ROM image to account for copier headers, for instance. Sometimes there are false positives, which leads to a SHA256 that is different from a file agnostic program. In the future, I may decide to change to a more traditional SHA256 generator.";

	$filename = 'stuff/dumped_prototypes.txt';

	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">
	<tr class=\"row1\">
		<td width=\"150px\"><b>Prototype game name</b></td>
		<td width=\"150px\"><b>Final game name</b></td>
		<td width=\"50px\"><b>Region</b></td>
		<td width=\"100px\"><b>Board Type</b></td>
		<td width=\"50px\"><b>Dump status</b></td>
		<td width=\"150px\"><b>SHA256</b></td>
	</tr>

\n";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">
		<td><a href=\"" . $item[6] . "\">" . $item[0] . "</a></td>
		<td>" . $item[1] . "</td>
		<td>" . $item[2] . "</td>
		<td><a href=\"pcbboards.php?chip=" . $item[3] . "\">" . $item[3] . "</a></td>
		<td>" . $item[4] . "</td>
		<td><div style=\"width:150px; word-wrap:break-word; display:inline-block;\">" . $item[5] . "</div></td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";


	$file = "outputhtml.php";

	include 'template.php';

?>
