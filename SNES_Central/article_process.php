<?php

echo "<p class=\"headingtext\">$name</p>\n";

$title_file = $folder . "titles.html";
$handle = file_exists($title_file);


if ($handle) {
	include $title_file;
}

$rom_images_file = $folder . "rom_images.html";
$handle = file_exists($rom_images_file);


if ($handle) {
	include $rom_images_file;
}


$cart_file = $folder . "cart_table.html";
$handle = file_exists($cart_file);


if ($handle) {
	include $cart_file;
}

$box_file = $folder . "box_table.html";
$handle = file_exists($box_file);


if ($handle) {
	include $box_file;
}


$pcb_file = $folder . "pcb_table.html";
$handle = file_exists($pcb_file);


if ($handle) {
	include $pcb_file;
}


$manual_file = $folder . "manual_table.html";
$handle = file_exists($manual_file);


if ($handle) {
	include $manual_file;
}

$other_image_file = $folder . "other_image_table.html";
$handle = file_exists($other_image_file);


if ($handle) {
	include $other_image_file;
}

?>
