<?php

$region = $_GET['region'];
$scan_type = $_GET['type'];

if ($scan_type == "box") {

	$name = "List of box scans for region: ";

} else if ($scan_type == "cart") {
	$name =  "List of cartridge label scans for region: ";
} else {
	$name = "404";
}


if ($region == "AUS") {
	$country = "Australia";
} else if ($region == "Brazil") {
	$country = "Brazil";
} else if ($region == "CAN") {
	$country = "Canada";
} else if ($region == "EUR") {
	$country = "Europe";
} else if ($region == "FRA") {
	$country = "France";
} else if ($region == "FAH") {
	$country = "France and Netherlands";
} else if ($region == "FRG") {
	$country = "Germany, Austria, Switzerland";
} else if ($region == "NOE") {
	$country = "Germany (Nintendo of Europe)";
} else if ($region == "HKG") {
	$country = "Hong Kong";
} else if ($region == "ITA") {
	$country = "Italy";
} else if ($region == "JPN") {
	$country = "Japan";
} else if ($region == "LTN") {
	$country = "Latin America";
} else if ($region == "HOL") {
	$country = "Netherlands";
} else if ($region == "SCN") {
	$country = "Scandinavia";
} else if ($region == "KOR") {
	$country = "South Korea";
} else if ($region == "ESP") {
	$country = "Spain";
} else if ($region == "USA") {
	$country = "USA";
} else if ($region == "UKV") {
	$country = "United Kingdom";
} else  {
	$country = "404";
}

$name =  $name . $country;
$file_name = "cart_box/" . $region . "_" . $scan_type . ".html";


$handle = file_exists($file_name);


if (! $handle) {


	$file = '404.php';
} else {

	$file = $file_name;
}

include 'template.php';

?>
