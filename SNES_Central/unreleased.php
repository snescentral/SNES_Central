<?php

	$listtype = "Unreleased Games";
	$name = $listtype;
	$outputhtml  = $outputhtml .  '<p class="name">' . $name . '</p>';
//	$outputhtml = include 'gamelisting2.php'; 

	$outputhtml  = $outputhtml . "<p>Throughout the lifespan of the Super NES, there were many games that never saw the light of day for various reasons.  Unfortunately, most of these games are likely lost due to the passage of time. I am hoping to uncover as many of these games as possible, try to find the reasons for their cancellation, and if possible, hunt down working binaries of these games. Preserving these lost games is an important task, essentially digital archeology.</p>
<p>I always am looking for binaries of these games to investigate, so if you know of one that isn't already circulating on the Internet <a href=\"mailto:snes_central@yahoo.ca\">please let me know</a>. I also love any commentary from developers on these games.</p>";

// binaries exist
	$outputhtml = $outputhtml .  '<p class="name">Unreleased with leaked binary</p>

	<p>These are games where we have a binary of the unreleased version of the game, though some are early in development.</p>
';

	$filename = 'stuff/unreleased_rom.txt';
	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">
	<tr class=\"row1\">
		<td align=\"left\"><b>Game Name</b></td>
		<td align=\"left\"><b>Publisher</b></td>
		<td align=\"left\"><b>Developer</b></td>
		<td align=\"left\"><b>Unreleased Region</b></td>
		<td align=\"left\"><b>Other Region</b></td>
		<td align=\"left\"><b>Binary Available</b></td>
		<td align=\"left\"><b>Known Prototypes</b></td>
	</tr>

\n";

	$file = "gamelisting.php";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">";
		if (strcmp($item[1], '')) {
			 
$outputhtml = $outputhtml . "<td align=\"left\"><a href=\"article.php?id=" . $item[1] . "\">" . $item[0] . "</a></td>";
		}
		else {
			$outputhtml = $outputhtml . "<td align=\"left\">"  . $item[0] . "</td>";
			
		}
		 $outputhtml = $outputhtml .  "
		<td align=\"left\">" . $item[2] . "</td>
		<td align=\"left\">" . $item[3] . "</td>
		<td align=\"left\">" . $item[4] . "</td>
		<td align=\"left\">" . $item[5] . "</td>
		<td align=\"left\">" . $item[6] . "</td>
		<td align=\"left\">" . $item[7] . "</td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";


// released elsewhere
		$outputhtml = $outputhtml .  '<p class="name">Unreleased in one region but released elsewhere</p>

	<p>These are games that came out in one of the three major regions (Japan, North America, PAL), but plans for release in another region were cancelled. </p>
';

	$filename = 'stuff/unreleased_other.txt';
	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">
	<tr class=\"row1\">
		<td align=\"left\"><b>Game Name</b></td>
		<td align=\"left\"><b>Publisher</b></td>
		<td align=\"left\"><b>Developer</b></td>
		<td align=\"left\"><b>Unreleased Region</b></td>
		<td align=\"left\"><b>Other Region</b></td>
		<td align=\"left\"><b>Binary Available</b></td>
		<td align=\"left\"><b>Known Prototypes</b></td>
	</tr>

\n";

	$file = "gamelisting.php";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">";
		if (strcmp($item[1], '')) {
			 
$outputhtml = $outputhtml . "<td align=\"left\"><a href=\"article.php?id=" . $item[1] . "\">" . $item[0] . "</a></td>";
		}
		else {
			$outputhtml = $outputhtml . "<td align=\"left\">"  . $item[0] . "</td>";
			
		}
		 $outputhtml = $outputhtml .  "
		<td align=\"left\">" . $item[2] . "</td>
		<td align=\"left\">" . $item[3] . "</td>
		<td align=\"left\">" . $item[4] . "</td>
		<td align=\"left\">" . $item[5] . "</td>
		<td align=\"left\">" . $item[6] . "</td>
		<td align=\"left\">" . $item[7] . "</td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";

// the rest

	$outputhtml = $outputhtml .  '<p class="name">Unreleased everywhere</p>

	<p>These are games that were planned for release, but did not make it out anywhere. This includes games that came out on other systems like the Sega Genesis/Megadrive or Turbografx 16/PC Engine.</p>
';
	$filename = 'stuff/unreleased.txt';


	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">
	<tr class=\"row1\">
		<td align=\"left\"><b>Game Name</b></td>
		<td align=\"left\"><b>Publisher</b></td>
		<td align=\"left\"><b>Developer</b></td>
		<td align=\"left\"><b>Unreleased Region</b></td>
		<td align=\"left\"><b>Other Region</b></td>
		<td align=\"left\"><b>Binary Available</b></td>
		<td align=\"left\"><b>Known Prototypes</b></td>
	</tr>

\n";

	$file = "gamelisting.php";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">";
		if (strcmp($item[1], '')) {
			 
$outputhtml = $outputhtml . "<td align=\"left\"><a href=\"article.php?id=" . $item[1] . "\">" . $item[0] . "</a></td>";
		}
		else {
			$outputhtml = $outputhtml . "<td align=\"left\">"  . $item[0] . "</td>";
			
		}
		 $outputhtml = $outputhtml .  "
		<td align=\"left\">" . $item[2] . "</td>
		<td align=\"left\">" . $item[3] . "</td>
		<td align=\"left\">" . $item[4] . "</td>
		<td align=\"left\">" . $item[5] . "</td>
		<td align=\"left\">" . $item[6] . "</td>
		<td align=\"left\">" . $item[7] . "</td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";

	$file = "outputhtml.php";

	include "template.php";

?>
