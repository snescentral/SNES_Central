<?php


if ($origin == '') {
   $filename = $folder . "bib.txt";
}

else {
   $filename = $folder . $origin;
}
   $handle = file_exists($filename);

   if (!$handle) {
      echo "";
   }

   else {
	echo '<p class="name">Bibliography</p>' . "\n";

	echo '<ul>';
	$fileopen = file($filename);

	$amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

	if ($item[0] == 'website')
	  echo '<li>' . $item[2] . ' (<a href="' . $item[1]  . '">link</a>)</li>';

	else

         echo '<li> <i>' . $item[0] . '</i>, <u>' . $item[1] . '</u>, Publication date: ' . $item[2] . ', Volume: <b>' . $item[3] . '</b>, Pages: ' . $item[4] . '</li>';


         
      }
   }
 	echo '</ul>';
?>
