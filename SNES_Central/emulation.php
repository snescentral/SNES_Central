<?php

	$listtype = "Emulation";

	$intro = "This section details the history of various SNES emulators, some guides on how to use emulator features and reviews on tools that will be useful for people who want to use emulators.  SNES Central recommends <a href=\"http://www.snes9x.com/\">SNES9x</a> and <a href=\"http://www.zsnes.com\">ZSNES</a> for great SNES emulation!
<br><br>I used to have a development section on this site, but I no longer maintain it.  You can view the old page <a href=\"development.php\">here</a>.";

	$filename = 'stuff/emulation.txt';
	$name= $listtype;

	$file = "gamelisting.php";

	include "template.php";

?>
