<?php

// chiplisting.php


$name = "PCB listing";
$meta_description="PCB list on SNES Central";
$meta_image= "icon/banner.gif";

$outputhtml = file_get_contents('pcblist/description.html');
$outputhtml  = $outputhtml .   file_get_contents('chips/stats.html');
$outputhtml  = $outputhtml .   file_get_contents('pcblist/list.html');

$file = "outputhtml.php";

include 'template.php';

?>
