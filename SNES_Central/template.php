<?php

	header("Content-type: text/html; charset=utf-8");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

  <head>

	<link rel="shortcut icon" href="icon/sneslogo.ico" type="image/x-icon" />
	<title>Snes Central: <?=$name?></title>
	<link rel="stylesheet" href="stylesheet4.css">
	  
	  
<meta property="og:title" content="SNES Central: <?=$name?>">
<meta property="twitter:title" content="SNES Central: <?=$name?>">
<meta property="og:type" content="article">
<meta property="og:url" content="http://snescentral.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<?php
	  if (strlen($meta_image) < 1) {
		 $meta_image = "icon/banner.gif";
	  }
  echo '
<meta property="og:image" content="http://snescentral.com/' . $meta_image . '">
<meta property="twitter:image" content="http://snescentral.com/' . $meta_image . '">	
';

	  if (strlen($meta_description) < 1) {
		 $meta_description = "Page on SNES Central for " . $name;
	  }  
	  echo '
	  <meta property="og:description" content="' . $meta_description . '">
<meta property="twitter:description" content="' . $meta_description . '">
';
?>

<meta property="og:site_name" content="SNES Central">
<meta property="og:locale" content="en_US">

  </head>

      <body>
<table class="contenttable">
		<tr>
		<td colspan="2" class="topbar">
		
			<img src="icon/banner.gif" alt="image">
			<img src="icon/terra.gif" alt="image">
			<img src="icon/belmont.gif" alt="image">
			<img src="icon/yoshi.gif" alt="image">
			<img src="icon/mario.gif" alt="image">
			<img src="icon/samus.gif" alt="image">
			<img src="icon/chrono.gif" alt="image">
			<img src="icon/donkeykong.gif" alt="image">
			<img src="icon/megamanx.gif" alt="image">
			<img src="icon/vicviper.gif" alt="image">
			<img src="icon/falcon.gif" alt="image">
			<img src="icon/umihara.gif" alt="image">
			<img src="icon/zero.gif" alt="image">
			<img src="icon/rocky.gif" alt="image">
		
		</td>
	
<tr>	
<td class ="sidebar">

<p></p>
	

<a href="index.php" class="yoshi">Main</a><br>
<a href="gameindex.php" class="yoshi">Games</a><br>
<a href="pcblisting.php" class="yoshi">PCB<br>&nbsp;&nbsp;Archive</a><br>
<a href="chiplisting.php" class="yoshi">Chip<br>&nbsp;&nbsp;Archive</a><br>
<a href="scan_index.php" class="yoshi">Cart/Box<br>&nbsp;&nbsp;Scans</a><br>
<a href="art.php" class="yoshi">Articles</a><br>
<a href="peripherals.php" class="yoshi">Peripherals</a><br>
<a href="prototypes.php" class="yoshi">Prototypes</a><br>
<a href="unreleased.php" class="yoshi" >Unreleased<br>&nbsp;&nbsp;Games</a><br>
<!-- <li class="sidelist"><a href="pirates.php"><img src="icon/pirates.gif" alt="image"></a></li> -->
<a href="rarities.php" class="yoshi">Rarities</a><br>
<a href="homebrew.php" class="yoshi">Homebrew</a><br>
<a href="emulation.php" class="yoshi">Emulation</a><br>
<!-- <li class="sidelist"><a href="development.php"><img src="icon/development.gif" alt="image"></a></li> -->
<!--  <li class="sidelist"><a href="forum/"><img src="icon/message.gif" alt="image"></a></li>
<li class="sidelist"><a href="links.php"><img src="icon/links.gif" alt="image"></a></li>
<li class="sidelist"><a href="website.php"><img src="icon/website.gif" alt="image"></a></li> -->


	
<p class="sidetext"><b>Email: <a href="mailto:snes_central@yahoo.ca">snes_central@yahoo.ca</a></b></p> 


<ul class="navbar">
		<li class="sidelist"><a href="https://discord.gg/CERtpHZ"><img src="icon/logo_discord_menu.svg"></a></li>
</ul>

<?php
	echo $name;
	if ($name=='Index') {

		echo '<p class="sidetextbig"><b>Partner Websites:</b></p>
<ul class="navbar">
<li class="sidelist"><a href="http://www.superfamicom.org/"><img src="icon/super_famicom_org.png" alt="image"></a></li>
<li class="sidelist"><a href="https://eludevisibility.org/"><img src="icon/eludevisibility.png" alt="image"></a></li>
<li class="sidelist"><a href="http://gamehistory.org/"><img src="icon/vghf.png" alt="image"></a></li>
<li class="sidelist"><a href="http://www.vr32.de/"><img src="icon/pvbr_vid.gif" alt="image"></a></li>
<li class="sidelist"><a href="http://www.game-rave.com/"><img src="icon/grbutton.jpg" alt="image"></a></li>
</ul>
</ul>

';
	}

?>

</td>



		<td class="contentbar">
		
		<?php
		
			include $file;

		?>
		
		<br />

		</td>
</tr>

<tr>
<td colspan="2" class="bottombar">
&copy; Evan G.  This site made by a Canadian, and fueled by beer.  Do not use material on this site without permission.
</td>

</table>	



      </body>

</html>
