<?php

	
           if( file_exists ( $folder . "logo.gif" ) ){
             $logo_image=  "logo.gif";
           }

           if( file_exists ( $folder . "logo.png" ) ){
             $logo_image=  "logo.png";
           }

	echo "<p  class=\"logo\"> 
<img src=\"" . $folder . $logo_image . "\" alt=\"logo\"></p>\n";

echo 

"<TABLE class=\"infotable\">
<CAPTION>" . $name . "</CAPTION>
<tr class=\"row1\">
	<td class=\"infoleft\">Game Code:</td>
        <td class=\"inforight\">" . $gameid . "</td>
	<td class=\"infoleft\">Genre:</td>
        <td class=\"inforight\">" . $genre . "</td>
</tr>
<tr class=\"row2\">
	<td class=\"infoleft\">US Title:</td>
        <td class=\"inforight\">" . $US_title . "</td>
	<td class=\"infoleft\">US Publisher:</td>
        <td class=\"inforight\">" . $US_pub . "</td>
</tr>
<tr class=\"row1\">
	<td class=\"infoleft\">Japan Title:</td>
        <td class=\"inforight\">" . $Jap_title . "</td>
	<td class=\"infoleft\">Japan Publisher:</td>
        <td class=\"inforight\">" . $Jap_pub . "</td>
</tr>
<tr class=\"row2\">
	<td class=\"infoleft\">Title in Kanji:</td>
        <td class=\"inforight\">" . $kanji . "</td>

	<td class=\"infoleft\">Title in Kana:</td>
        <td class=\"inforight\">" . $kana . "</td>
</tr>
<tr class=\"row1\">
	<td class=\"infoleft\">European Title:</td>
        <td class=\"inforight\">" . $Eur_title . "</td>
	<td class=\"infoleft\">Europe Publisher:</td>
        <td class=\"inforight\">" . $Eur_pub . "</td>
</tr>
<tr class=\"row2\">
	<td class=\"infoleft\">Developer:</td>
        <td class=\"inforight\">" . $Dev . "</td>
	<td class=\"infoleft\">Players:</td>
        <td class=\"inforight\">" . $players . "</td>
</tr>
<tr class=\"row1\">
	<td class=\"infoleft\">Accessories:</td>
        <td class=\"inforight\">" . $Acc . "</td>
	<td class=\"infoleft\">Special Chips:</td>
        <td class=\"inforight\">" . $chip . "</td>
</tr>
<tr class=\"row2\">
	<td class=\"infoleft\">Save:</td>
        <td class=\"inforight\">" . $save . "</td>
	<td class=\"infoleft\"></td>
        <td class=\"inforight\"></td>
</tr>
</TABLE>\n";

  echo "<p>";
if($article_type == "1") {
	if ($description != '')
  		echo  $description;
	else
  		echo '';
}

else if($article_type == "4") {

	include 'fancy.php';

}

	echo "</p>\n<p>";
echo '<hr>';

include 'prototype_info.php';

//include 'cart_info.php';
$filename = $folder . "carts.html";
$handle = file_exists($filename);

if ($handle) {
	include $filename;
}

//include 'pcbinfo.php';

$filename = $folder . "pcb.html";
$handle = file_exists($filename);

if ($handle) {
	include $filename;
}

//include 'box_info.php';

$filename = $folder . "box.html";
$handle = file_exists($filename);

if ($handle) {
	include $filename;
}

include 'manual.php';

include 'others_info.php';

/*
echo '<p class="name">PCB Scans</p>' . "\n";

   $filename = $folder . "pcb.txt";

   $handle = file_exists($filename);

   if (!$handle) {
      echo "There are no pcb scans <br />";
   }

   else {
	$fileopen = file($filename);

	$amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

         echo '<a href="pcb.php?id=' . $id . '&num=' . $i . '">' . $item[2] . '</a><br />';
         
      }
   }



echo '<p class="name">Box Scans</p>' . "\n";

   $filename = $folder . "box.txt";

   $handle = file_exists($filename);

   if (!$handle) {
      echo "There are no box scans <br />";
   }

   else {
	$fileopen = file($filename);

	$amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

         echo '<a href="box.php?id=' . $id . '&num=' . $i . '">' . $item[2] . '</a><br />';
         
      }
   }


echo '<p class="name">Manual and other scans</p>' . "\n";

   $filename = $folder . "manual.txt";

   $handle = file_exists($filename);

   if (!$handle) {
      echo "There are no manual scans <br />";
   }

   else {
	$fileopen = file($filename);

	$amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

	if ($item[3] == 'file') {

	echo '<a href="' . $folder . $item[0] . '">' . $item[2] . '</a> - contributed by ' . $item[1] . '<br />';

	}

	else {

         echo '<a href="manual.php?id=' . $id . '&num=' . $i . '">' . $item[2] . '</a><br />';

	}
         
      }
   }

*/



   $filename = $folder . "reviews.txt";

   $handle = file_exists($filename);

   if (!$handle) {
    //  echo "There are no reviews <br />";
   }

   else {

	echo '<p class="name">Reviews</p>' . "\n";
	$fileopen = file($filename);

	$amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

         echo '<a href="review.php?id=' . $id . '&num=' . $i . '&fancy=' . $item[3] . '&article=review">' . $item[1] . ' - ' . $item[2] . '</a><br />';
         
      }
   }




   $filename = $folder . "screenshots.txt";

   $handle = file_exists($filename);

   if (!$handle) {
 //     echo "There are no screenshots <br />";
   }

   else {

	echo '<p class="name">Screenshots</p>' . "\n";
      $fileopen = file($filename);

      $amount = count($fileopen);

      echo '<a href="screenshots.php?id=' . $id . '&num=0">Screenshots (' . $amount . ')</a></p>'; 

   }



   $filename = $folder . "cheats.txt";

   $handle = file_exists($filename);

   if (!$handle) {
    //  echo "There are no cheats or Strategy Guides <br />";
   }

   else {
echo '<p class="name">Cheats and Strategy Guides</p>' . "\n";
	$fileopen = file($filename);

	$amount = count($fileopen);

        for ($i=0; $i < $amount; $i++) {

		$item = explode('|', $fileopen[$i]);

		if ($item[0] == "1") {

         		echo '<a href="stuff.php?id=' . $id . '&fil=' . $item[1] . '">' . $item[3] . '</a> - by ' . $item[2] . '<br />';

		}


		else if ($item[0] == "2") {

	 		echo '<a href="' . $folder . $item[1] . '">' . $item[3] . '</a> - by ' . $item[2] . '<br />';

		}
      	   
      	}
   }
/*
echo '<p class="name">Collector\'s Guide</p>' . "\n";
   $filename = $folder . "collect.txt";

   $handle = file_exists($filename);

   if (!$handle) {
      echo "There are no entries in the collector's guide as of yet.  <a href=\"http://www.snescentral.com/forum/index.php?topic=270.0\">Click here</a> for more information.<br />";
   }

   else {
      $fileopen = file($filename);

      $amount = count($fileopen);

      for ($i=0; $i < $amount; $i++)
      {
	 $item = explode('|', $fileopen[$i]);

         echo '<table><tr><td VALIGN="top"><img src="' . $folder .  $item[0] . '"></td><td><b>'
		 . $item[1] . ' Release</b> <br / >Contributed by: ' . $item [2] .
		 '<br><br><b>Comments</b>:<br /> <br />'. $item[3] .
		 '<br /><br /><b>Contents</b>:<br /><ul> ';
	
	$countedarray = count($item);

	for ($k=4; $k < $countedarray; $k++) 
		{ 
			echo '<li>' . $item[$k] . '</li>';
		}

	echo '</ul></td></tr></table>';
         
      }
   }

*/

include 'reviewscores.php';
include 'bib.php';		

?>
