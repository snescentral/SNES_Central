<?php

$id = $_GET['id'];
$number = intval($_GET['num']);

include 'id_check.php';

if($proceed){

	$folder = 'article_files/' . substr($id, 0,1) . '/' . substr($id, 1,1) .  '/' . $id . '/';


	$filename = $folder . "cart_check.txt";
	$handle = file_exists($filename);

	if($handle) {
		$fileopen = file($filename);

		$amount_lines = count($fileopen);

		$line_found = False;
		for ($i=0; $i < $amount_lines; $i++) {

			$item = explode("\t", $fileopen[$i]);
			
			if ($number == $item[0]) {
				$line_found = True;
				$name = $item[1] . " - Cart Information";

			}

		}
	}
	else {

		$line_found = False;

	}

	if ($line_found){

		$file = $folder . "carts/" . $number . ".html";

	}
	else {
		$file = '404.php';
	}

} else {
	$file = 'error.txt';
}


	include 'template.php';


?>
