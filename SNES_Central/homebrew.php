<?php

	$listtype = "Homebrew SNES Games/Demos";
	$name = $listtype;
	$outputhtml  = $outputhtml .  '<p class="name">' . $name . '</p>';
//	$outputhtml = include 'gamelisting2.php'; 

	$outputhtml  = $outputhtml . "<p>With the commercial death of the SNES long past, people who grew up with the console have longed for new releases. With this page, I hope to document non-commercial homebrew that will work on the SNES. I have attempted to play these on Higan or an actual console to ensure that they will actually work. I've trying to order things based on time of release, rather than practical playability (which at this point is limited in most homebrew).</p>";


	$filename = 'stuff/homebrew.txt';


	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">

	<col width=\"150\">
	<col width=\"100\">
	<col width=\"50\">
	<col width=\"350\">

	<tr class=\"row1\">
		<td align=\"left\"><b>Game/demo Name</b></td>
		<td align=\"left\"><b>Author</b></td>
		<td align=\"left\"><b>Year Released</b></td>
		<td align=\"left\"><b>Notes</b></td>
	</tr>

\n";

	$file = "gamelisting.php";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">";
		if (strcmp($item[1], '')) {
			 
$outputhtml = $outputhtml . "<td align=\"left\"><a href=\"article.php?id=" . $item[1] . "\">" . $item[0] . "</a></td>";
		}
		else {
			$outputhtml = $outputhtml . "<td align=\"left\">"  . $item[0] . "</td>";
			
		}
		 $outputhtml = $outputhtml .  "
		<td align=\"left\">" . $item[2] . "</td>
		<td align=\"left\">" . $item[3] . "</td>
		<td align=\"left\">" . $item[4] . "</td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";

 // hardware demos


	$outputhtml  = $outputhtml .  '<p class="name">Hardware tests</p>';

	$outputhtml  = $outputhtml . "<p>These are demos created with the purpose of testing SNES hardware. Putting them here for preservation purposes.</p>";


	$filename = 'stuff/homebrew_hardware.txt';


	$fileopen = file($filename);

	$amount = count($fileopen);

	$outputhtml = $outputhtml .  "
<table class=\"infotable\">

	<col width=\"150\">
	<col width=\"100\">
	<col width=\"50\">
	<col width=\"350\">

	<tr class=\"row1\">
		<td align=\"left\"><b>Game/demo Name</b></td>
		<td align=\"left\"><b>Author</b></td>
		<td align=\"left\"><b>Year Released</b></td>
		<td align=\"left\"><b>Notes</b></td>
	</tr>

\n";

	$file = "gamelisting.php";

      for ($i=0; $i < $amount; $i++)
      {

	if ($i % 2 == 1) {
		$rowval = 1;
	}
	else {
		$rowval = 2;
	}
	
	 $item = explode('|', $fileopen[$i]);

	  $outputhtml = $outputhtml .  "
	<tr class=\"row" . $rowval . "\">";
		if (strcmp($item[1], '')) {
			 
$outputhtml = $outputhtml . "<td align=\"left\"><a href=\"article.php?id=" . $item[1] . "\">" . $item[0] . "</a></td>";
		}
		else {
			$outputhtml = $outputhtml . "<td align=\"left\">"  . $item[0] . "</td>";
			
		}
		 $outputhtml = $outputhtml .  "
		<td align=\"left\">" . $item[2] . "</td>
		<td align=\"left\">" . $item[3] . "</td>
		<td align=\"left\">" . $item[4] . "</td>
	</tr>
\n";
	  

      }

	$outputhtml = $outputhtml .  "</table>\n";
	$file = "outputhtml.php";

	include "template.php";

?>
