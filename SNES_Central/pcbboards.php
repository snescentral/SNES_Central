<?php

// pcbboards_temp.php


$chip = $_GET['chip'];

$fileopen = fopen('pcblist/list.txt',"r");
$amount_lines = count(file("pcblist/list.txt"));

$line_found = False;
for ($i=1; $i < $amount_lines+1; $i++) {
	$line_of_text = fgetcsv( $fileopen, 1024, "\t" );
	if ($chip == $line_of_text[0]) {
		$line_found = True;
		$matched_line = $line_of_text;

	}
}

if ($line_found) {
	$name = $matched_line[1];
	$meta_description="PCB information for board type: " . $matched_line[1] . " on SNES Central";
	$meta_image= "icon/banner.gif";
	$file = "pcblist/html/" . $matched_line[0] . ".html";

	$outputhtml = file_get_contents($file);

	// the following section will be replaced later


// Games that use the PCB type

	$outputhtml = $outputhtml . '<p class="headingtext">Games that use this PCB type:</p>';

  $filename2 = 'chips/games/' . $chip . '_games';
	$fileopen2 = file($filename2);

	$amount2 = count($fileopen2);

	$outputhtml = $outputhtml . '

<table  class="infotable">
	<tr  class="row2">
		<th style="width : 400px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Game Name</th>
		<th style="width : 100px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Region</th>
		<th style="width : 200px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Serial Code</th>
	</tr>

		

';

      for ($i=0; $i < $amount2; $i++) {

		if ( floatval($i)%2 == 0 ) {
		$row = "row1";
		}

		else {
		$row = "row2";
		}

	 $item = explode('|', $fileopen2[$i]);


	$outputhtml = $outputhtml .  '
	<tr class = "' . $row . '">
		<td style="text-align: left;"><a href="article.php?id=' . $item[1] . '">' . $item[0] . '</a></td>
		<td>' . $item[2] . '</td>
		<td>' . $item[3] . '</td>
	</tr>
';

     }

	$outputhtml = $outputhtml . "</table>";




	$file = 'outputhtml.php';


} else {

	$file = '404.php';
}



include 'template.php';


?>
