<?php




   $filename = $folder . "pcb.txt";

   $handle = file_exists($filename);

   if (!$handle) {
   //   echo "There is no pcb information<br />";
   }

   else {

echo '<p class="name">PCB Information</p>' . "\n";
	$fileopen = file($filename);

	$amount = count($fileopen);

	

	echo '

<table  class="infotable">
	<tr  class="row2">
		<th style="width : 120px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">ROM Chip ID</th>
		<th style="width : 60px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Region</th>
		<th style="width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Known Man. Dates</th>
		<th style="width : 120px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">PCB Type</th>
		<th style="width : 70px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">CIC</th>
		<th style="width : 80px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">SRAM</th>
		<th style="width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Decoder/ Logic Circuit</th>
		<th style="width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;">Special Chips</th>
	</tr>

		

';



      for ($i=0; $i < $amount; $i++)
      {

if ( floatval($i)%2 == 0 ) {
$row = "row1";
}

else {
$row = "row2";
}

	 $item = explode('|', $fileopen[$i]);

	   if (count($item) < 5) {
         echo '
	<tr class = "' . $row . '">
		<td style="text-align: left;"><a href="pcb.php?id=' . $id . '&num=' . $i . '">' . $item[2] . '</a></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
';

	  }

	  else {

	    echo '
	<tr class = "' . $row . '">
		<td style="text-align: left; vertical-align: top;">' . $item[3] . ' ';

echo '(<a href="pcb.php?id=' . $id . '&num=' . $i . '&side=front">front</a>) ';

if ($item[11] != '') {
	echo '(<a href="pcb.php?id=' . $id . '&num=' . $i . '&side=back">back</a>) ';

}


echo '</td>
		<td style="text-align: left; vertical-align: top;">' . $item[4] . '</td>
		<td style="text-align: left; vertical-align: top;">' . $item[5] . '</td>
		<td style="text-align: left; vertical-align: top;"><a href="pcbboards.php?chip=' . $item[6] . '">' . $item[6] . '</a></td>
		<td style="text-align: left; vertical-align: top;">' . $item[7] . '</td> 

<td style="text-align: left; vertical-align: top;">' . $item[8] . '</td> 

';

	   if ($item[9] == '') { echo '<td style="text-align: left; vertical-align: top;">There is no decoder chip</td>'; }
	   else { echo '<td style="text-align: left; vertical-align: top;">' . $item[9] . '</td>'; }

	   if ($item[10] == '') { echo '<td style="text-align: left; vertical-align: top;">There are no special chips</td>'; }
	   else { echo '<td style="text-align: left; vertical-align: top;">' . $item[10] . '</td>'; }
	echo '	
	</tr>

';
         
      }

	}

	echo "</table>
		<p><i>Do you have a PCB with a different ROM chip ID, PCB type or manufacturing date?</i> Read the <a href=\"article.php?id=1094\">Submission guidelines</a> for PCB scans.

";
   }

?>
