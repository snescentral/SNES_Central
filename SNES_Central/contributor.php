<?php

// chips.php

$id = $_GET['id'];

$fileopen = fopen('contributors/filecheck.txt',"r");
$amount_lines = count(file("contributors/filecheck.txt"));

$line_found = False;
for ($i=1; $i < $amount_lines+1; $i++) {
	$line_of_text = fgetcsv( $fileopen, 1024, "\t" );
	if ($id == $line_of_text[0]) {
		$line_found = True;
		$matched_line = $line_of_text;

	}
}

if ($line_found) {
	$name = "Contributor - " . $matched_line[1];
	$meta_description="Contributor: " . $matched_line[1] . " on SNES Central";
	$meta_image= "icon/banner.gif";
	$file = "contributors/html/" . $id . ".html";

} else {

	$file = '404.php';
}

include 'template.php';

?>
