<?php

// chips.php

$id = $_GET['chiptype'];

$fileopen = fopen('chiplist/list.txt',"r");
$amount_lines = count(file("chiplist/list.txt"));

$line_found = False;
for ($i=1; $i < $amount_lines+1; $i++) {
	$line_of_text = fgetcsv( $fileopen, 1024, "\t" );
	if ($id == $line_of_text[0]) {
		$line_found = True;
		$matched_line = $line_of_text;

	}
}

if ($line_found) {
	$name = $id;
	$meta_description="Chip list for: " . $id . " on SNES Central";
	$meta_image= "icon/banner.gif";
	$file = "chiplist/html/" . $id . ".html";

} else {

	$file = '404.php';
}

include 'template.php';

?>
