#!/bin/bash

rm all_pcbs.txt
rm to_fix.txt
rm pcb_matchup.txt
rm chip_dates.txt


cat << END_cat > all_pcbs.txt
page_id|file_name|contributor|description|serial_code|region|date_stamp|pcb_type|lockout|ram|decoder|special_chip|back_file_name|
END_cat

for number in $( seq 0 1210 )
do

  if [ ${number} -lt 10 ]
  then
    full=000${number}
  elif [ ${number} -lt 100 ]
  then
    full=00${number}
  elif [ ${number} -lt 1000 ]
  then
    full=0${number}
  else
    full=${number}
  fi
  

  folder=SNES_Central/${full:0:1}/${full:1:1}/${full:2:1}/${full}

  if [ -e "${folder}/pcb.txt" ]
  then
    awk -v number=${full} '{print number"|"$0}' ${folder}/pcb.txt >> all_pcbs.txt

  fi


  

done

awk --field-separator '|'  '{if ($6 != "PAL" && $6 != "USA" && $6 != "Japan" && $6 != "France" && $6 != "Germany") {print number, $0}}' all_pcbs.txt >> to_fix.txt
awk --field-separator '|'  '{print $1, $5, $6, $8}' all_pcbs.txt  >> pcb_matchup.txt
awk  --field-separator '|'  '{if ($5 != "Prototype") {print $1, $7}}' all_pcbs.txt  >> chip_dates.txt



