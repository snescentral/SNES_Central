#!/usr/bin/env python3

import json
import sys
import os
from .genre import genreClass

class game:
	def __init__(self, game_id):
		self.game_id = game_id # four digit ID, which represents the folder which the information is stored
		self.global_title = '' # general title that goes in the HTML header
		self.releases = [] # should be a list of releases objects
		self.developer = [] # will contain a list of developers
		self.peripherals = [] # contains a list of peripherals
		self.genre = [] # contains a list of genres
		self.savetype = [] # could be more than one save type for some games

		self.valid_peripherals = ['Super Scope', 'Mouse', 'Barcode Battler', 'Turbo File Twin', 'Justifier', 'Multitap']

	def print_game_info(self):

		region_list = []

		for region_temp2 in self.releases:
			region_list.append(region_temp2.region)

		print("\nDump of game data:")
		print("1) ID:               " + self.game_id)
		print("2) Global name:      " + self.global_title)
		print("3) Release regions: " ,  region_list)
		print("4) Developers:      ", self.developer)
		print("5) Peripherals:     ", self.peripherals)
		print("6) genre:           ", self.genre)
		print("7) save type:       ", self.savetype)

	def print_peripherals(self):

		counter = 0

		for peripherals in self.valid_peripherals:
			counter += 1
			print("%i) %s" % (counter, peripherals))

	def check_peripherals(self, number):

		if int(number) <= len(self.valid_peripherals)+1 and int(number) > 0:
			valid = True
		else:
			valid = False

		return valid

	def peripheral_entry(self,number):
		return self.valid_peripherals[int(number)-1]

	def read_file(self):

		# detect if the game entry is there
		game_path = "data_files/" + self.game_id[0] + '/' + self.game_id[1] + '/' + self.game_id[2] + '/' + self.game_id
		game_info_file = game_path + "/game_info.json"

		# temporary for testing, delete later
		game_info_file = "workspace/data_file.json"

		is_game =  os.path.isfile(game_info_file)

		if not is_game:
			print("Warning: You must add the game info file with game_handler.py first")
			sys.exit()


		with open(game_info_file, 'r') as file_open:
			datastore = json.load(file_open)

			if 'game_id' in datastore: # check to ensure the game id is the same 
				if datastore['game_id'] != self.game_id:
					print("Warning: game id in the file does not match the input, the file should be manually checked!")
					sys.exit()


			if 'global_title' in datastore:
				self.global_title = datastore['global_title']

			releases_counter = 0

			if 'releases' in datastore:
				for release_temp in datastore['releases']:
					self.releases.append(releases_store())

					self.releases[releases_counter].add_region_old(release_temp['region'])
					self.releases[releases_counter].add_title(release_temp['title'])
					self.releases[releases_counter].add_publisher(release_temp['publisher'])
					self.releases[releases_counter].add_serial_code(release_temp['serial_code'])


					if 'kana' in release_temp:
						self.releases[releases_counter].add_kana(release_temp['kana'])
					if 'kanji' in release_temp:
						self.releases[releases_counter].add_kanji(release_temp['kanji'])
					if 'hangul' in release_temp:
						self.releases[releases_counter].add_hangul(release_temp['hangul'])
					if 'hanzi' in release_temp:
						self.releases[releases_counter].add_hanzi(release_temp['hanzi'])

					releases_counter += 1

			if 'developers' in datastore:
				self.developer = datastore['developers']
			if 'peripherals' in datastore:
				self.developer = datastore['peripherals']
			if 'genre' in datastore:
				self.genre = datastore['genre']
			if 'savetype' in datastore:
				self.genre = datastore['savetype']



class releases_store:



	def __init__(self):
		self.valid_regions = ['Japan', 'USA', 'Canada', 'Brazil', 'South Korea', 'PAL', 'United Kingdom', 'France', 'France and Holland', 'Germany', 'Italy', 'Australia', 'Taiwan', 'Spain', 'Scandinavia']
		self.region = ""
		self.title = ""
		self.publisher = ""
		self.serial_code = "" # most games have only one serial code, but this is not universally true, i.e. World League Basketball/ NCAA Basketball
		self.kanji = ''
		self.kana = ''
		self.hangul = ''
		self.hanzi = ''

	def add_region(self): # fails if aborted or bad input

		print("available regions, type a number to select or enter to abort adding:")
		self.print_regions()

		entry = input(">>:")
		found = False
		counter = 0
		if entry:
			for region_choice in self.valid_regions:
				counter +=1
				if entry == str(counter):
					self.region = region_choice
					found = True

		return found

	def add_region_old(self, region):

		assert region in self.valid_regions

		self.region = region

	def add_title(self, title):
		self.title = title
	def add_publisher(self, publisher):
		self.publisher = publisher

	def add_serial_code(self, serial_code):
		self.serial_code = serial_code


	def add_kanji(self, kanji):
		self.kanji = kanji

	def add_kana(self, kana):
		self.kana = kana

	def add_hangul(self, hangul):
		self.hangul = hangul

	def add_hanzi(self, hanzi): # only really relevant for Romance of Three Kingdoms
		self.hanzi = hanzi

	def print_regions(self):
		counter = 0
		for region in self.valid_regions:
			counter += 1
			print("%i) %s" % (counter, region))

	def change_region(self, region_number, old_region):
		counter = 0
		new_region = old_region
		found = False
		for region in self.valid_regions:
			counter +=1
			if region_number == str(counter):
				new_region = region
				found = True

		if not found:
			print("region not found, retaining old region")

		return new_region



def check_number(entry, total_number):
	found = False
	for numbers in range(1, total_number+1):
		if entry == str(numbers):
			found = True
	return found


if len(sys.argv) > 2 or len(sys.argv) < 2:
	print("Warning: you must enter the four digit id code \nof the game/article you are interested in")
	sys.exit()

if len(sys.argv[1]) > 4 or len(sys.argv[1]) < 4:
	print("Warning: you must enter the four digit id code \nof the game/article you are interested in")
	sys.exit()

game_id = sys.argv[1]

game_a = game(game_id)
game_a.read_file()



	
editing = True

while (editing):

	game_a.print_game_info()

	print("If you would like to add or edit, type the number. \"q\" to quit")
	entry = input('>>: ')


	if entry == "1":
		print("You cannot edit the ID number")
	elif entry == "2":
		print("Enter a new global title (do not enter anything if you want it unchanged):")
		entry2 = input(">>: ")

		if entry2:
			game_a.global_title = entry2

	elif entry == "5":

		peripheral_counter = 0
		for peripheral_temp in game_a.peripherals:
			peripheral_counter += 1
			print("%i) %s" % (peripheral_counter, peripheral_temp))

		print("Type a number to delete entry, 'a' to add a peripheral, or 'enter' to do nothing")
		entry5 = input(">>: ")

		if entry5 != "":
			if entry5 == "a":
				game_a.print_peripherals()

				print("Type a number to add a peripheral, or enter to skip")

				entry5a = input(">>: ")

				if entry5a != "":
					if game_a.check_peripherals(entry5a):
						game_a.peripherals.append(game_a.peripheral_entry(entry5a))
					else:
						print("invalid entry")

			elif check_number(entry5, peripheral_counter):
				game_a.peripherals.pop(int(entry5)-1)


	elif entry == "4":
		print("\nCurrent developers")
		dev_counter = 0
		for dev_temp in game_a.developer:
			dev_counter +=1
			print("%i) %s" % (dev_counter, dev_temp))
		
		print("Type a number to edit or delete entry, 'a' to add a developer, or 'enter' to do nothing")
		entry4 = input(">>: ")
	
		if entry4 == "a":
		
			print("Type in the developer:")
			entry4a = input(">>: ")

			if entry4a != "":
				game_a.developer.append(entry4a)

		elif entry4 != "":
			if check_number(entry4, dev_counter):
				print("Developer: [%s], type something to change, \"enter\" to skip, \"-\" to delete" % game_a.developer[int(entry4)-1])
				entry4b  = input(">>: ")
				if entry4b != "":
					if entry4b == "-":
						game_a.developer.pop(int(entry4)-1)
					else: 
						game_a.developer[int(entry4)-1] =  entry4b


	elif entry == "3":
		
		print("\nCurrent regions: ")

		region_counter2 = 0
		for region_temp in game_a.releases:
			region_counter2 += 1
			print(str(region_counter2) + ") " + region_temp.region)

		
		print("To view a region, type the number; type 'a' to add a new region, or 'enter' to do nothing")

		entry2 = input(">>: ")

		if entry2 == "a":

			game_a.releases.append(releases_store())

			print(region_counter2)
			valid_region_found = game_a.releases[region_counter2].add_region()
			if valid_region_found:
	
				print("Enter title:")
				entry5  = input(">>: ")
				if entry5 != "":
					game_a.releases[region_counter2].title =  entry5

				print("Enter publisher:")
				entry5  = input(">>: ")
				if entry5 != "":
					game_a.releases[region_counter2].publisher =  entry5

				print("Enter serial code:")
				entry5  = input(">>: ")
				if entry5 != "":
					game_a.releases[region_counter2].serial_code =  entry5


				if (game_a.releases[region_counter2].region == "Japan"):
					
					print("Title in kana: \"enter\" to skip" )
					entry5  = input(">>: ")
					if entry5 != "":

						game_a.releases[region_counter2].kana =  entry5

					print("Title in kanji: \"enter\" to skip" )
					entry5  = input(">>: ")
					if entry5 != "":

						game_a.releases[region_counter2].kanji =  entry5

				if (game_a.releases[region_counter2].region == "South Korea"):
					
					print("Title in hangul: \"enter\" to skip" )
					entry5  = input(">>: ")
					if entry5 != "":

						game_a.releases[region_counter2].hangul =  entry5


				if (game_a.releases[region_counter2].region == "Taiwan"):
					
					
					print("Title in hanzi: \"enter\" to skip" )
					entry5  = input(">>: ")
					if entry5 != "":

						game_a.releases[region_counter2].hanzi =  entry5


			else: # remove
				game_a.releases.pop(region_counter2)



		elif entry2 != "" : 
			if check_number(entry, region_counter2): # display entry
				print("Region: " + game_a.releases[int(entry2)-1].region)
				print("Title: " + game_a.releases[int(entry2)-1].title)
				print("Publisher: " + game_a.releases[int(entry2)-1].publisher)
				print("Serial Code: " + game_a.releases[int(entry2)-1].serial_code)
				
				if game_a.releases[int(entry2)-1].kana:
					print("Kana: " + game_a.releases[int(entry2)-1].kana)
				if game_a.releases[int(entry2)-1].kanji:
					print("Kanji: " + game_a.releases[int(entry2)-1].kanji)
				if game_a.releases[int(entry2)-1].hangul:
					print("Hangul: " + game_a.releases[int(entry2)-1].hangul)
				if game_a.releases[int(entry2)-1].hanzi:
					print("hanzi: " + game_a.releases[int(entry2)-1].hanzi)

				print("To edit, type 'e'; type 'd' to delete; 'enter' to do nothing")

				entry3 = input(">>: ")

				if entry3 == "e":
					print("Region: [%s], type number to change, \"enter\" to skip" % game_a.releases[int(entry2)-1].region)
					game_a.releases[int(entry2)-1].print_regions()
					entry4  = input(">>: ")
					if entry4 != "":
						game_a.releases[int(entry2)-1].region = game_a.releases[int(entry2)-1].change_region(entry4, game_a.releases[int(entry2)-1].region)


					print("Title: [%s], type something to change, \"enter\" to skip" % game_a.releases[int(entry2)-1].title)
					entry4  = input(">>: ")
					if entry4 != "":
						game_a.releases[int(entry2)-1].title =  entry4

					print("Publisher: [%s], type something to change, \"enter\" to skip" % game_a.releases[int(entry2)-1].publisher)
					entry4  = input(">>: ")
					if entry4 != "":
						game_a.releases[int(entry2)-1].publisher =  entry4

					print("Serial Code: [%s], type something to change, \"enter\" to skip" % game_a.releases[int(entry2)-1].serial_code)
					entry4  = input(">>: ")
					if entry4 != "":
						game_a.releases[int(entry2)-1].serial_code =  entry4

					if (game_a.releases[int(entry2)-1].region == "Japan"):
						
						print("Title in kana: [%s], type something to change, \"enter\" to skip, \"-\" to delete" % game_a.releases[int(entry2)-1].kana)
						entry4  = input(">>: ")
						if entry4 != "":
							if entry4 == "-":
								game_a.releases[int(entry2)-1].kana = ""
							else: 
								game_a.releases[int(entry2)-1].kana =  entry4

						print("Title in kanji: [%s], type something to change, \"enter\" to skip, \"-\" to delete" % game_a.releases[int(entry2)-1].kanji)
						entry4  = input(">>: ")
						if entry4 != "":
							if entry4 == "-":
								game_a.releases[int(entry2)-1].kanji = ""
							else: 
								game_a.releases[int(entry2)-1].kanji =  entry4

					if (game_a.releases[int(entry2)-1].region == "South Korea"):
						
						print("Title in hangul: [%s], type something to change, \"enter\" to skip, \"-\" to delete" % game_a.releases[int(entry2)-1].hangul)
						entry4  = input(">>: ")
						if entry4 != "":
							if entry4 == "-":
								game_a.releases[int(entry2)-1].hangul = ""
							else: 
								game_a.releases[int(entry2)-1].hangul =  entry4

					if (game_a.releases[int(entry2)-1].region == "Taiwan"):
						
						print("Title in hanzi: [%s], type something to change, \"enter\" to skip, \"-\" to delete" % game_a.releases[int(entry2)-1].hanzi)
						entry4  = input(">>: ")
						if entry4 != "":
							if entry4 == "-":
								game_a.releases[int(entry2)-1].hanzi = ""
							else: 
								game_a.releases[int(entry2)-1].hanzi =  entry4

				elif entry3 == "d":
					game_a.releases.pop(int(entry2)-1)


	elif entry == "q":
		editing = False
			


sys.exit()

