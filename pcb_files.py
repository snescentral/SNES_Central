#!/usr/bin/env python3

import csv
import sys
import os.path
import copy
import operator
import datetime
import filecmp
from shutil import copyfile

game_info = open("SNES_Central/info.csv", "r")

pcb_info = open("all_pcbs.txt", "r")

game_info_csv = csv.DictReader(game_info, delimiter='\t')
pcb_info_csv = csv.DictReader(pcb_info, delimiter='|')


# make empty lists

game_id = []
game_title = []
game_US = []
game_Japan = []
game_PAL = []

for row in game_info_csv:

    game_id.append(row["page ID"])
    game_title.append(row["Title"])
    game_US.append(row["US Title"])
    game_Japan.append(row["Japanese Title"])
    game_PAL.append(row["PAL Title"])



# make empty lists

pcb_id = []
pcb_title = []
pcb_region = []
pcb_type = []
pcb_date = []
pcb_date_no_proto = []
pcb_serial = []

pcb_full = []



current_index = 0

USA_count = 0
Japan_count = 0
PAL_count = 0
France_count = 0
Germany_count = 0
Netherlands_count = 0
Spain_count = 0
Italy_count = 0
other_count = 0

for row in pcb_info_csv:

#    print row
	pcb_id.append(row["page_id"])
	pcb_region.append(row["region"])
	pcb_type.append(row["pcb_type"])
	pcb_date.append(row["date_stamp"])
	pcb_serial.append(row["serial_code"])

	id_match = game_id.index(row["page_id"])

	if row["region"] == "USA":
		title = game_US[id_match]
		USA_count = USA_count + 1
	elif row["region"] == "Japan":
		title = game_Japan[id_match]
		Japan_count = Japan_count + 1
	elif row["region"] == "PAL":
		title = game_PAL[id_match]
		PAL_count = PAL_count + 1
	elif row["region"] == "France":
		title = game_PAL[id_match]
		France_count = France_count + 1
	elif row["region"] == "Germany":
		title = game_PAL[id_match]
		Germany_count = Germany_count + 1
	elif row["region"] == "Netherlands":
		title = game_PAL[id_match]
		Netherlands_count = Netherlands_count + 1
	elif row["region"] == "Spain":
		title = game_PAL[id_match]
		Spain_count = Spain_count + 1
	elif row["region"] == "Italy":
		title = game_PAL[id_match]
		Italy_count = Italy_count + 1
	else:
		title = game_title[id_match]
		other_count = other_count + 1
		print("Other region detected: " + row["region"] + " " + str(id_match))

	if row["serial_code"] != "Prototype":
		pcb_date_no_proto.append(row["date_stamp"])

	pcb_title.append(title)

	all_info =  (pcb_id[current_index], title, pcb_region[current_index], pcb_type[current_index], pcb_date[current_index], pcb_serial[current_index])

	#    if pcb_type[current_index] == "D411A":
	#	print pcb_id[current_index]

	pcb_full.append(all_info)

	current_index = current_index + 1

#for item in pcb_full:
#    print item[0]
pcb_type_backup = pcb_type[:]
pcb_type.sort()

unique_type = []

last = "s"

for item in pcb_type:

	if item != last:
		unique_type.append(item)
		last=item

print ("Unknown PCB boards:")

for item in unique_type:
	file_name = "SNES_Central/chips/"+item
	if not os.path.isfile(file_name):

		index_number = pcb_type_backup.index(item)
		id_value = pcb_id[index_number]
		print(id_value, pcb_type_backup[index_number])

print("\n")
# check if the pcb scan image is linked properly
print("Checking PCB board scan links")
pcb_list_file = open("SNES_Central/chips/pcblist.txt","w+")
for item in unique_type:
	pcb_list_file.write(item+"\n")
	file_name = "SNES_Central/chips/"+item
	if os.path.isfile(file_name):
		pcb_info_file = open(file_name, "r")

		for i, line in enumerate(pcb_info_file):
			if i == 1:
                        
				image_file = "SNES_Central/"+line.rstrip()
				if not os.path.exists(image_file):
					print("bad link: " + item)
					print(image_file)

pcb_list_file.close()

print("\n")
pcb_full.sort(key=operator.itemgetter(3,2,1))

pcb_reduced = []

last_name = ""
last_region = ""
last_pcb_type = ""
last_serial = ""

for item in pcb_full:

	if item[1] != last_name or item[2] != last_region or item[3] != last_pcb_type or item[5] != last_serial:
		last_name = item[1]
		last_region = item[2]
		last_pcb_type = item[3]
		last_serial = item[5]
		pcb_reduced.append(item)

last_pcb_type=""
out_file = ""
for item in pcb_reduced:
	if item[3] != last_pcb_type:
		last_pcb_type = item[3]
		temp_pcb_list_file = "temp/" + last_pcb_type + "_games"
		out_file = open(temp_pcb_list_file, "w+")

	out_file.write(item[1] + "|" + item[0] + "|" + item[2] + "|" + item[5] + "|\n")

out_file.close()
# move the file if it is changed and report
last_pcb_type=""
for item in pcb_reduced:
	if item[3] != last_pcb_type:
		last_pcb_type = item[3]
		print(last_pcb_type)
		temp_pcb_list_file = "temp/" + last_pcb_type + "_games"
		pcb_list_file = "SNES_Central/chips/games/" + last_pcb_type + "_games"

		if os.path.isfile(pcb_list_file):
			if not filecmp.cmp(temp_pcb_list_file, pcb_list_file):
				print("changed file: " + last_pcb_type)
				copyfile(temp_pcb_list_file, pcb_list_file)
		else:
			copyfile(temp_pcb_list_file, pcb_list_file)

# write out some stats
pcb_stats_file = "SNES_Central/chips/stats.html" 
out_file = open(pcb_stats_file, "w+")

date_stats = [0,0,0,0,0,0,0,0,0,0,0,0,0]
for item in pcb_date_no_proto:
	if item[:2] == "89":
		date_stats[0] = date_stats[0] + 1
	if item[:2] == "90":
		date_stats[1] = date_stats[1] + 1
	if item[:2] == "91":
		date_stats[2] = date_stats[2] + 1
	if item[:2] == "92":
		date_stats[3] = date_stats[3] + 1
	if item[:2] == "93":
		date_stats[4] = date_stats[4] + 1
	if item[:2] == "94":
		date_stats[5] = date_stats[5] + 1
	if item[:2] == "95":
		date_stats[6] = date_stats[6] + 1
	if item[:2] == "96":
		date_stats[7] = date_stats[7] + 1
	if item[:2] == "97":
		date_stats[8] = date_stats[8] + 1
	if item[:2] == "98":
		date_stats[9] = date_stats[9] + 1
	if item[:2] == "99":
		date_stats[10] = date_stats[10] + 1
	if item[:2] == "00":
		date_stats[11] = date_stats[11] + 1
	if item[:2] == "01":
		date_stats[12] = date_stats[12] + 1

current_date = datetime.datetime.today().strftime('%Y-%m-%d')

out_file.write("<p class=\"name\">Stats</p>\n")
out_file.write("<p><b>Number of documented PCBs:</b> " + str(current_index) + " (as of " +current_date + ")</p>\n")
out_file.write("<table><tr>\n")
out_file.write("<td valign=\"top\", width=\"200px\"><p>Year manufactured:</p>\n")
out_file.write("<ul>\n")

year_counter = 1989

for date_number in date_stats:
	if date_number > 0:
		out_file.write("<li><b>" + str(year_counter) + ":</b> " + str(date_number) + "</li>\n")
	year_counter = year_counter + 1

out_file.write("</ul></td>\n")

out_file.write("<td valign=\"top\"><p>Regions:</p>\n")
out_file.write("<ul>\n")
out_file.write("<li><b>USA:</b> " + str(USA_count) + "</li>\n")
out_file.write("<li><b>Japan:</b> " + str(Japan_count) + "</li>\n")
out_file.write("<li><b>PAL:</b> " + str(PAL_count) + "</li>\n")
out_file.write("<li><b>Germany:</b> " + str(Germany_count) + "</li>\n")
out_file.write("<li><b>France:</b> " + str(France_count) + "</li>\n")
out_file.write("<li><b>Netherlands:</b> " + str(Netherlands_count) + "</li>\n")
out_file.write("<li><b>Spain:</b> " + str(Spain_count) + "</li>\n")
out_file.write("<li><b>Italy:</b> " + str(Italy_count) + "</li>\n")

out_file.write("<li><b>Other:</b> " + str(other_count) + "</li>\n")

out_file.write("</ul></td>\n")
out_file.write("</tr></table>\n")

out_file.write("<p><b>Number of PCB types:</b> " +str(len(unique_type)) + "</p>\n")
