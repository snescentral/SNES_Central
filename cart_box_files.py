#!/usr/bin/env python3

import csv
import sys
import os.path
import copy
import operator
import datetime
import filecmp
import shutil
from shutil import copyfile

countries = {"USA": ["USA", 0, 0]}
countries["Canada"] = ["CAN", 0, 0]


countries["Latin America"] = ["LTN", 0, 0]
countries["Brazil"] = ["Brazil", 0, 0]
countries["Japan"] = ["JPN", 0, 0]
countries["South Korea"] = ["KOR", 0, 0]
countries["Hong Kong"] = ["HKG", 0, 0]
countries["Europe"] = ["EUR", 0, 0]
countries["Germany (Nintendo of Europe)"] = ["NOE", 0, 0]
countries["Germany, Austria, Switzerland"] = ["FRG", 0, 0]
countries["France and Netherlands"] = ["FAH", 0, 0]
countries["Netherlands"] = ["HOL", 0, 0]
countries["Australia"] = ["AUS", 0, 0]
countries["Spain"] = ["ESP", 0, 0]
countries["United Kingdom"] = ["UKV", 0, 0]
countries["Italy"] = ["ITA", 0, 0]
countries["Scandinavia"] = ["SCN", 0, 0]
countries["France"] = ["FRA", 0, 0]
countries["European Economic Community"] = ["EEC", 0, 0]


game_info = open("SNES_Central/info.csv", "r")


game_info_csv = csv.DictReader(game_info, delimiter='\t')


# make empty lists





total_carts = 0

all_regions = ["Americas", "Asia", "PAL"]

all_carts = []
cart_fieldnames = ["filename", "contributor", "description", "region", "country", "serial code", "notes"]
for row in game_info_csv:

	game_id = row["page ID"]
	game_title = row["Title"]
	game_US = row["US Title"]
	game_Japan= row["Japanese Title"]
	game_PAL = row["PAL Title"]

	game_path = "SNES_Central/" + game_id[0] + '/' + game_id[1] + '/' + game_id[2] + '/' + game_id

	cart_file=game_path + "/carts2.txt"
	box_file = game_path + "/box2.txt"

	if os.path.isfile(cart_file):
		cart_info = open(cart_file, "r")
		cart_info_csv = csv.DictReader(cart_info, fieldnames=cart_fieldnames, delimiter='|')
		counter = 0
		for cart_row in cart_info_csv:

			country = cart_row["country"]
			region = cart_row["region"]
			


			if region == "PAL":				
				title = row["PAL Title"]
			elif region == "Americas":
				title = row["US Title"]
			elif region == "Asia":
				title = row["Japanese Title"]
			else:
				print(game_id + " Unknown region (cart): " + region)

			if cart_row["filename"] == "":
				scan = "n"
			else:
				scan = "y"

			if country in countries:

				combined = []
				combined.append(country)
				combined.append(title)
				combined.append(cart_row["serial code"])
				combined.append(cart_row["notes"])
				combined.append(counter)
				combined.append(scan)
				combined.append(game_id)

				all_carts.append(combined)

				countries[country][1] = countries[country][1] + 1
				total_carts = total_carts + 1
			else:
				print(game_id + " Unknown country: " + country)

			counter = counter + 1

		cart_info.close()

total_boxes = 0
box_fieldnames = ["filename", "contributor", "description", "region", "country", "serial code", "notes"]

all_boxes = []
game_info.seek(0)
for row in game_info_csv:

	game_id = row["page ID"]
	game_title = row["Title"]
	game_US = row["US Title"]
	game_Japan= row["Japanese Title"]
	game_PAL = row["PAL Title"]

	game_path = "SNES_Central/" + game_id[0] + '/' + game_id[1] + '/' + game_id[2] + '/' + game_id

	box_file = game_path + "/box2.txt"

	if os.path.isfile(box_file):
		box_info = open(box_file, "r")
		box_info_csv = csv.DictReader(box_info, fieldnames=box_fieldnames, delimiter='|')
		counter = 0
		for box_row in box_info_csv:

			country = box_row["country"]
			region = box_row["region"]
			

			if region == "PAL":				
				title = row["PAL Title"]
			elif region == "Americas":
				title = row["US Title"]
			elif region == "Asia":
				title = row["Japanese Title"]
			else:
				print(game_id + " Unknown region (box): " + region)

			if box_row["filename"] == "":
				scan = "n"
			else:
				scan = "y"

			if country in countries:

				combined = []
				combined.append(country)
				combined.append(title)
				combined.append(box_row["serial code"])
				combined.append(box_row["notes"])
				combined.append(counter)
				combined.append(scan)
				combined.append(game_id)

				all_boxes.append(combined)
				countries[country][2] = countries[country][2] + 1
				total_boxes = total_boxes + 1
			else:
				print(game_id + " Unknown country: " + country)


			counter = counter + 1

		box_info.close()

game_info.close()

all_carts.sort(key=operator.itemgetter(0,1,2))
#for carts in all_carts:
#	print(carts)

all_boxes.sort(key=operator.itemgetter(0,1,2))
#for boxes in all_boxes:
#	print(boxes)

print("Cart stats:")
print("Total carts: " + str(total_carts))
#for row in countries:
#	print (row + ": " + str(countries[row][1]))
print(" ")
print("Box stats:")
print("Total boxes: " + str(total_boxes))
#for row in countries:
#	print (row + ": " + str(countries[row][2]))


index_file = "SNES_Central/cart_box/summary.html"
out_file = open(index_file, "w+")

current_date = datetime.datetime.today().strftime('%Y-%m-%d')

out_file.write("<p class=\"news\">Cartridge Label and Box Scans</p>\n")
out_file.write("<p><b>Last updated:</b> " + current_date + "</p>\n")
out_file.write("<p><b>Total label scans:</b> " + str(total_carts) + "<br>\n")

out_file.write("<b>Total box scans:</b> " + str(total_boxes) + "</p>\n")


#out_file.write("<p class="\"name\">USA (region code: USA)</p>\n")

#	out_file.write("<p class=\"name\">Cartridge label information</p>\n")
out_file.write("<table  class=\"infotable2\">\n")
out_file.write("	<tr  class=\"row2\">\n")
out_file.write("\n")
out_file.write("		<th style=\"width : 270px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Region</th>\n")
out_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Code</th>\n")
out_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\"># Cart Scans</th>\n")
out_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Cart scan list</th>\n")
out_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\"># Box Scans</th>\n")
out_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Box scan list</th>\n")
out_file.write("	</tr>\n")

counter = 0

for country in  sorted(countries.iterkeys()):

	if  counter%2 == 0:
		row = "row1"
	else:
		row = "row2"

	out_file.write("	<tr class = " + row + ">\n")
	out_file.write("\n")
	out_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + country + "</td>\n")
	out_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + countries[country][0] + "</td>\n")


	out_file.write("		<td style=\"text-align: center; vertical-align: top;\">" + str(countries[country][1]) + "</td>\n")

	if countries[country][1] > 0:
		out_file.write("		<td style=\"text-align: center; vertical-align: top;\"><a href=\"scanlist.php?region=" + countries[country][0] + "&type=cart\">link</a></td>\n")

		temp_cart_html = "temp/" + countries[country][0] + "_cart.html"
		final_cart_html = "SNES_Central/cart_box/" + countries[country][0] + "_cart.html"

		temp_cart_file = open(temp_cart_html, "w+")
		temp_cart_file.write("<p class=\"news\">Cartridge label scans</p>\n")
		temp_cart_file.write("<p style=\"font-size: 24px;\">" + country + " (" + countries[country][0] + ")</p>\n")
		temp_cart_file.write("<table  class=\"infotable\">\n")
		temp_cart_file.write("	<tr  class=\"row2\">\n")
		temp_cart_file.write("\n")
		temp_cart_file.write("		<th style=\"width : 260px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Game name</th>\n")
		temp_cart_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Serial code</th>\n")
		temp_cart_file.write("		<th style=\"width : 260px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Notes</th>\n")
		temp_cart_file.write("		<th style=\"width : 50px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Link</th>\n")
		temp_cart_file.write("	</tr>\n")
		counter2 = 0
		for entry in all_carts:

			if entry[0] == country:
				if  counter2%2 == 0:
					row_a = "row1"
				else:
					row_a = "row2"

				temp_cart_file.write("	<tr class = " + row_a + ">\n")
				temp_cart_file.write("\n")
				temp_cart_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[1] + "</td>\n")
				temp_cart_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[2] + "</td>\n")
				temp_cart_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[3] + "</td>\n")
				if entry[5] == "y":
					temp_cart_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"cart.php?id=" + str(entry[6]) + "&num=" + str(entry[4]) + "\">link</a></td>\n")
				else:

					temp_cart_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
				temp_cart_file.write("	</tr>\n")

				counter2 = counter2 + 1


		temp_cart_file.write(" </table>\n")
		temp_cart_file.write("		<p><i>Do you have a label variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1097\">Submission guidelines</a> for cartridge label scans.</p>\n")
		temp_cart_file.close()

		if os.path.isfile(final_cart_html):
			cart_same = filecmp.cmp(temp_cart_html, final_cart_html)
		else:
			shutil.move(temp_cart_html,final_cart_html)
			cart_same = True
		if not cart_same:
			print ("the carts html file is different for region: " + country)
			copyfile(temp_cart_html, final_cart_html)

	else:
		out_file.write("		<td style=\"text-align: center; vertical-align: top;\">-</td>\n")

	out_file.write("		<td style=\"text-align: center; vertical-align: top;\">" + str(countries[country][2]) + "</td>\n")
	if countries[country][2] > 0:
		out_file.write("		<td style=\"text-align: center; vertical-align: top;\"><a href=\"scanlist.php?region=" + countries[country][0] + "&type=box\">link</a></td>\n")


		temp_box_html = "temp/" + countries[country][0] + "_box.html"
		final_box_html = "SNES_Central/cart_box/" + countries[country][0] + "_box.html"

		temp_box_file = open(temp_box_html, "w+")
		temp_box_file.write("<p class=\"news\">Box scans</p>\n")
		temp_box_file.write("<p style=\"font-size: 24px;\">" + country + " (" + countries[country][0] + ")</p>\n")
		temp_box_file.write("<table  class=\"infotable\">\n")
		temp_box_file.write("	<tr  class=\"row2\">\n")
		temp_box_file.write("\n")
		temp_box_file.write("		<th style=\"width : 250px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Game name</th>\n")
		temp_box_file.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Serial code</th>\n")
		temp_box_file.write("		<th style=\"width : 240px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Notes</th>\n")
		temp_box_file.write("		<th style=\"width : 50px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Front</th>\n")
		temp_box_file.write("		<th style=\"width : 50px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Back</th>\n")
		temp_box_file.write("	</tr>\n")
		counter2 = 0
		for entry in all_boxes:

			if entry[0] == country:
				if  counter2%2 == 0:
					row_a = "row1"
				else:
					row_a = "row2"

				temp_box_file.write("	<tr class = " + row_a + ">\n")
				temp_box_file.write("\n")
				temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[1] + "</td>\n")
				temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[2] + "</td>\n")
				temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + entry[3] + "</td>\n")
				if entry[5] == "y":
					temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"box.php?id=" + str(entry[6]) + "&num=" + str(entry[4]) + "&side=front\">link</a></td>\n")
					temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"box.php?id=" + str(entry[6]) + "&num=" + str(entry[4]) + "&side=back\">link</a></td>\n")
				else:

					temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
					temp_box_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
				temp_box_file.write("	</tr>\n")

				counter2 = counter2 + 1


		temp_box_file.write(" </table>\n")
		temp_box_file.write("		<p><i>Do you have a box variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1098\">Submission guidelines</a> for box scans.</p>\n")
		temp_box_file.close()

		if os.path.isfile(final_box_html):
			box_same = filecmp.cmp(temp_box_html, final_box_html)
		else:
			shutil.move(temp_box_html,final_box_html)
			box_same = True
		if not box_same:
			print ("the box html file is different for region: " + country)
			copyfile(temp_box_html, final_box_html)






	else:
		out_file.write("		<td style=\"text-align: center; vertical-align: top;\">-</td>\n")

	out_file.write("	</tr>\n")
	counter = counter + 1
out_file.write(" </table>\n")

out_file.close()

