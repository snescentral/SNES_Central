#!/usr/bin/env python3

import yaml
import datetime
import itertools
import os
import filecmp
import shutil
import time
import sys

class chips:

	def __init__(self):

		# this lists all the possible fields in a chip object
		'''
		  'chiptype_id' 	# general chip type
		  'chipID'		# manufacturer label for the chip
		  'fullChipID'	# manufacturer label in full, which can have extra stuff on it
		  'size'		# only used for memory chips
		  'manufacturer'	# manufactuer of the chip, if it is known
		  'gameSerial'	# Serial code on the chip, if it is a ROM chip
		  'dateCode'	# written out code for the date stamp
		  'dateFormat'	# format code for the date stamp, see the date_stamp() function
		  'fileName'	# file name for the image of the chip, should be in a folder chiptype/fileName
              'chipNumber'    # Some chips have a label for which chip it is on a multi-ROM board
              'unknown'    # some chips have a label with unknown purpose
              'gameTitle' # some chips have the game title on it
              'writtenLabel' # written label on the EPROM window
		'''

		self.chip = { 
		  'chiptype_id' : '', 
		  'chipID' : '', 
		  'fullChipID' : '', 
		  'size' : '', 
		  'manufacturer' : '', 
		  'gameSerial' : '',
		  'dateCode' : '',
		  'dateFormat' : '', 
		  'fileName' : '',
		  'chipNumber' : '',
		  'unknown' : '',
              'gameTitle': '',
              'writtenLabel': ''
		}

		self.chip_description = { 
		  'chiptype' : 'Chip Type (i.e. MASKROM, Super FX)', 
		  'chipID' : 'Part #', 
		  'fullChipID' : 'Full Part # on chip', 
		  'size' : 'chip size (in hexidecimal)', 
		  'manufacturer' : 'Chip manufacturer', 
		  'gameSerial' : 'Game serial code',
		  'dateCode' : 'Date code in full',
		  'dateFormat' : 'Date format code number (see chips.py)', 
		  'fileName' : 'filename of the image of the chip',
              'chipNumber' : 'label to indicate the ROM chip number (leave blank if none)',
              'unknown' : 'label with unknown purpose',
              'gameTitle': 'Game title as stated on the chip',
              'writtenLabel': 'written label on the EPROM window'
		}

		self.chip_description_short = { 
		  'chiptype' : 'Chip type', 
		  'chipID' : 'Part #', 
		  'fullChipID' : 'Full part #', 
		  'size' : 'Size', 
		  'manufacturer' : 'Manufacturer', 
		  'gameSerial' : 'Serial code',
		  'dateCode' : 'Date code',
		  'dateFormat' : 'Date format',
		  'chipNumber' : 'Chip #',
              'unknown' : 'unknown label',
              'gameTitle': 'Game title',
              'writtenLabel': 'written label'
		}



		self.chiptype_id_values = []
		# load yaml file
		self.filename = os.path.join(os.path.dirname(__file__),"chip_description.yml")
		file = open(self.filename, 'r')
		self.datastore = yaml.load_all(file, Loader=yaml.FullLoader)
		i=0
		for loadchips in self.datastore:
			for entries in loadchips:
				self.chiptype_id_values.insert(i, entries.copy())
				i+=1


		self.yearNumber = ''
		self.weekNumber = ''
		self.allchips = []
		# load yaml file
		self.filename = os.path.join(os.path.dirname(__file__),"chips.yml")
		file = open(self.filename, 'r')
		self.datastore = yaml.load_all(file, Loader=yaml.FullLoader)
		i=0
		for loadchips in self.datastore:
			for entries in loadchips:
				self.allchips.insert(i, entries['chip'].copy())
				i+=1
		self.number_of_chips = i-1


	#return chiptype

	def return_chiptype(self, chiptype_id):

		chiptype = ''
		found = False
		for chiptype_id_value in self.chiptype_id_values:
			if chiptype_id == chiptype_id_value['chiptype_id']:
				chiptype = chiptype_id_value['chiptype']
				found = True
				break

		return found, chiptype

	# clears entry
	def clear_entry(self):
		
		for item in self.chip:
			self.chip[item] = ''


	# loads the information related to the specified part # (id)
	def load_chip(self,id):


		found = False
		for i in range(len(self.allchips)):

			if id == self.allchips[i]['chipID']:
				self.chip.update(self.allchips[i])
				found = True
				break

		return found

#	def return_chiptype(self,chiptype_id):
	
#		chiptype = ''

#		for chiptype_info in self.chiptype_id_values:
#			if chiptype_id == chiptype_info['chiptype_id']:
#				chiptype = chiptype_info['chiptype']
#				break

#		return chiptype


	def select_chip(self,chiptype_id=''):
		if chiptype_id == '':
			print("Enter chiptype_id")
			chiptype_id = input(">>> ")

		chiptype_id = int(chiptype_id)

		found, chiptype = self.return_chiptype(chiptype_id)

		print(f"Chips labeled as {chiptype}:")

		found = False
		i = 0
		for chip in self.allchips:
			if chiptype_id == chip['chiptype_id']:
				found = True
				i = i + 1
				print(f"- {chip['chipID']}")

		if found:

			print("Enter the name of the chip you want")
			chipID = input(">>> ")

			self.load_chip(chipID)

		else:
			print("none found, you must add it first!")

			sys.exit()

	# When called, it will ask the user to input the information written on the chip
	def read_chip_info(self):

		print("Current chip: " + self.chip['chipID'])
		for entries in self.chip:
			if 'dummy' in str(self.chip[entries]):

				if entries == 'dateCode':
					dateFormat = self.date_format_types()
					print(f"Date format is: {dateFormat}")

				userEntry = input("Enter " + self.chip_description[entries] + ": ")
				self.chip[entries] = userEntry

		# convert date code
		found = False
		self.yearNumber, self.weekNumber, found = self.date_format(self.chip['dateCode'], self.chip['dateFormat'])

		if not found:
			print("warning, invalid dateFormat")


	def return_four_date(self):

		found = False
		self.yearNumber, self.weekNumber, found = self.date_format(self.chip['dateCode'], self.chip['dateFormat'])

		if found:
			four_date = self.yearNumber + self.weekNumber
		else:
			four_date = "----"

		return four_date


	# this function converts the date code to a standard YY and MM that can be input into 
	# returns false if the code is not found
	def date_format(self, dateCode, dateFormat):


		year = '0'
		week = '0'
		found = False

		# YYWW
		if (dateFormat == 1):
			year = str(dateCode)[:2]
			week = str(dateCode)[-2:]
			found = True
		# YYWWxxxxx
		if (dateFormat == 2):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True
		# YYWW xxx
		if (dateFormat == 3):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True

		# CYWW xxx
		if (dateFormat == 4):
			year = "9" + str(dateCode)[1:2]
			week = str(dateCode)[2:4]
			found = True

		# YMxxx
		if (dateFormat == 5):
			year = "9" + str(dateCode)[0:1]
			month = str(dateCode)[1:2]
			if (month == "A"):
				month = 10
			elif (month == "B"):
				month = 11
			elif (month == "C"):
				month = 12
			week = self.get_week_number(year, month, 1)
			found = True

		# xYWWxxxxx
		if (dateFormat == 6):
			year = "9" + str(dateCode)[1:2]
			week = str(dateCode)[2:4]
			found = True

		# YYWWxx
		if (dateFormat == 7):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True

		# YWWxxxx
		if (dateFormat == 8):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# YWW xxx
		if (dateFormat == 9):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# YWW xxx xxx
		if (dateFormat == 10):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# xYWW xx xx
		if (dateFormat == 11):
			year = "9" + str(dateCode)[1:2]
			week = str(dateCode)[2:4]
			found = True
		# YYWWx
		if (dateFormat == 12):
			year = str(dateCode)[0:2]
			week = str(dateCode)[2:4]
			found = True


		# xYMx xxx
		if (dateFormat == 13):
			year = "9" + str(dateCode)[1:2]
			month = str(dateCode)[2:3]
			if (month == "A"):
				month = 10
			elif (month == "B"):
				month = 11
			elif (month == "C"):
				month = 12
			week = self.get_week_number(year, month, 1)
			found = True

		# YWW xx
		if (dateFormat == 14):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# xYWW xx x
		if (dateFormat == 15):
			year = "9" + str(dateCode)[1:2]
			week = str(dateCode)[2:4]
			found = True

		# YMxx
		if (dateFormat == 16):
			year = "9" + str(dateCode)[0:1]
			month = str(dateCode)[1:2]
			if (month == "A"):
				month = 10
			elif (month == "B"):
				month = 11
			elif (month == "C"):
				month = 12
			week = self.get_week_number(year, month, 1)
			found = True

			week = self.get_week_number(year, month, 1)
			found = True
		# YYWW xx
		if (dateFormat == 17):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True

		# YMW x - Hitachi format
		if (dateFormat == 18):
			year =  "9" + str(dateCode)[:1]
			month = str(dateCode)[1:2]

			if (month == "A"):
				month = 1
			elif (month == "B"):
				month = 2
			elif (month == "C"):
				month = 3
			elif (month == "D"):
				month = 4
			elif (month == "E"):
				month = 5
			elif (month == "F"):
				month = 6
			elif (month == "G"):
				month = 7
			elif (month == "H"):
				month = 8
			elif (month == "J"):
				month = 9
			elif (month == "K"):
				month = 10
			elif (month == "L"):
				month = 11
			elif (month == "M"):
				month = 12

			week = self.get_week_number(year, month, 1)
			week = str( int(week) + int(str(dateCode)[2:3]) - 1) 
			found = True

		# YYWW xxxx
		if (dateFormat == 19):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True

		# Panasonic format YMTDxx
		if (dateFormat == 20):
			year = "9" + str(dateCode)[0:1]
			month = str(dateCode)[1:2]
			if (month == "O"):
				month = 10
			elif (month == "N"):
				month = 11
			elif (month == "D"):
				month = 12
			daystring = str(dateCode)[2:3]
			if (daystring == "_"):
				day = 21
			elif (daystring == " "):
				day = 11
			elif (daystring == "-"):
				day = 1
			day = day + int(str(dateCode)[3:4])
			week = self.get_week_number(year, month, day)

			found = True

		# xYYWW-x
		if (dateFormat == 21):
			year = str(dateCode)[1:3]
			week = str(dateCode)[3:5]
			found = True

		# YWW
		if (dateFormat == 22):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# YYWWxxx x
		if (dateFormat == 23):
			year = str(dateCode)[:2]
			week = str(dateCode)[2:4]
			found = True


		# xYYWW
		if (dateFormat == 24):
			year = str(dateCode)[1:3]
			week = str(dateCode)[3:5]
			found = True
		# YWWx
		if (dateFormat == 25):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		# YWW xxxxx
		if (dateFormat == 26):
			year = "9" + str(dateCode)[0:1]
			week = str(dateCode)[1:3]
			found = True

		return year, week, found


	# returns a text string describing the date format
	def date_format_types(self):
		format_text = ""
		# YYWW
		if (self.chip['dateFormat'] == 1):
			format_text = "YYWW"
		# YYWWxxxxx
		if (self.chip['dateFormat'] == 2):
			format_text = "YYWWxxxxx"
		# YYWW xxx
		if (self.chip['dateFormat'] == 3):
			format_text = "YYWW xxx"
		# CYWW xxx
		if (self.chip['dateFormat'] == 4):
			format_text = "CYWW xxx"
		# YMxxx
		if (self.chip['dateFormat'] == 5):
			format_text = "YMxxx"
		# xYWWxxxxx
		if (self.chip['dateFormat'] == 6):
			format_text = "xYWWxxxxx"
		# xYWWxxxxx
		if (self.chip['dateFormat'] == 7):
			format_text = "xYWWxx"
		# YWWxxxx
		if (self.chip['dateFormat'] == 8):
			format_text = "YWWxxxx"
		# YWW xxx
		if (self.chip['dateFormat'] == 9):
			format_text = "YWW xxx"
		# YWW xxx xxx
		if (self.chip['dateFormat'] == 10):
			format_text = "YWW xxx xxx"
		# xYWW xx xx
		if (self.chip['dateFormat'] == 11):
			format_text = "xYWW xx xx"
		# YYWWx
		if (self.chip['dateFormat'] == 12):
			format_text = "YYWW x"

		# xYMx xxx
		if (self.chip['dateFormat'] == 13):
			format_text = "xYMx xxx"

		# YWW xx
		if (self.chip['dateFormat'] == 14):
			format_text = "YWW xx"

		# xYWW xx x
		if (self.chip['dateFormat'] == 15):
			format_text = "xYWW xx x"
		# YMxx
		if (self.chip['dateFormat'] == 16):
			format_text = "YMxx"
		# YYWW xx
		if (self.chip['dateFormat'] == 17):
			format_text = "YYWW xx"
		# YMW x
		if (self.chip['dateFormat'] == 18):
			format_text = "YMW x"
		# YYWW xxxx
		if (self.chip['dateFormat'] == 19):
			format_text = "YYWW xxxx"

		# Panasonic format YMTDxx
		if (self.chip['dateFormat'] == 20):
			format_text = "YMTDxx (Panasonic, \"T\" location should be _, - or blank)"
		# xYYWW-x
		if (self.chip['dateFormat'] == 21):
			format_text = "xYYWW-x"

		# YWW
		if (self.chip['dateFormat'] == 22):
			format_text = "YWW"

		# YYWWxxx x
		if (self.chip['dateFormat'] == 23):
			format_text = "YYWWxxx x"

		# xYYWW
		if (self.chip['dateFormat'] == 24):
			format_text = "xYYWW"
		# YWWx
		if (self.chip['dateFormat'] == 25):
			format_text = "YWWx"

		# YWW xxxxx
		if (self.chip['dateFormat'] == 26):
			format_text = "YWW xxxxx"

		return format_text

	# in some cases, the date format is given as year/month/date
	def get_week_number(self,year,month,day):
		iso_calendar_tuple = datetime.date(int(year), int(month), int(day)).isocalendar()
		return iso_calendar_tuple[1]

	# in some cases, the date format is given as year/month/week
	def get_week_number2(self,year,month,week):

		day = str((int(week)-1) * 7)
		iso_calendar_tuple = datetime.date(int(year), int(month), int(day)).isocalendar()
		return iso_calendar_tuple[1]

	# converts year/week number to YYYY-MM-DD
	def convert_iso_date(self, year, week_number):

		formatted_iso =  year +"-" + week_number + "-1"
		full_date = datetime.datetime.strptime(formatted_iso, "%y-%W-%w")
		tt = full_date.timetuple()

		return str(tt[0]) + "/" + str(tt[1]) + "/" + str(tt[2])

	# prints out the chip information for the main chip pages. Can be called without filling in information.
	def write_html_chip_page(self, slot_number=''):

		# not done
		html_all =            "		<div class=\"chip_table\">\n"
		html_all = html_all + "			<div class=\"chip_heading\">\n"

		if slot_number:
			html_all = html_all + "				<div class=\"chip_heading_text\">U" + str(slot_number) + "</div>\n"
		else:
			html_all = html_all + "				<div class=\"chip_heading_text\">" + self.chip['chipID'] + "</div>\n"
		html_all = html_all + "			</div>\n"
		html_all = html_all + "			<div class=\"chip_bulk\">\n"


		found, chiptype = self.return_chiptype(self.chip['chiptype_id'])

		html_all = html_all + "				<div class=\"chip_image\"><img src = \"chiplist/" + chiptype + "/" + self.chip['fileName'] + "\"></div>\n"
		html_all = html_all + "				<div class=\"chip_info\">\n"

		# output information
		myIterator = itertools.cycle([1,2])





		for chip_key, chip_description in self.chip_description_short.items():
			if chip_description:


				if chip_key == 'chiptype' and found:
					html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>"+ chip_description + "</b>: " + chiptype +"</div></div>\n"
				else:
					if self.chip[chip_key] and self.chip[chip_key] != 'dummy':
						
						if chip_key == 'size':
							html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>"+ chip_description + "</b>: " + "%#5x"% (self.chip[chip_key]) +"</div></div>\n"
						elif chip_key == 'dateFormat':
							html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>"+ chip_description + "</b>: " + self.date_format_types() +"</div></div>\n"
						elif chip_key == 'dateCode':
							html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>"+ str(chip_description) + "</b>: " + str(self.chip[chip_key]) + " &#8594; <b>" + self.return_four_date() + "</b></div></div>\n"
						else:
							html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>"+ str(chip_description) + "</b>: " + str(self.chip[chip_key]) +"</div></div>\n"







		# variable fields should be labeled "dummy" in the chips.yml file
		variable_string = ""
		for entries in self.chip:
			if 'dummy' in str(self.chip[entries]):
				if (variable_string): 
					variable_string = variable_string + ", " + self.chip_description_short[entries]
				else:
					variable_string = self.chip_description_short[entries]
		if (variable_string): 
			html_all = html_all + "					<div class=\"chip_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Variables</b>: <i>" + variable_string +"</i></div></div>\n"


		html_all = html_all + "				</div>\n"
		html_all = html_all + "			</div>\n"
		html_all = html_all + "		</div>\n"

		return html_all

	def make_pages(self):

		#chip_object = chips()

		SCpathHTML = "../SNES_Central/chiplist/html"
		if not os.path.exists(SCpathHTML):
			os.makedirs(SCpathHTML)

		listfile="temp/list.txt"
		openlist = open(listfile,"w")

		listHTMLfile="temp/list.html"
		openlistHTML = open(listHTMLfile,"w")

		myIterator = itertools.cycle([1,2])

		openlistHTML.write("		<p class=\"headingtext\">SNES Chip List</p>\n")
		openlistHTML.write("		<p>This is a list of chip types that appear on SNES PCBs. Note that this is very likely to be incomplete and there are going to be errors due to the lack of information online about them. This listing is very much influenced by bootgod's <a href=\"http://bootgod.dyndns.org:7777/\">NES Cart database</a>, and a lot of the information is derived from there. Many thanks to JAPAN SNES MANIA, who identified some of the manufacturers of these chips.</p>\n")
		openlistHTML.write("		<table class=\"infotable\">\n")
		openlistHTML.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
		openlistHTML.write("				<td width=\"150px\"><b>Chip Type</b></td>\n")
		openlistHTML.write("				<td width=\"450px\"><b>Description</b></td>\n")
		openlistHTML.write("				<td width=\"100px\"><b>Count</b></td>\n")
		openlistHTML.write("			</tr>\n")

		for entries in self.chiptype_id_values:

			# create chiplisting

			chiplisting = []
			for individual_chip in self.allchips:
				if individual_chip['chiptype_id'] == entries['chiptype_id']:
					chiplisting.append(individual_chip['chipID'])

			openlistHTML.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
			openlistHTML.write("				<td><a href=\"chips.php?chiptype=" + str(entries["chiptype"]) + "\">" + str(entries["chiptype"]) + "</a></td>\n")
			openlistHTML.write("				<td>" + entries["shortDescription"] + "</td>\n")
			openlistHTML.write("				<td>" + str(len(chiplisting)) + "</td>\n")
			openlistHTML.write("			</tr>\n")


			openlist.write(entries["chiptype"] + '\n')
			filename = "temp/" + entries["chiptype"] + ".html"
			openfile = open(filename,"w")

			openfile.write("		<p class=\"headingtext\">")
			openfile.write("		" + entries["chiptype"])
			openfile.write("		</p>\n")
			openfile.write("		<p>" + entries["description"] + "</p>\n")
			i = 0


			while i < len(chiplisting):
				self.clear_entry()
				#print(entries["chiplisting"][i])
				found = self.load_chip(chiplisting[i])
				if(found):

					openfile.write(self.write_html_chip_page())
					# check if the image file exists
					chipImageFile = entries["chiptype"] + "/" + self.chip["fileName"] 
					SCpath = "../SNES_Central/chiplist/" + entries["chiptype"]
					if not os.path.exists(SCpath):
					    os.makedirs(SCpath)
					chipImageFile2 = "../SNES_Central/chiplist/" + chipImageFile
					exists = os.path.isfile(chipImageFile)
					if not exists:
						print("file not found: " + self.chip["fileName"] )
					else:

						exists2 = os.path.isfile(chipImageFile2)
						if(exists2):
							# check if file is the same
							
							if not filecmp.cmp(chipImageFile,chipImageFile2):
								shutil.copyfile(chipImageFile,chipImageFile2)
						else:
							shutil.copyfile(chipImageFile,chipImageFile2)
							

				else:
					print("Did not find chip: " + entries["chiplisting"][i])
				i+=1

				
			openfile.close()
			# move the file to SNES Central

			SCfile = SCpathHTML + "/" + entries["chiptype"] + ".html"
			shallow = False

			exists = os.path.isfile(SCfile)
			if not exists:
				print("file change: " + entries["chiptype"] + ".html")
				shutil.copyfile(filename,SCfile)
			else:

				if not filecmp.cmp(filename,SCfile):
					print("file change: " + entries["chiptype"] + ".html")
					shutil.copyfile(filename,SCfile)

		openlistHTML.write("		</table>\n")
		openlistHTML.close()


		SClistfileHTML = "../SNES_Central/chiplist/list.html"
		exists = os.path.isfile(SClistfileHTML)
		if not exists:
			print("file change: list.html")
			shutil.copyfile(listHTMLfile,SClistfileHTML)
		else:
			if not filecmp.cmp(listHTMLfile,SClistfileHTML):
				print("file change: list.html")
				shutil.copyfile(listHTMLfile,SClistfileHTML)



		openlist.close()
		SClistfile = "../SNES_Central/chiplist/list.txt"

		exists = os.path.isfile(SClistfile)
		if not exists:
			print("file change: list.txt")
			shutil.copyfile(listfile,SClistfile)
		else:
			if not filecmp.cmp(listfile,SClistfile):
				print("file change: list.txt")
				shutil.copyfile(listfile,SClistfile)


if __name__ == "__main__":

	chip_object = chips()

	chip_object.make_pages()
	chip_object.select_chip()
	chip_object.read_chip_info()
	print(chip_object.chip)	
