#!/usr/bin/env python

import yaml
import sys
import os
import filecmp
import shutil

class contributor:
	
	def __init__(self):

		self.entry = {
		  'id_number' : '',
		  'name' : '',
		  'description' : '',
		  'email' : '',
		  'twitter' : '',
		  'twitch' : '',
		  'website' : '',
		}

		self.contributor_list_temp = []

		self.data_filename = os.path.join(os.path.dirname(__file__),"contributors.yml")

		self.allcontributors = []

		# load yaml file
		file = open(self.data_filename, 'r')
		self.datastore = yaml.load_all(file, Loader=yaml.FullLoader)
		i=0
		for loadentries in self.datastore:
			for entries in loadentries:
				self.allcontributors.insert(i, entries.copy())
				i+=1
		self.number_of_contributors = i-1

		file.close()

	# functions that allow you to modify the id values for a data entry

	def add_contributor_list(self, existing_list = []):

		if existing_list:
			self.contributor_list_temp = existing_list[:]

			temp_list = []

			for id_number in self.contributor_list_temp:
				self.load_contributor_id(id_number)
				temp_list.append(self.entry['name'])

			print(f"Current contributors: {temp_list}")
		else:
			self.contributor_list_temp = []

		done = False

		changed = False
		while not done:

			found = self.load_contributor()
			if found:
				self.contributor_list_temp.append(self.entry['id_number'])
				changed = True

				print("Do you want to add another contributor ('y' to continue):")

				response = input(">>> ")

				if response != "y":
					done = True
			else:
				done = True
		# remove duplicates
		trim_list = list(set(self.contributor_list_temp))

		return trim_list, changed

	def delete_contributor_list(self, exiting_list = []):

		if exiting_list:

			done = False

			while not done:

				self.print_contributor_list(existing_list)

				print("Enter the id number of the contributor you want to delete (f if finished):")

				response = input(">>> ")

				if response == 'f':
					done = True
				else:
					try:
						response = int(response)

						for i in range(len(existing_list)):
							if existing_list[i] == response:
								del existing_list[i]
								break
					
					except ValueError:
						print("Invalid id")



		return exiting_list


	def print_contributor_list(self, exiting_list = []):

		print("\nContributors:")
		if exiting_list:

			for contributor_id in exiting_list:

				self.load_contributor_id(contributor_id)
				print(f"{self.entry['id_number']}) {self.entry['name']}")
		else:
			print("There are no entries yet")

	##############################################################################
	# check if the id value exists
	##############################################################################

	def check_id(self, contributor_id):

		found = False
		current_entry = -1

		try:
			contributor_id = int(contributor_id)

			for i in range(len(self.allcontributors)):


				if self.allcontributors[i]['id_number'] == contributor_id:

					self.entry.update(self.allcontributors[i])
					found = True
					current_entry = contributor_id
		except ValueError: 
			found = False


		return found, current_entry

	##############################################################################
	# check if the name value exists
	##############################################################################

	def check_name(self, name_val):

		found = False


		for i in range(len(self.allcontributors)):


			if self.allcontributors[i]['name'] == name_val:

				found = True
				current_entry = self.allcontributors[i]['id_number']

		if not found:
			print (f"Contributor {namve_val} not found, would you like to add it? (y/n):")
			confirmation = input('>>: ')
			if confirmation == "y":
				self.add_contributor(name_val)
				found = True

		return found, current_entry

	##############################################################################
	# Get a list of contributor id values
	##############################################################################

	def get_id_list(self, id_list = []):

		print("\nEnter the contributors you want associated with this.")
		done = False
		while not done:

			


			self.print_contributor_list(id_list)
			print("\Enter an id number to delete, or enter name to add (enter for done):")
			contributor_val = input(">>> ")

			if contributor_val:
				found, current_entry = self.check_id(contributor_val)

				if found:

					id_list.remove(current_entry)
				else:
					found, current_entry = self.check_name(contributor_val)

					if found:
						id_list.append(current_entry)

			else:
				done = True

			print("\n")

		return id_list



	def delete_contributor_list(self, exiting_list = []):

		self.contributor_list_temp = exiting_list[:]

		counter = len(self.contributor_list_temp)
		if counter > 0:
			done = False
			counter -= 1
		else:
			done = True
		
		while not done:


			found = self.load_contributor_id(self.contributor_list_temp[counter])
			print(f"{self.contributor_list_temp[counter]}) {self.entry['name']}")
			print("Delete? (y for yes)")
			entry = input(">>> ")
			if entry == "y":
				del self.contributor_list_temp[counter]

			counter -=1

			if counter < 0:
				done = True

		return self.contributor_list_temp


	def clear_entry(self):
		
		for item in self.entry:
			self.entry[item] = ''

	def load_contributor(self, name=''):

		# find the contributor name

		if not name:
			print("Please enter the name of the contributor (enter for none):")
			name = input(">>> ")

		found = False
		for i in range(len(self.allcontributors)):

			if name == self.allcontributors[i]['name']:
				self.entry.update(self.allcontributors[i])
				found = True
				break

		if not found:

			if name != "":
				print ("Contributor " + name + "  not found, would you like to add it? (y/n):")
				confirmation = input('>>: ')
				if confirmation == "y":
					self.add_contributor(name)
					found = True
		return found


	def load_contributor_id(self, id_number):

		# find the contributor name

		found = False
		for i in range(len(self.allcontributors)):

			if id_number == self.allcontributors[i]['id_number']:
				self.entry.update(self.allcontributors[i])
				found = True
				break

		if not found:
			print ("id number: " + id_number + "  was not found")

		return found

	def return_link(self):
		html_out = "<a href=\"contributor.php?id=" + str(self.entry['id_number']) + "\">" + self.entry['name'] + "</a>"
		return html_out

	def add_contributor(self, name="None"):

		if name:
			self.entry['name'] = name
		else:
			print ("Enter name of contributor:")
			self.entry['name'] = input('>>: ')

		print ("Write a short description of the contributor:")
		self.entry['description'] = input('>>: ')

		print ("Enter the email address of the contributor:")
		self.entry['email'] = input('>>: ')

		print ("Enter Twitter username of the contributor:")
		self.entry['twitter'] = input('>>: ')

		print ("Enter Twitch username of the contributor:")
		self.entry['twitch'] = input('>>: ')

		print ("Enter website of the contributor:")
		self.entry['website'] = input('>>: ')

		self.number_of_contributors +=  1

		self.entry['id_number'] = self.number_of_contributors

		self.allcontributors.insert(self.number_of_contributors, self.entry.copy())


		with open(self.data_filename, 'w') as outfile:
    			yaml.dump(self.allcontributors, outfile, default_flow_style=False)

	def write_pages(self):

		if not os.path.exists("temp"):
			os.makedirs("temp")


		SCpath = "../SNES_Central/contributors"

		if not os.path.exists(SCpath):
			os.makedirs(SCpath)

		SCpathHTML = SCpath + "/html"
		if not os.path.exists(SCpathHTML):
			os.makedirs(SCpathHTML)

		fileout_check = 'temp/filecheck.txt'

		opened_checkfile = open(fileout_check,'w')

		for i in range(len(self.allcontributors)):

			opened_checkfile.write(str(self.allcontributors[i]['id_number']) + "\t" + self.allcontributors[i]['name'] + '\n')


			fileout = 'temp/' + str(self.allcontributors[i]['id_number']) + '.html'

			opened_file = open(fileout,'w')
			opened_file.write("<p class=\"headingtext\">" + self.allcontributors[i]['name'] + '</p>\n')
			opened_file.write("<p>" + self.allcontributors[i]['description'] + '</p>\n')

			if self.allcontributors[i]['email'] or self.allcontributors[i]['twitter'] or self.allcontributors[i]['twitch'] or self.allcontributors[i]['website']:

				opened_file.write('<ul>\n')
				if self.allcontributors[i]['email']:
					opened_file.write('	<li><a href="mailto:' + self.allcontributors[i]['email'] + '">Email</a></li>\n')

				if self.allcontributors[i]['twitter']:
					opened_file.write('	<li><a href="https://twitter.com/' + self.allcontributors[i]['twitter'] + '">Twitter</a></li>\n')
				if self.allcontributors[i]['twitch']:
					opened_file.write('	<li><a href="https://www.twitch.tv/' + self.allcontributors[i]['twitch'] + '">Twitch</a></li>\n')
				if self.allcontributors[i]['website']:
					opened_file.write('	<li><a href="' + self.allcontributors[i]['website'] + '">Website</a></li>\n')

				opened_file.write('</ul>\n')

			opened_file.close()

			SChtmlfile = SCpathHTML + "/" + str(self.allcontributors[i]['id_number']) + '.html'
			exists = os.path.isfile(SChtmlfile)
			if not exists:
				print("file change: " + str(self.allcontributors[i]['id_number']) + '.html')
				shutil.copyfile(fileout,SChtmlfile)
			else:
				if not filecmp.cmp(fileout_check,SChtmlfile):
					print("file change: " + str(self.allcontributors[i]['id_number']) + '.html')
					shutil.copyfile(fileout,SChtmlfile)



		opened_checkfile.close()


		SCdescfile = SCpath + "/filecheck.txt"
		exists = os.path.isfile(SCdescfile)
		if not exists:
			print("file change: filecheck.txt")
			shutil.copyfile(fileout_check,SCdescfile)
		else:
			if not filecmp.cmp(fileout_check,SCdescfile):
				print("file change: filecheck.txt")
				shutil.copyfile(fileout_check,SCdescfile)
			

