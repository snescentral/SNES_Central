for number in $( seq 0 1210 )
do

  if [ ${number} -lt 10 ]
  then
    full=000${number}
  elif [ ${number} -lt 100 ]
  then
    full=00${number}
  elif [ ${number} -lt 1000 ]
  then
    full=0${number}
  else
    full=${number}
  fi
	rm temp/*
	python game_sort.py ${full}

done
