#!/usr/bin/env python3

import csv
import sys
import os.path
import copy
import operator
import datetime
import filecmp
from shutil import copyfile

if len(sys.argv) > 2 or len(sys.argv) < 2:
	print("Warning: you must enter the four digit id code of the game/article you are interested in")
	sys.exit()

if len(sys.argv[1]) > 4 or len(sys.argv[1]) < 4:
	print("Warning: you must enter the four digit id code of the game/article you are interested in")
	sys.exit()

game_id = sys.argv[1]


# detect if the game entry is there
game_path = "SNES_Central/" + game_id[0] + '/' + game_id[1] + '/' + game_id[2] + '/' + game_id



if not os.path.isdir(game_path):
	print("This entry does not exist yet.\n")
	sys.exit()

game_info = open("SNES_Central/info.csv", "r")
game_info_csv = csv.DictReader(game_info, delimiter='\t')

for row in game_info_csv:
	if row["page ID"] == game_id:
		article_type = row["page type"]
		article_file = row["file"]
		title = row["Title"]
		serial_code = row["Serial Code"]
		genre = row["Genre"]
		US_title = row["US Title"]
		US_publisher = row["US Publisher"]
		Japan_title = row["Japanese Title"]
		Japan_publisher = row["Japanese Publisher"]
		Japan_kana = row["Title in kana"]
		Japan_kanji = row["Title in Kanji"]
		PAL_title = row["PAL Title"]
		PAL_publisher = row["PAL Publisher"]
		developer = row["Developer"]
		players = row["Players"]
		accessories = row["Accessories"]
		coprocessor = row["Coprocessor"]
		save_type = row["Save type"]
		alt_title = row["Alternative title (state region)"]
		alt_publisher = row["Alternative publisher (state region)"]

print(game_id + " " + title)

# cart info file

is_Japan = False
is_USA = False
is_PAL = False
cart_same = True

cart_file = game_path + "/carts2.txt"
final_cart_html = game_path + "/carts.html"
temp_cart_html = "temp/carts.html"

if os.path.isfile(cart_file):
	cart_open = open(cart_file,"r")
	cart_csv = csv.reader(cart_open, delimiter='|')

	cart_html = open(temp_cart_html,"w+")


	cart_entry = []
	combined = []
	counter = 0
	for item in cart_csv:
		combined.append(counter)
		combined.extend(item)
		cart_entry.append(combined)
		counter = counter+1
		combined = []

		if item[4] == "Japan":
			is_Japan = True
		if item[3] == "Americas":
			is_USA = True
		if item[3] == "PAL":
			is_PAL = True

	cart_entry.sort(key=operator.itemgetter(4,5,6))

	cart_html.write("<p class=\"name\">Cartridge label information</p>\n")
	cart_html.write("<table  class=\"infotable\">\n")
	cart_html.write("	<tr  class=\"row2\">\n")
	cart_html.write("\n")
	cart_html.write("		<th style=\"width : 80px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Region</th>\n")
	cart_html.write("		<th style=\"width : 100px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Country</th>\n")
	cart_html.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Serial Code</th>\n")
	cart_html.write("		<th style=\"width : 270px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">notes</th>\n")
	cart_html.write("		<th style=\"width : 70px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Scan</th>\n")
	cart_html.write("	</tr>\n")

	counter = 0
	for item in cart_entry:

		if  counter%2 == 0:
			row = "row1"
		else:
 			row = "row2"


		cart_html.write("	<tr class = " + row + ">\n")
		cart_html.write("\n")
		cart_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[4] + "</td>\n")
		cart_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[5] + "</td>\n")
		cart_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[6] +  "</td>\n")
		cart_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[7] +  "</td>\n")
		cart_html.write("		<td style=\"text-align: left; vertical-align: top;\">")

		if item[1] != "":
			cart_html.write("<a href=\"cart.php?id=" + str(game_id) + "&num=" + str(item[0])  + "\">link</a>")
			cart_scan_file = game_path + "/" +  str(item[1])
			if not os.path.isfile(cart_scan_file):
				print("cart file is missing: " + str(item[1]))

		else:
			cart_html.write("scan needed")

		cart_html.write("</td>\n")
	
		cart_html.write("	</tr>\n")
		counter = counter + 1

	cart_html.write(" </table>\n")
	cart_html.write("		<p><i>Do you have a label variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1097\">Submission guidelines</a> for cartridge label scans.</p>\n")

	cart_open.close()
	cart_html.close()


	if (os.path.isfile(final_cart_html)):
		cart_same = filecmp.cmp(temp_cart_html, final_cart_html)
	else:
		cart_same = False

# pcb info file

pcb_file = game_path + "/pcb.txt"

pcb_same = True
final_pcb_html = game_path + "/pcb.html"
temp_pcb_html = "temp/pcb.html"

if os.path.isfile(pcb_file):
	pcb_open = open(pcb_file,"r")
	pcb_csv = csv.reader(pcb_open, delimiter='|')

	pcb_html = open(temp_pcb_html,"w+")


	pcb_entry = []
	combined = []
	counter = 0
	for item in pcb_csv:
		combined.append(counter)
		combined.extend(item)
		pcb_entry.append(combined)
		counter = counter+1
		combined = []

		if item[4] == "Japan":
			is_Japan = True
		if item[4] == "USA":
			is_USA = True
		if item[4] == "PAL" or item[4] == "Germany" or item[4] == "France" or item[4] == "Netherlands":
			is_PAL = True

	pcb_entry.sort(key=operator.itemgetter(5,4,6))

	pcb_html.write("<p class=\"name\">PCB Information</p>\n")

	pcb_html.write("<table  class=\"infotable\">\n")
	pcb_html.write("	<tr  class=\"row2\">\n")
	pcb_html.write("		<th style=\"width : 120px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">ROM Chip ID</th>\n")
	pcb_html.write("		<th style=\"width : 60px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Region</th>\n")
	pcb_html.write("		<th style=\"width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Known Man. Dates</th>\n")
	pcb_html.write("		<th style=\"width : 120px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">PCB Type</th>\n")
	pcb_html.write("		<th style=\"width : 70px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">CIC</th>\n")
	pcb_html.write("		<th style=\"width : 80px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">SRAM</th>\n")
	pcb_html.write("		<th style=\"width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Decoder/ Logic Circuit</th>\n")
	pcb_html.write("		<th style=\"width : 90px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Special Chips</th>\n")
	pcb_html.write("	</tr>\n")

	counter = 0
	for item in pcb_entry:

		if  counter%2 == 0:
			row = "row1"
		else:
 			row = "row2"


		pcb_html.write("	<tr class = \"" + row + "\">\n")
		pcb_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[4] + " \n")

		pcb_html.write(" (<a href=\"pcb.php?id=" + str(game_id) + "&num=" + str(item[0]) + "&side=front\">front</a>) ")

		pcb_scan_file = game_path + "/" +  str(item[1])
		if not os.path.isfile(pcb_scan_file):
			print("pcb front file is missing: " + str(item[1]))

		if item[12] != "":
			pcb_html.write("	(<a href=\"pcb.php?id="  + str(game_id) + "&num=" + str(item[0]) + "&side=back\">back</a>) ")
			pcb_scan_file = game_path + "/" +  str(item[12])
			if not os.path.isfile(pcb_scan_file):
				print("pcb back file is missing: " + str(item[12]))


		pcb_html.write("  </td>\n")
		pcb_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[5] + "</td>\n")
		pcb_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[6] + "</td>\n")
		pcb_html.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"pcbboards.php?chip=" + item[7] + "\">" + item[7] + "</a></td>\n")
		pcb_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[8] + "</td>\n")


		if item[9] == "":
			pcb_html.write("<td style=\"text-align: center; vertical-align: top;\">-</td>\n")
		else:
			pcb_html.write("<td style=\"text-align: left; vertical-align: top;\">" + item[9] + "</td>\n")


		if item[10] == "":
			pcb_html.write("<td style=\"text-align: center; vertical-align: top;\">-</td>\n")
		else:
			pcb_html.write("<td style=\"text-align: left; vertical-align: top;\">" + item[10] + "</td>\n")

		if item[11] == "":
			pcb_html.write("<td style=\"text-align: center; vertical-align: top;\">-</td>\n")
		else:
			pcb_html.write("<td style=\"text-align: left; vertical-align: top;\">" + item[11] + "</td>\n")
	
		pcb_html.write("</tr>\n")

	pcb_html.write(" </table>\n")
	pcb_html.write("		<p><i>Do you have a PCB with a different ROM chip ID, PCB type or manufacturing date?</i> Read the <a href=\"article.php?id=1094\">Submission guidelines</a> for PCB scans.</p>\n")

	pcb_open.close()
	pcb_html.close()

	if (os.path.isfile(final_pcb_html)):
		pcb_same = filecmp.cmp(temp_pcb_html, final_pcb_html)
	else:
		pcb_same = False

# box info file

box_file = game_path + "/box2.txt"

box_same = True
final_box_html = game_path + "/box.html"
temp_box_html = "temp/box.html"

if os.path.isfile(box_file):
	box_open = open(box_file,"r")
	box_csv = csv.reader(box_open, delimiter='|')

	box_html = open(temp_box_html,"w+")


	box_entry = []
	combined = []
	counter = 0
	for item in box_csv:
		combined.append(counter)
		combined.extend(item)
		box_entry.append(combined)
		counter = counter+1
		combined = []

		if item[4] == "Japan":
			is_Japan = True
		if item[3] == "Americas":
			is_USA = True
		if item[3] == "PAL":
			is_PAL = True


	box_entry.sort(key=operator.itemgetter(4,5,6))

	box_html.write("<p class=\"name\">Box information</p>\n")
	box_html.write("<table  class=\"infotable\">\n")
	box_html.write("	<tr  class=\"row2\">\n")
	box_html.write("\n")
	box_html.write("		<th style=\"width : 80px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Region</th>\n")
	box_html.write("		<th style=\"width : 100px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Country</th>\n")
	box_html.write("		<th style=\"width : 130px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Serial Code</th>\n")
	box_html.write("		<th style=\"width : 240px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">notes</th>\n")
	box_html.write("		<th style=\"width : 50px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Front scan</th>\n")
	box_html.write("		<th style=\"width : 50px; padding : 0px; border : 2px  #000000; text-align: left; vertical-align: top;\">Back scan</th>\n")
	box_html.write("	</tr>\n")

	counter = 0
	for item in box_entry:

		if  counter%2 == 0:
			row = "row1"
		else:
 			row = "row2"


		box_html.write("	<tr class = " + row + ">\n")
		box_html.write("\n")
		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[4] + "</td>\n")
		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[5] + "</td>\n")
		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[6] +  "</td>\n")
		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">" + item[7] +  "</td>\n")
		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">")

		if item[1] != "":
			box_html.write("<a href=\"box.php?id=" + str(game_id) + "&num=" + str(item[0])  + "&side=front\">link</a>")

			box_scan_file = game_path  + "/" +  str(item[1])
			if not os.path.isfile(box_scan_file):
				print("box front file is missing: " + str(item[1]))
		else:
			box_html.write("scan needed")
		box_html.write("</td>\n")

		box_html.write("		<td style=\"text-align: left; vertical-align: top;\">")
		if item[8] != "":
			box_html.write("<a href=\"box.php?id=" + str(game_id) + "&num=" + str(item[0])  + "&side=back\">link</a>")
			box_scan_file = game_path + "/" + str(item[8])
			if not os.path.isfile(box_scan_file):
				print("box front file is missing: " + str(item[8]))
		else:
			box_html.write("scan needed")

		box_html.write("</td>\n")
	
		box_html.write("	</tr>\n")
		counter = counter + 1

	box_html.write(" </table>\n")
	box_html.write("		<p><i>Do you have a box variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1098\">Submission guidelines</a> for box scans.</p>\n")

	box_open.close()
	box_html.close()

	if (os.path.isfile(final_box_html)):
		box_same = filecmp.cmp(temp_box_html, final_box_html)
	else:
		box_same = False


if is_Japan and Japan_title == "":
	print("Detected Japan region, but title is not set in info.csv!")
if is_USA and US_title == "":
	print("Detected USA region, but title is not set in info.csv!")
if is_PAL and PAL_title == "":
	print("Detected PAL region, but title is not set in info.csv!")


if not cart_same:
	print ("the carts html file is different")
	copyfile(temp_cart_html, final_cart_html)

if not pcb_same:
	print ("the pcb html file is different")
	copyfile(temp_pcb_html, final_pcb_html)

if not box_same:
	print ("the box html file is different")
	copyfile(temp_box_html, final_box_html)

