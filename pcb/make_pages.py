#!/usr/bin/env python3
import sys
from pcb import pcb
import yaml
import os
import filecmp
import shutil
import itertools


dummypcb = pcb()

SCpath = "../SNES_Central/pcblist"

SCpathHTML = "../SNES_Central/pcblist/html"
if not os.path.exists(SCpathHTML):
	os.makedirs(SCpathHTML)


SCpathIMG = SCpath + "/pcb_scans"
if not os.path.exists(SCpathIMG):
	os.makedirs(SCpathIMG)

if not os.path.exists('temp'):
	os.makedirs('temp')


descHTMLfile="temp/description.html"
openDescHTML = open(descHTMLfile,"w")



myIterator = itertools.cycle([1,2])

openDescHTML.write("		<p class=\"headingtext\">SNES PCB List</p>\n")
openDescHTML.write("		<p>This is a list of known PCB types that have been used for SNES cartridges. This includes standard PCBs used for commercial releases and prototypes, add-on cartridges, and third party PCBs. The PCB board can be used to indicate the memory mapping to get a game working on an emulator, so this list has proven to be quite a bit move valuable than I originally though when I started collectiong PCB scans back in the early 2000s. The serial code of standard PCBs gives information about how the PCB is set up. The meaning of the serial codes was originally summarized by Fitzroy on the website of the now defunct Zapatabase (<a href=\"https://web.archive.org/web/20110221122057/http://www.zapatabase.com/documentation/SFC_SNES_Serial_Codes.htm\">Archived link</a>), and that information is included on all of the pages below. I have also included the memory mapping used in byuu's bsnes and higan emulators. If a game does not work, and I have linked the mapping for the ROM image to a specific board type, you can copy this into a file called \"manifest.bml\" in the game folder of the game, and it should work properly.</p>\n")
openDescHTML.write("		<p>The order I have put the PCBs in the list below is in order of compatibility. That is, the second component of the second part of the serial code. Roughly, the order should correspond to: special chip, mapping, SRAM, decoder chip, number of chips, revision, manufacturer.</p>\n")

openDescHTML.close()

# count number of PCBs

listHTMLfile="temp/list.html"
openlistHTML = open(listHTMLfile,"w")


listfile="temp/list.txt"
openlist = open(listfile,"w")

counter = 0

for pcbtypes in ['Standard','Add-on','Prototype','Third Party']:
	templist = dummypcb.list_pcbs(pcbtypes)

	for item in templist:

		found = dummypcb.load_pcb(item)

		if found:
			counter = counter + 1

openlistHTML.write("		<p><b>Number of documented PCB types:</b> " + str(counter) + "</b></p>\n")


missing_pcb_scans = []

dummypcb.clear_entry()
# print list of pcbs

for pcbtypes in ['Standard','Add-on','Prototype','Third Party']:

	openlistHTML.write("		<p class=\"name\">" + pcbtypes + " PCBs</p>\n")
	openlistHTML.write("		<table class=\"infotable2\">\n")
	openlistHTML.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
	openlistHTML.write("				<td width=\"150px\"><b>Serial Code</b></td>\n")
	openlistHTML.write("				<td width=\"150px\"><b>Special Chip</b></td>\n")
	openlistHTML.write("				<td width=\"150px\"><b>Number of ROM Chips</b></td>\n")
	openlistHTML.write("				<td width=\"150px\"><b>SRAM</b></td>\n")
	openlistHTML.write("				<td width=\"150px\"><b>Mapping</b></td>\n")
	openlistHTML.write("			</tr>\n")



	templist = dummypcb.list_pcbs(pcbtypes)

	for item in templist:

		found = dummypcb.load_pcb(item)

		if found:


			if dummypcb.pcb['serial_code']:
				openlist.write(item + "\t" + dummypcb.pcb['serial_code'] + '\n')

			else:
				openlist.write(item + "\t" + item + '\n')


			found2, special_chip, mapping_type, ROM_chip_type = dummypcb.mapping_info()
			found3, description, number_chips = dummypcb.ROM_chip_info()
			found4, sram_size = dummypcb.sram_info()

			openlistHTML.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")

			if dummypcb.pcb['serial_code']:
				openlistHTML.write("				<td><a href=\"pcbboards.php?chip=" + item +"\">" + dummypcb.pcb['serial_code'] + "</a></td>\n")
			else:
				openlistHTML.write("				<td><a href=\"pcbboards.php?chip=" + item +"\">" + item + "</a></td>\n")

			openlistHTML.write("				<td>"+ special_chip +"</td>\n")
			openlistHTML.write("				<td>"+ str(number_chips) + "</td>\n")
			openlistHTML.write("				<td>" + sram_size + "</td>\n")
			openlistHTML.write("				<td>" + mapping_type + "</td>\n")
			openlistHTML.write("			</tr>\n")

			# write out standard page for the chip

			filename = "temp/" + item + ".html"
			openfile = open(filename,"w")

			html = dummypcb.write_html_pcb_page()

			openfile.write(html)
			openfile.close()

			SCfile = SCpathHTML + "/" + item + ".html"
			shallow = False

			exists = os.path.isfile(SCfile)
			if not exists:
				print("file change: " + item + ".html")
				shutil.copyfile(filename,SCfile)
			else:

				if not filecmp.cmp(filename,SCfile):
					print("file change: " + item + ".html")
					shutil.copyfile(filename,SCfile)

			# move the image files

			found5, path = dummypcb.front_image(item, report_error = True)
			fullPath = SCpath + "/" + path
			if found5:

				exists2 = os.path.isfile(fullPath)
				if(exists2):
					# check if file is the same
					
					if not filecmp.cmp(path,fullPath):
						shutil.copyfile(path,fullPath)
				else:
					shutil.copyfile(path,fullPath)


			found5, path = dummypcb.back_image(item, report_error = True)
			fullPath = SCpath + "/" + path
			if found5:

				exists2 = os.path.isfile(fullPath)
				if(exists2):
					# check if file is the same
					
					if not filecmp.cmp(path,fullPath):
						shutil.copyfile(path,fullPath)
				else:
					shutil.copyfile(path,fullPath)

			else:

				missing_pcb_scans.append(dummypcb.pcb['serial_code'])
			

		dummypcb.clear_entry()



	openlistHTML.write("		</table>\n")

# write out the missing pcbs

openlistHTML.write("		<p>This is a list of PCBs for which I do not have a scan of the back of the pcb. Although I have some of them (and have not had a chance to scan them), if you have a scan of these, please let me know and I will add them. (<a href=\"mailto:snes_central@yahoo.ca\">snes_central@yahoo.ca</a>)</p>\n")
openlistHTML.write("		<ul>\n")
for entries in missing_pcb_scans:
	openlistHTML.write("			<li>" + str(entries) + "</li>\n")
openlistHTML.write("		</ul>\n")
openlistHTML.close()

SClistfileHTML = "../SNES_Central/pcblist/list.html"
exists = os.path.isfile(SClistfileHTML)
if not exists:
	print("file change: list.html")
	shutil.copyfile(listHTMLfile,SClistfileHTML)
else:
	if not filecmp.cmp(listHTMLfile,SClistfileHTML):
		print("file change: list.html")
		shutil.copyfile(listHTMLfile,SClistfileHTML)


openlist.close()
SClistfile = "../SNES_Central/pcblist/list.txt"

exists = os.path.isfile(SClistfile)
if not exists:
	print("file change: list.txt")
	shutil.copyfile(listfile,SClistfile)
else:
	if not filecmp.cmp(listfile,SClistfile):
		print("file change: list.txt")
		shutil.copyfile(listfile,SClistfile)


SCdescfileHTML = "../SNES_Central/pcblist/description.html"
exists = os.path.isfile(SCdescfileHTML)
if not exists:
	print("file change: description.html")
	shutil.copyfile(descHTMLfile,SCdescfileHTML)
else:
	if not filecmp.cmp(descHTMLfile,SCdescfileHTML):
		print("file change: description.html")
		shutil.copyfile(descHTMLfile,SCdescfileHTML)
