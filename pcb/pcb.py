#!/usr/bin/env python3

import yaml
import itertools
import os
import sys

sys.path.append('../contributors')
from contributors import contributor
sys.path.append('../chiptype')
from chips import chips

class pcb:

	def __init__(self):

		# this lists all the possible fields in a pcb object
		'''
		  'id_number' # unique number for the entry
		  'pcbID' 	# Serial code for the PCB
              'contributor_id' # pointer to the person who contributed the scans
              'serial_code' # serial code, since the ID might not just be the serial code
		  'chipSlots'		# number of chip slots
		  'chipSlotType'	# for each chip slot
		  'memoryMapping'		# Memory Mapping for each chip
		  'SRAM_Size'     # SRAM size for this board
		  'notes'	# notes for the chip
		  'company_id' # first part off the serial code
		  'ROM_chip_id'  # first component of the second part of the serial code
		  'mapping_id'  # second component of the second part of the serial code
		  'SRAM_id'  # third component of the second part of the serial code
		  'sram_decoder_id'  # fourth component of the second part of the serial code
		  'secondary_sram_id' # fifth component of the second part of the serial code
		  'revision_id'  # third compnent of the serial code
		  'secondary_revision_id' fourth component of the serial code
		  'pcb_type' # can be standard, add-on or third-party
		'''

		self.pcb = { 
		  'id_number' : '',
		  'pcbID' : '',
		  'contributor_id' : '',
		  'serial_code' : '',
		  'chipSlots' : '',
		  'chipSlotType' : '', 
		  'memoryMapping' : '', 
		  'SRAM_Size': '', 
		  'notes' : '',
		  'company_id' : '',
		  'ROM_chip_id': '',
		  'mapping_id': '',
		  'SRAM_id': '',
		  'sram_decoder_id': '',
		  'secondary_sram_id': '',
		  'revision_id': '', 
		  'secondary_revision_id': '',
		  'pcb_type': 'can be standard, add-on or third-party, use the notes field'

		}

		self.pcb_description = { 

		  'id_number' : 'unique ID for the entry',
		  'pcbID' : 'Unique ID for the PCB',
		  'contributor_id' : 'ID number of the person who contributed the scan',
              'serial_code' : "serial code for the PCB",
		  'chipSlots' : 'number of chip slots', 
		  'chipSlotType' : 'for each chip slot', 
		  'memoryMapping' : 'Memory Mapping for each chip',
		  'SRAM_Size': 'SRAM size for this board',
		  'notes' : 'notes for the pcb',
		  'company_id': 'first part off the serial code',
		  'ROM_chip_id': 'first component of the second part of the serial code',
		  'mapping_id': 'second component of the second part of the serial code',
		  'SRAM_id': 'third component of the second part of the serial code',
		  'sram_decoder_id': 'fourth component of the second part of the serial code',
		  'secondary_sram_id': 'fifth component of the second part of the serial code',
		  'revision_id': 'third compnent of the serial code',
		  'secondary_revision_id': 'fourth component of the serial code',
		  'pcb_type': ''
		}


		self.chip_object = chips()

		self.contributor_obj = contributor()

		self.allPCBs = []

		# load yaml file
		self.filename = os.path.join(os.path.dirname(__file__),"pcb.yml")
		file = open(self.filename, 'r')
		self.datastore = yaml.load_all(file, Loader=yaml.FullLoader)
		i=0
		for loadPCBs in self.datastore:
			for entries in loadPCBs:
				self.allPCBs.insert(i, entries.copy())
				i+=1
		self.number_of_pcbs = i-1


	####################################################################
	##### Methods for creating and editing a list of pcb id values #####
	####################################################################

	def get_id_list(self, id_list = [], pcb_serial = ''):

		# assuming if pcb_serial is entered, then it will automatically add it
		if pcb_serial:

			found = self.load_pcb(pcb_serial)

			if found:
				id_list.append(self.pcb['id_number'])

		done = False
		while not done:
			print("\nEnter pcb serial code ('f' if finished):")
			pcb_value = input(">>> ")

			if pcb_value == "f":
				done = True

			else:

				found = self.load_pcb(pcb_value)

				if found:
					id_list.append(self.pcb['id_number'])



				else:
					print(f"Could not find PCB: {pcb_value}")


		# check if there are duplicates

		number_pcbs = len(id_list)
		if number_pcbs > 1:
			id_list.sort()

			counter = 0
			while counter < number_pcbs-1:
				if id_list[counter] == id_list[counter+1]:
					del id_list[counter+1]
					number_pcbs -= 1
				else:
					counter += 1

		return id_list

	def delete_from_id_list(self,id_list=[]):

		if id_list:

			done = False
			while not done:
				self.print_current_pcbs(id_list)

				print("Select pcb id value to delete (f to finish):")

				entry = input(">>> ")
				if entry == 'f':
					done = True
				else:
					try:
						entry = int(entry)

						for i in range(len(id_list)):
							if id_list[i] == entry:
								del id_list[i]
								break

					except ValueError: 
						print("invalid id number")

		return id_list

	def print_current_pcbs(self, id_list=[]):

		if id_list:

			print("Current PCBs:")
			for id_value in id_list:
				self.load_pcb_id(id_value)
				print(f"{self.pcb['id_number']}) {self.pcb['serial_code']}")

		else:
			print("There are currently no assigned PCBs")


	# loads the information related to the specified part # (id)
	def load_pcb(self,id_value = ''):

		self.clear_entry()
		found = False

		if not id_value:
			print("Enter the serial code of the PCB")
			id_value = input(">>> ")


		for i in range(len(self.allPCBs)):

			if id_value == self.allPCBs[i]['pcbID']:
				self.pcb.update(self.allPCBs[i])
				found = True
				break


		return found

	def load_pcb_id(self,id_number):

		self.clear_entry()
		found = False
		for i in range(len(self.allPCBs)):

			if id_number == self.allPCBs[i]['id_number']:
				self.pcb.update(self.allPCBs[i])
				found = True
				break

		return found

	# clears entry
	def clear_entry(self):
		
		for item in self.pcb:
			self.pcb[item] = ''


	def print_info(self, id):

		found = False
		for i in range(len(self.allPCBs)):

			if id == self.allPCBs[i]['pcbID']:
				print(self.allPCBs[i]['pcbID'])
				print(self.allPCBs[i]['contributor_id'])
				print(self.allPCBs[i]['chipSlots'])
				print(self.allPCBs[i]['memoryMapping'])
				found = True
				break

	def front_image(self, id, report_error = False):
		found = False
		imgpath = "pcb_scans/" + id + "-pcb-front.jpg"

		if not os.path.exists(imgpath):
			if(report_error):
				print("Front image for pcb board " + id + " does not exist")
		else:
			found = True
		return found, imgpath

	def back_image(self, id, report_error = False):
		found = False
		imgpath = "pcb_scans/" + id + "-pcb-back.jpg"
		if not os.path.exists(imgpath):
			if(report_error):
				print("Back image for pcb board " + id + " does not exist")
		else:
			found = True
		return found, imgpath

	# PCB code information deduced by Fitzroy, which can be found on an old page https://web.archive.org/web/20110221122057/http://www.zapatabase.com/documentation/SFC_SNES_Serial_Codes.htm

	# the first part of the serial code usually gives the company that manufactured the PCB
	def company_info(self):

		id = self.pcb['company_id']

		found = True
		description = ""
		if (id == "SHVC"):
			description = "Super Home Video Computer - Nintendo (Note, I have found no primary source that this acronym is correct)"
		elif (id == "MAXI"):
			description = "Majesco - (Fitzroy listed this as \"Maxis\", but I am less certain as to what it means.)"
		elif (id == "MJSC"):
			description = "Majesco"
		elif (id == "EA"):
			description = "Electronic Arts"
		elif (id == "SNSP"): 
			description = "Super Nintendo System PAL"
		elif (id == "WEI"): 
			description = "Williams Entertainment Inc."
		elif (id == "Ocean"): 
			description = "Ocean Software"
		elif (id == "Capcom"): 
			description = "Capcom"
		elif (id == "SGB"): 
			description = "Super Game Boy - Nintendo"
		elif (id == "BSC"): 
			description = "Broadcast Satellaview Cartridge"
		elif (id == "BSMC"): 
			description = "Broadcast Satellaview Memory Cartridge"
		else:
			found = False
		return found, description

	# the first part of the second component of the serial code is related to the number of ROM chips and the size of ROM chips allowed
	def ROM_chip_info(self):

		id = self.pcb['ROM_chip_id']

		found = True
		description = ""
		number_chips = 0
		if (id == "1"):
			description = "2Mb, 4Mb, 8Mb, 16Mb, or 32Mb"
			number_chips = 1
		elif (id == "2"):
			description = "8Mb+2Mb, 8Mb+4Mb, or 8Mb+8Mb"
			number_chips = 2
		elif (id == "3"):
			description = "2Mb+2Mb+2Mb, 4Mb+4Mb+4Mb, or 8Mb+8Mb+8Mb"
			number_chips = 3
		elif (id == "4"):
			description = "2Mb+2Mb+2Mb+2Mb, 4Mb+4Mb+4Mb+4Mb, or 8Mb+8Mb+8Mb+8Mb"
			number_chips = 4
		elif (id == "8"):
			description = "2Mb+2Mb+2Mb+2Mb+2Mb+2Mb+2Mb+2Mb, 4Mb+4Mb+4Mb+4Mb+4Mb+4Mb+4Mb+4Mb, or 8Mb+8Mb+8Mb+8Mb+8Mb+8Mb+8Mb+8Mb"
			number_chips = 8
		elif (id == "Y"):
			description = "4Mb+2Mb or 4Mb+4Mb"
			number_chips = 2
		elif (id == "B"):
			description = "16Mb+2Mb, 16Mb+4Mb, 16Mb+8Mb, or 16Mb+16Mb"
			number_chips = 2
		elif (id == "L"):
			description = "32Mb+2Mb, 32Mb+4Mb, 32Mb+8Mb, 32Mb+16Mb, 32Mb+32Mb"
			number_chips = 2
		else:
			found = False

		return found, description, number_chips

	# the second part of the second component of the serial code is related to type of chips and whether it is "hirom or "lorom", and whether or not it is a prototype board
	def mapping_info(self):

		id = self.pcb['mapping_id']

		found = True
		special_chip = ""
		mapping_type = ""
		ROM_chip_type = ""
		

		if (id == "A"):
			special_chip = "none"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "B"):
			special_chip = "DSP-x"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "C"):
			special_chip = "Super FX - Mario Chip 1"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "CA"):
			special_chip = "Super FX - GSU-1"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "CB"):
			special_chip = "Super FX - GSU-2"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "DC"):
			special_chip = "CX4"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "DE"):
			special_chip = "ST018"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "DH"):
			special_chip = "SPC7110"
			mapping_type = "HiROM"
			ROM_chip_type = "MASKROM"
		elif (id == "DS"):
			special_chip = "ST010 or ST011"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "E"):
			special_chip = "OBC1"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "J"):
			special_chip = "none"
			mapping_type = "HiROM"
			ROM_chip_type = "MASKROM"
		elif (id == "K"):
			special_chip = "DSP-x"
			mapping_type = "HiROM"
			ROM_chip_type = "MASKROM"
		elif (id == "L"):
			special_chip = "SA-1"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "N"):
			special_chip = "S-DD1"
			mapping_type = "LoROM"
			ROM_chip_type = "MASKROM"
		elif (id == "P"):
			special_chip = "none"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "PV"):
			special_chip = "none"
			mapping_type = "HiROM"
			ROM_chip_type = "EPROM"
		elif (id == "Q"):
			special_chip = "DSP-x"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "QW"):
			special_chip = "DSP-x"
			mapping_type = "HiROM"
			ROM_chip_type = "EPROM"
		elif (id == "RA"):
			special_chip = "Super FX - GSU-1, GSU-1A"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "RB"):
			special_chip = "Super FX - GSU-2"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "T"):
			special_chip = "OBC1"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "X"):
			special_chip = "SA-1"
			mapping_type = "LoROM"
			ROM_chip_type = "EPROM"
		elif (id == "SGB2"):
			special_chip = "Super Game Boy 2"
			mapping_type = "-"
			ROM_chip_type = "MASKROM"
		elif (id == "SGB-R"):
			special_chip = "Super Game Boy"
			mapping_type = "-"
			ROM_chip_type = "MASKROM"
		elif (id == "MMS"):
			special_chip = "MX15001 (Nintendo Power)"
			mapping_type = "-"
			ROM_chip_type = "Flash"
		elif (id == "BR"):
			special_chip = "Satellaview Memory Cartridge - MASKROM"
			mapping_type = "-"
			ROM_chip_type = "MASKROM"
		else:
			found = False

		return found, special_chip, mapping_type, ROM_chip_type


	# the third part of the second component of the serial code is related to the SRAM size
	def sram_info(self):

		id = str(self.pcb['SRAM_id'])


		found = True
		sram_size = ""

		if (id == "0"):
			sram_size = "none"
		elif (id == "1"):
			sram_size = "16k"
		elif (id == "2"):
			sram_size = "32k"
		elif (id == "3"):
			sram_size = "64k"
		elif (id == "5"):
			sram_size = "256k"
		elif (id == "6"):
			sram_size = "512k"
		elif (id == "7"):
			sram_size = "1024k"
		elif (id == "8"):
			sram_size = "2048k"
		elif (id == "9"):
			sram_size = "4096k"
		else:
			found = False

		return found, sram_size


	# the fourth part of the second component of the serial code is related to chips that interact with SRAM
	def sram_chip_decoder(self):

		id = self.pcb['sram_decoder_id']

		found = True
		sram_decoder = ""

		if (id == "B"):
			sram_decoder = "74LS139"
		elif (id == "F"):
			sram_decoder = "74AC139"
		elif (id == "C"):
			sram_decoder = "RTC"
		elif (id == "M"):
			sram_decoder = "MAD-1 or MAD-R"
		elif (id == "N"):
			sram_decoder = "none"
		elif (id == "R"):
			sram_decoder = "S-RTC"
		elif (id == "X"):
			sram_decoder = "MAD-2"
		else:
			found = False

		return found, sram_decoder

	# the fifth part of the second component of the serial code is related to addtional SRAM
	def additional_sram_info(self):

		id = self.pcb['secondary_sram_id']

		found = True
		addition_sram = ""
		addition_sram_description = ""

		if (id == "3S"):		
			addition_sram = "64Kb"
			addition_sram_description = "partially dedicated for framebuffer storage"
		elif (id == "5S"):		
			addition_sram = "256Kb"
			addition_sram_description = "partially dedicated for SuperFX framebuffer storage"
		elif (id == "6S"):		
			addition_sram = "512Kb"
			addition_sram_description = "partially dedicated for SuperFX framebuffer storage"
		elif (id == "7S"):		
			addition_sram = "512Kb"
			addition_sram_description = "partially dedicated for SuperFX2 framebuffer storage"
		elif (id == "9P"):		
			addition_sram = "512Kb"
			addition_sram_description = "dedicated for BS-X temporary storage"
		else:
			found = False

		return found, addition_sram, addition_sram_description

	# returns a list of the PCBs of a specific pcb_type
	def list_pcbs(self, in_type):
	
		outlist = []
		for entries in self.allPCBs:
			if entries['pcb_type'] == in_type:
				outlist.append(entries['pcbID'])

		return outlist

	# returns the HTML code for a link to the PCB

	def pcb_link(self, pcb_id):

		self.load_pcb_id(pcb_id)

		link = "<a href=\"pcbboards.php?chip=" + self.pcb['pcbID'] +"\">" + self.pcb['serial_code'] + "</a>"
		return link

	# prints out the chip information for the main pcb pages. Can be called without filling in information.
	def write_html_pcb_page(self):
		print(self.pcb['pcbID'])

		# not done
		if not self.pcb['serial_code']:
			html_all = 	"	<p class=\"headingtext\">" +  self.pcb['pcbID'] + "</p>\n"
		else:
			html_all = 	"	<p class=\"headingtext\">" +  self.pcb['serial_code'] + "</p>\n"


		myIterator = itertools.cycle([1,2])


		html_all = html_all + "		<table class=\"infotable2\">\n"
		html_all = html_all + "		<caption class=\"name\">PCB information</caption>\n"
		html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
		html_all = html_all + "				<td width=\"175px\"><b>property</b></td>\n"
		html_all = html_all + "				<td width=\"500px\"><b>description</b></td>\n"
		html_all = html_all + "			</tr>\n"


		html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
		html_all = html_all + "				<td>PCB type</td>\n"
		html_all = html_all + "				<td>" + self.pcb['pcb_type'] + "</td>\n"
		html_all = html_all + "			</tr>\n"

		if self.pcb['serial_code']:
			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Serial code</td>\n"
			html_all = html_all + "				<td>" + self.pcb['serial_code'] + "</td>\n"
			html_all = html_all + "			</tr>\n"





		found, description = self.company_info()

		if found:
			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Company</td>\n"
			html_all = html_all + "				<td>" + description + "</td>\n"
			html_all = html_all + "			</tr>\n"


		html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
		html_all = html_all + "				<td>Number of chip slots</td>\n"
		html_all = html_all + "				<td>" + str(self.pcb['chipSlots']) + "</td>\n"
		html_all = html_all + "			</tr>\n"

		found, description, number_chips = self.ROM_chip_info()

		if found:
			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Number of ROM chips</td>\n"
			html_all = html_all + "				<td>" + str(number_chips) + "</td>\n"
			html_all = html_all + "			</tr>\n"

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>ROM chip memory size</td>\n"
			html_all = html_all + "				<td>" + description + "</td>\n"
			html_all = html_all + "			</tr>\n"

		found, special_chip, mapping_type, ROM_chip_type = self.mapping_info()

		if found:

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>ROM chip type</td>\n"
			html_all = html_all + "				<td>" + ROM_chip_type + "</td>\n"
			html_all = html_all + "			</tr>\n"

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Special coprocessor/ additional chip</td>\n"
			html_all = html_all + "				<td>" + special_chip + "</td>\n"
			html_all = html_all + "			</tr>\n"

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Mapping type, HiROM/LoROM</td>\n"
			html_all = html_all + "				<td>" + mapping_type + "</td>\n"
			html_all = html_all + "			</tr>\n"



		found, sram_size = self.sram_info()

		if found:

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>SRAM</td>\n"
			html_all = html_all + "				<td>" + sram_size + "</td>\n"
			html_all = html_all + "			</tr>\n"

		found, sram_decoder = self.sram_chip_decoder()

		if found:

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>SRAM decoder chip</td>\n"
			html_all = html_all + "				<td>" + sram_decoder + "</td>\n"
			html_all = html_all + "			</tr>\n"

		found, addition_sram, addition_sram_description = self.additional_sram_info()

		if found:

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Additional SRAM size</td>\n"
			html_all = html_all + "				<td>" + addition_sram + "</td>\n"
			html_all = html_all + "			</tr>\n"

			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Additional SRAM purpose</td>\n"
			html_all = html_all + "				<td>" + addition_sram_description + "</td>\n"
			html_all = html_all + "			</tr>\n"




		html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
		html_all = html_all + "				<td>Board revision</td>\n"
		html_all = html_all + "				<td>" + str(self.pcb['revision_id']) + "</td>\n"
		html_all = html_all + "			</tr>\n"



		if self.pcb['secondary_revision_id']:
			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>Secodnary board revision</td>\n"
			html_all = html_all + "				<td>" + str(self.pcb['secondary_revision_id']) + "</td>\n"
			html_all = html_all + "			</tr>\n"

		html_all = html_all + "		</table>\n"

		html_all = html_all + "		<p></p>\n"

		# add chip information


		html_all = html_all + "		<table class=\"infotable2\">\n"
		html_all = html_all + "		<caption class=\"name\">Chip slot description</caption>\n"
		html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
		html_all = html_all + "				<td width=\"175px\"><b>Slot ID</b></td>\n"
		html_all = html_all + "				<td width=\"500px\"><b>description</b></td>\n"
		html_all = html_all + "			</tr>\n"

		counter = 0
		for chip in self.pcb['chipSlotType']:

			found, chiptype = self.chip_object.return_chiptype(chip['chiptype_id'])
			counter = counter + 1
			html_all = html_all + "			<tr class=\"row" + str(next(myIterator)) + "\">\n"
			html_all = html_all + "				<td>U" + str(chip['slot_number']) + "</td>\n"
			if found:
				html_all = html_all + "				<td>" + chiptype + "</td>\n"
			else:
				html_all = html_all + "				<td>error: could not find chip</td>\n"
			html_all = html_all + "			</tr>\n"

		html_all = html_all + "		</table>\n"

		html_all = html_all + "	<p class=\"name\">Mapping information </p>\n"
		html_all = html_all + "	<p>This info box is taken from the emulators BSNES and HIGAN, by Near. If your game does not run, and the mapping type of the game directs you to this page, you can copy this information into a file called \"manifest.bml\", in the same game folder directory. This should allow the emulator to properly load the game.</p>\n"

		if self.pcb['memoryMapping']:


			html_all = html_all + "	<pre>" + self.pcb['memoryMapping'] + "</pre>\n"	

		else:
			html_all = html_all + "	<p class=\"imagec\"><i>mapping information not found</i></img></p>\n"
			

		html_all = html_all + "	<p class=\"headingtext\">Images</p>\n"



		if (len(self.pcb['contributor_id'])) > 1:
			html_all = html_all + "	<p><b>Image contributors:</b> "
			counter = 1
			for entries in self.pcb['contributor_id']:

				found = self.contributor_obj.load_contributor_id(entries)
				if(found):
					html_all = html_all + self.contributor_obj.return_link()
				if (counter < len(self.pcb['contributor_id'])):
					html_all = html_all + ", "
					counter = counter + 1

			html_all = html_all + "</p>\n"

		elif (len(self.pcb['contributor_id'])) == 1:

			found = self.contributor_obj.load_contributor_id(self.pcb['contributor_id'][0])
			if(found):
				html_all = html_all + "	<p><b>Image contributor:</b> " + self.contributor_obj.return_link() + "</p>\n"
		else:
			print("missing contributor for:" + self.pcb['pcbID'])
			html_all = html_all + "	<p><b>Image contributor:</b> none recorded</p>\n"	

		found, path = self.front_image(self.pcb['pcbID'])
		html_all = html_all + "	<p class=\"name\">PCB Front: </p>\n"
		if (found):
			html_all = html_all + "	<p class=\"imagec\"><img src=\"pcblist/"  +  path + "\"></p>\n"
		else:
			html_all = html_all + "	<p class=\"imagec\"><i>front image not found</i></img></p>\n"


		found, path = self.back_image(self.pcb['pcbID'])

		html_all = html_all + "	<p class=\"name\">PCB Back: </p>\n"
		if (found):
			html_all = html_all + "	<p class=\"imagec\"><img src=\"pcblist/"  +  path + "\"></p>\n"
		else:
			html_all = html_all + "	<p class=\"imagec\"><i>back image not found</i></p>\n"




		return html_all

# let's run this

if __name__ == "__main__":

	pcb_object = pcb()
	pcb_object.load_pcb()
	print(pcb_object.write_html_pcb_page())	
