#!/usr/bin/env python

import yaml
import sys
import os
import itertools
from entries import entries
from titles import titles

sys.path.append('../contributors')
from contributors import contributor


sys.path.append('../pcb')
from pcb import pcb

class rom_image:

	def __init__(self, entry_id = ''):

		# title information is purely what is on the title screen, not based on what is on the box or cart
		self.rom_info = {
		  'rom_id' : '',          # unique ID number for the ROM image
		  'region' : '',          # region id number from self.regions
		  'release_type_id' : [], # what kind of ROM image is this?
		  'crc32': '',            # crc32 hash, which used to be used, but not really considered secure
		  'sha256' : '',          # sha256 hash, the prefered hash of Near
		  'version_number' : '',  # Version number, which is in the internal header of the game
		  'notes' : '',           # notes for this ROM image
		  'serial_code' : '',     # if dumped off a commercial cart, there should be a serial code on the chips
		  'PCB_id_values' : [],   # pointers to PCB types that are associated with this ROM image
		  'title_id_values' : [], # pointers to the titles associated with this ROM image
		  'contributors' : []     # pointers to the contributors associated with this ROM image
		}



		# regions are defined by the first code in the ROM chip (i.e. SNS, SHVC) or in the fourth character on the second code on the ROM chip with the four character format
		# A few exceptions are for the French Canadian release of Link to the Past, and a couple of games released only in Brazil
		self.regions = [
              {'id':0, 'region':'USA'}, 
              {'id':1, 'region':'Japan'}, 
              {'id':2, 'region':'PAL'}, 
              {'id':3, 'region':'France'}, 
              {'id':4, 'region':'Germany'}, 
              {'id':5, 'region':'Spain'}, 
              {'id':6, 'region':'South Korea'}, 
              {'id':7, 'region':'Sweden'}, 
              {'id':8, 'region':'Taiwan/Hong Kong'}, 
              {'id':9, 'region':'Italy'}, 
              {'id':10, 'region':'Netherlands'}, 
              {'id':11, 'region':'Canada'}, 
              {'id':12, 'region':'Brazil'}, 
              {'id':13, 'region':'NTSC'}, # probably relevant for homebrew
              {'id':14, 'region':'unknown'} # stuff with corrupt headers
		] 

		########################################################################################
		# release status is a way to distinguish what kind of ROM it is. Can have multiple values.
		#########################################################################################

		# Licensed: game published with the blessing of Nintendo, complete with box/cart/manual
		# Unlicensed: legitimate game published without the blessing of Nintendo, contemporary to the systems' lifespan (within 2-3 years), with cart/box release
		# Prototype/sample: incomplete version of a released game. Samples tend to have restrictions like a limit on the number of levels. Prototypes can be complete games, but with watermarks or incomplete headers that do not affect the actual game.
		# Demo: program created by licensed developers to show off aspects of the hardware
		# Unreleased: A game produced for the purposes of being a licensed release, but for various reasons never came out to market
		# Satellaview: game released on the BS Satellaview
		# Sufami Turbo: Game released on Bandai's add-on unit
		# Nintendo Power: Game released on the Nintendo Power download kiosk
		# Hack: game that has been modified after the fact, usually unauthorized
		# Bad dump: The binary is corrupt due to poor quality dump, or because of bitrot (with prototypes)
		# Homebrew: game produced after the lifespan of the system
		# Homebrew demo: program designed by enthusiasts for artistic purposes, or for testing different aspects of the hardware
		# BIOS/ Coprocessor: binary use to boot a system or cart, or a complementary chip (e.g. CX-4/DSP-x)
		# Test cart: cart with a program designed to test the integrity of the hardware
		# Competition cart: low print run game or set of games used in contests and competitions
		# Pirate: low quality game published contemporary to the system's lifespan (within 2-3 years), with blatant copyright infringement. Can be hacks.
		# Super Famicom Box: files associated with the special hardware Super Famicom Box
		# SNES / SFC Mini: Games released on the SNES/SFC mini consoles
		# Virtual Console: Games released on the Wii/Wii U/3DS Virtual Console
		# Switch Online: games released on the Nintendo Switch Online service

		# Demo: software produced to work for the system but does not qualify as a game
		self.release_type_list = [
		  {'id':0, 'release_type':'Licensed release'}, 
              {'id':1, 'release_type':'Unlicensed release'}, 
              {'id':2, 'release_type':'Prototype / Sample'}, 
              {'id':3, 'release_type':'Demo'}, 
              {'id':4, 'release_type':'Satellaview'}, 
              {'id':5, 'release_type':'Sufami Turbo'}, 
              {'id':6, 'release_type':'Nintendo Power'}, 
              {'id':7, 'release_type':'Hack'}, 
              {'id':8, 'release_type':'Bad dump'}, 
              {'id':9, 'release_type':'Homebrew'}, 
              {'id':10, 'release_type':'Homebrew demo'}, 
              {'id':11, 'release_type':'BIOS / Coprocessor'}, 
              {'id':12, 'release_type':'Test cart'}, 
              {'id':13, 'release_type':'Competition cart'}, 
              {'id':14, 'release_type':'Pirate'}, 
              {'id':15, 'release_type':'Super Famicom Box'}, 
              {'id':16, 'release_type':'Nintendo Super System'}, 
              {'id':17, 'release_type':'SNES / SFC Mini'}, 
              {'id':18, 'release_type':'Virtual Console'}, 
              {'id':19, 'release_type':'Switch Online'},
              {'id':21, 'release_type':'Unreleased'}
		]


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "rom_images.yaml"

		# create a titles object

		self.title_object = titles(self.entry_object.entry['id_number'])

		# create pcb object

		self.pcb_object = pcb()

		# create contributors object

		self.contributors_object = contributor()

		# load yaml file containing the dictionary of the ROM image entries

		self.rom_image_archive = []
		self.largest_rom_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.rom_image_archive.insert(i, RIP.copy())
					if self.rom_image_archive[i]['rom_id'] > self.largest_rom_id:
						self.largest_rom_id = self.rom_image_archive[i]['rom_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any ROM images entered yet\n")



		self.number_of_rom_images = i

		self.current_entry = ''


		self.html_file = "rom_images.html"
		self.temp_file = "temp/" + self.html_file

	# clears entry
	def clear_entry(self):
		
		for item in self.rom_info:
			if type(self.rom_info[item]) is list:
				self.rom_info[item] = []
			else:
				self.rom_info[item] = ''


	def load_rom_image_info(self, rom_id):


		self.clear_entry()
		# find the game_id
		found = False
		try: 

			rom_id = int(rom_id)

			for i in range(len(self.rom_image_archive)):

				if rom_id == self.rom_image_archive[i]['rom_id']:
					self.rom_info.update(self.rom_image_archive[i])
					found = True
					break

			if not found:
				print(f"ROM image id {rom_id} does not exit")

		except ValueError:
			print("You must enter an integer corresponding to a ROM id")

		return found

	def replace_rom_image_info(self,rom_id):

		found = False
		try: 

			rom_id = int(rom_id)

			for i in range(len(self.rom_image_archive)):

				if rom_id == self.rom_image_archive[i]['rom_id']:
					self.rom_image_archive[i] = self.rom_info
					found = True
					break

			if not found:
				print(f"ROM image id {rom_id} does not exit")

		except ValueError:
			print("You must enter an integer corresponding to a ROM id")

		return found		

	##############################################
	##### Prints out the rom image information #####
	##############################################

	def print_rom_image_info(self):
		print(f"ROM images for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_rom_images > 0:

			for rom_image in self.rom_image_archive:

				# get the title of the ROM image


				for title_id in rom_image['title_id_values']:

					self.title_object.load_title(title_id)
					title = self.title_object.title['title']



					found2, region = self.find_region_name(rom_image['region'])
					
					print(f"{rom_image['rom_id']}) {title} {rom_image['version_number']} ({region}) crc32: {rom_image['crc32']}")
						
		else:
			print("no ROM images entered yet")

	def print_current_rom_image_info(self):

		for title_id in self.rom_info['title_id_values']:

			self.title_object.load_title(title_id)
			title = self.title_object.title['title']
			found2, region = self.find_region_name(self.rom_info['region'])
			print(f"{self.rom_info['rom_id']}) {title} {self.rom_info['version_number']} ({region}) crc32: {self.rom_info['crc32']}")


	##############################################
	##### grab the rom image id #####
	##############################################

	def load_rom_image(self,rom_id = ''):


		if len(self.rom_image_archive) > 0:

			if rom_id == '':

				found_id = False

				self.print_rom_image_info()

				while not found_id:

					print("select id number")
					rom_id = input(">>> ")

					found = self.load_rom_image_info(rom_id)

					if found:
						found_id = True

			else:
				found = self.load_rom_image_info(rom_id)


		return found


	##############################################
	##### Add new rom image information #####
	##############################################

	def add_rom_image(self):


		self.clear_entry()

		# assign new ROM image ID
		self.largest_rom_id += 1
		self.rom_info['rom_id'] = self.largest_rom_id
		
		# add title

		self.rom_info['title_id_values'] = self.title_object.get_id_list()


		# add region
		self.add_region()

		# add release_type_id

		self.add_release_status()

		# add crc32

		self.add_crc32()

		# add sha256

		self.add_sha256()

		# add serial code

		self.add_serial_code()

		# add version number

		self.add_version_number()

		# add PCB board types

		self.rom_info['PCB_id_values'] = self.pcb_object.get_id_list()

		# add contributors

		self.rom_info['contributors'] = self.contributors_object.add_contributor_list()

		# add notes
		self.add_notes()

		self.rom_image_archive.append(self.rom_info)
		self.number_of_rom_images += 1

		self.__write_rom_image_info()

	def edit_rom_image(self,id_number=''):

		changed = False

		done = False
		while not done:
			self.print_rom_image_info()
			print("\nType ROM image id number you want to edit (doesn't need to be in the above list if it is associated with another game) (q to quit)")
			rom_id = input(">>> ")

			if rom_id == 'q':
				done = True
			else:
				done = self.load_rom_image_info(rom_id)

		if rom_id != "q":

			# edit title

			self.title_object.print_selected_titles(self.rom_info['title_id_values'])
			print("\nTo edit (e) (enter to continue)")
			option = input(">>> ")
			if option == "e":
				self.rom_info['title_id_values'] = self.title_object.get_id_list(self.rom_info['title_id_values'])
				changed = True


			# edit region

			self.print_region(self.rom_info['region'])
			print("\nDo you want to change the region ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_region()
				changed = True


			# edit release status

			self.print_release_status()

			print("\nRelease status, add (a), or delete (d) (enter to continue)")

			option = input(">>> ")


			if option == "a":
				self.add_release_status()
				changed = True
			elif option == "d":
				self.delete_release_status()
				changed = True


			# change CRC32

			self.print_crc32()

			print("\nDo you want to change the CRC32 ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_crc32()
				changed = True

			# change sha256

			self.print_sha256()

			print("\nDo you want to change the sha256 ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_sha256()
				changed = True

			# change serial code

			self.print_serial_code()

			print("\nDo you want to change the serial code ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_serial_code()
				changed = True

			# change version number

			self.print_version_number()

			print("\nDo you want to change the version number ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_version_number()
				changed = True



			# change PCB board types

			self.pcb_object.print_current_pcbs(self.rom_info['PCB_id_values'])

			print("\nPCB boards, add (a), or delete (d) (enter to continue)")

			option = input(">>> ")


			if option == "a":
				self.rom_info['PCB_id_values'] = self.pcb_object.get_id_list(self.rom_info['PCB_id_values'])
				changed = True
			elif option == "d":
				self.rom_info['PCB_id_values'] = self.pcb_object.delete_from_id_list(self.rom_info['PCB_id_values'])
				changed = True


			# change contributors

			self.contributors_object.print_contributor_list(self.rom_info['contributors'])

			print("\nContributors, add (a), or delete (d) (enter to continue)")

			option = input(">>> ")

			if option == "a":
				self.rom_info['contributors'] = self.contributors_object.add_contributor_list(self.rom_info['contributors'])
				changed = True
			elif option == "d":
				self.rom_info['contributors'] = self.contributors_object.delete_contributor_list(self.rom_info['contributors'])
				changed = True


			# change notes

			self.print_notes()

			print("\nDo you want to change the notes ('y' for yes)")
			option = input(">>> ")
			if option == "y":
				self.add_notes()
				changed = True

			# replace the information
			if changed:
				self.replace_rom_image_info(rom_id)
				self.__write_rom_image_info()

		


		return changed

	def __write_rom_image_info(self):

		if self.number_of_rom_images > 0:

			file = open(self.data_filename,'w')
			yaml.dump(self.rom_image_archive, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No ROM image information has been added")



	##############################################
	##### Methods associated with the region #####
	##############################################

	def print_all_regions(self):

		print("Available regions: \n")
		for region in self.regions:

			if region['id'] < 10:
				print(f" {region['id']}) {region['region']} ")
			else: 
				print(f"{region['id']}) {region['region']} ")


	def add_region(self):


		self.print_all_regions()

		print("\ntype the region you want:\n")
		region_id = input(">>> ")

		self.update_region(region_id)

	def update_region(self, region_id):

		found = False
		try:
			region_id = int(region_id)

			for region in self.regions:
				if region_id ==  region['id']:
					found = True
					break
			if found:
				self.rom_info['region'] = region_id
			else:
				print("you must enter an integer corresponding to the correct region")

		except ValueError:
			print("you must enter an integer")

		return found

	def find_region_name(self,region_id):

		# return values
		found = False
		region_name = ""

		region_id = int(region_id)

		for region in self.regions:
			if region_id ==  region['id']:

				region_name = region['region']
				found = True
				break

		return found, region_name
	
	def print_region(self,region_id):
		found= False

		try:
			region_id = int(region_id)

			for region in self.regions:
				if region_id ==  region['id']:
					found = True
					print(f"\nRegion: {region['region']}")
					break

			else:
				print("you must enter an integer corresponding to the correct region")

		except ValueError:
			print("you must enter an integer")



	######################################################
	##### Methods associated with the release status #####
	######################################################

	def print_all_release_status(self):

		print("Available release status values: \n")
		for release_type in self.release_type_list:

			if release_type['id'] < 10:
				print(f" {release_type['id']}) {release_type['release_type']} ")
			else: 
				print(f"{release_type['id']}) {release_type['release_type']} ")

	def add_release_status(self):

		done = False

		while not done:

			self.print_all_release_status()

			print("\ntype the release type you want ('f' to finish):\n")
			release_type_id = input(">>> ")

			if release_type_id == "f":
				done = True
			else:
				self.update_release_status(release_type_id)

	def check_release_status(self, release_type_id):

		found = False
		try:
			release_type_id = int(release_type_id)

			for release_type in self.release_type_list:
				if release_type_id ==  release_type['id']:
					found = True
					break

			else:
				print("you must enter an integer corresponding to the correct release status")

		except ValueError:
			print("you must enter an integer")

		return found

	def update_release_status(self, release_type_id):

		found = self.check_release_status(release_type_id)

		if found:
			release_type_id = int(release_type_id)
			self.rom_info['release_type_id'].append(release_type_id)

			# check to make sure there are no duplicates

			self.rom_info['release_type_id'].sort()
			number_status = len(self.rom_info['release_type_id'])

			counter = 0
			while counter < number_status-1:
				if self.rom_info['release_type_id'][counter] == self.rom_info['release_type_id'][counter+1]:
					del self.rom_info['release_type_id'][counter+1]
					number_status -= 1
				else:
					counter += 1

		return found

	def get_release_status(self,release_id):

		for rt in self.release_type_list:
			if release_id == rt['id']:
				release_status = rt['release_type']
		return release_status

	def print_release_status(self):

		print("\nCurrent release types associated with this ROM image:")
		for rs in self.rom_info['release_type_id']:
			for rt in self.release_type_list:
				if rs == rt['id']:
					print(f"{rs}) {rt['release_type']}")

	def delete_release_status(self):

		done = False

		while not done:

			if self.rom_info['release_type_id']:

				self.print_release_status()

				print("\nEnter the release type id number you want to delete ('f' to finish)")

				option = input(">>> ")
				if option == 'f':
					done = True
				else:

					try:
						option = int(option)
						counter = 0
						found = False
						for rs in self.rom_info['release_type_id']:

							if rs == option:
								found = True
								break
							else:
								counter += 1
						print(found)
						if found:
							del self.rom_info['release_type_id'][counter]

					except ValueError:
						print("you must enter an integer")

			else:
				done = True

		

	def find_release_status(self,release_status_values):

		# return values
		found = False
		release_status_types = []

		for release_status in release_status_values:
			for release_type in self.release_type_list:
				if release_status ==  release_type['id']:

					release_status_types.append(release_type['release_type'])
					found = True


		return found, release_status_types

	#############################
	##### Methods for crc32 #####
	#############################

	def add_crc32(self):

		print("\nEnter the crc32:")
		crc_value = input(">>> ")
		self.update_crc32(crc_value)

	def update_crc32(self,crc32):

		found = False
		try:

			test_int = int(crc32, 16)

			if(len(crc32) == 8):
				self.rom_info['crc32'] = crc32
				found = True
			else:
				print("Invalid crc32 value")

		except ValueError:
			print("Invalid crc32 value")

	def print_crc32(self):
		print(f"\nCRC32: {self.rom_info['crc32']}")

	##############################
	##### Methods for sha256 #####
	##############################

	def add_sha256(self):

		print("\nEnter the sha256:")
		sha_value = input(">>> ")
		self.update_sha256(sha_value)

	def update_sha256(self,sha256):

		found = False
		try:

			test_int = int(sha256, 16)

			if(len(sha256) == 64):
				self.rom_info['sha256'] = sha256
				found = True
			else:
				print("Invalid sha256 value")

		except ValueError:
			print("Invalid sha256 value")

	def print_sha256(self):
		print(f"\nsha256: {self.rom_info['sha256']}")

	###################################
	##### Methods for serial code #####
	###################################

	def add_serial_code(self):

		# there isn't really any way to check this that would be easy
		print("\nEnter the serial code:")
		sc = input(">>> ")
		self.rom_info['serial_code'] = sc

	def print_serial_code(self):
		print(f"\nserial code: {self.rom_info['serial_code']}")

	######################################
	##### Methods for version_number #####
	######################################

	def add_version_number(self):

		# I'm leaving this open ended, but for commercial games it can be 1.0, 1.1, 1.2, etc
		print("\nEnter the version number:")
		vn = input(">>> ")
		self.rom_info['version_number'] = vn

	def print_version_number(self):
		print(f"\nversion number: {self.rom_info['version_number']}")




	######################################
	##### Methods for notes #####
	######################################

	def add_notes(self):

		
		print("\nEnter any notes (note, add html tags as necessary):")
		notes = input(">>> ")
		self.rom_info['notes'] = notes
		

	def print_notes(self):
		if self.rom_info['notes']:
			print(f"\nNotes: \"{self.rom_info['notes']}\"")
		else:
			print("\nThere are no notes for this entry")



# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	rom_image_object = rom_image()

	print("Options:")
	print("1) add ROM image entry")
	print("2) edit ROM image entry")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			rom_image_object.add_rom_image()
		elif(entry==2):
			rom_image_object.edit_rom_image()
	#	elif(entry==3):
	#		rom_image_object.create_html_file()
		else:
			print("fail")

	except ValueError:
		print("fail")
			
