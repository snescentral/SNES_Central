import sys
import os
import shutil
import filecmp
import hashlib
import copy
from entries import entries

class file_handling:

	file_sections = ['screenshots', 'carts', 'manuals', 'boxes_front', 'boxes_back', 'boxes', 'pcbs_front', 'pcbs_back', 'pcbs', 'other_images', 'article_headers']

	extensions = [ 
	  {'section_type': 'screenshots', 'extensions_def': ('.png', '.jpg', '.jpeg', '.gif')},
	  {'section_type': 'carts', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'manuals', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'boxes_front', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'boxes_back', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'pcbs_front', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'pcbs_back', 'extensions_def': ('.png', '.jpg', '.jpeg')},
	  {'section_type': 'other_images', 'extensions_def': ('.png', '.jpg', '.jpeg', '.gif')},
	  {'section_type': 'article_headers', 'extensions_def': ('.txt',)}
	]

	snes_central_path = "../SNES_Central/"

	filename = ''
	storage_filename = ''
	file_extension = ''
	current_path = ''
	current_file = ''

	extension_options = ''
	
	entry_object=''

	entry_id_loaded = False
	current_entry_id = -1


	def __init__(self,entry_id=''):

		if entry_id == '':
			print("You must enter an entry_id")
			exit()

		found_entry = False
		self.entry_object = entries()

		found_entry, entry_id = self.entry_object.load_entry(entry_id)

		if not found_entry:
			print(f"error, {entry_id} is not a valid entry id number")
			exit()

	def add_file(self,section,file_id):

		self.check_section(section)


		self.grab_filename(section)

		path = self.entry_object.data_path


		self.current_path = path + section + "/"

		self.check_path(self.current_path)

		self.current_file = path + section + "/" + str(file_id) + self.file_extension

		self.move_file(self.filename, self.current_file,check=True)

	def check_file(self,section,file_id):
		path = self.entry_object.data_path
		self.current_path = path + section + "/"

		self.load_extensions(section)
		filename = ""
		for extension in self.extension_options:
			filename = str(file_id) + extension
			full_filename = path + section + "/" + filename

			fileexists = os.path.isfile(full_filename)

			if fileexists:

				break

		return fileexists, filename

	def get_filename(self,section,file_id):


		fileexists, filename = self.check_file(section,file_id)

		if not fileexists:

			print(f"something went wrong, the file for file_id {file_id} does not exist")
			exit()
			
		return filename

	def get_sc_path(self,section):

		sc_path = self.entry_object.sc_data_path


		sc_path = "article_files/" + sc_path + section + "/"

		self.check_path(self.snes_central_path + sc_path)

		return sc_path

	def move_html_to_site(self,file_temp,file_site,section=''):

		if section:
			self.check_section(section)
			section_component = section + "/"
		else:
			section_component = ""

		sc_path = self.entry_object.sc_data_path
		sc_file = self.snes_central_path +  "article_files/" + sc_path + section_component 

		self.check_path(sc_file)

		sc_file = sc_file + file_site

		filename_db = file_temp

		file_move = self.copy_file(filename_db,sc_file,False)
		if(file_move):
			print(f"{file_site} moved to SNES Central")

	def move_to_site(self,section,file_id):

		self.check_section(section)

		filename = self.get_filename(section,file_id)		

		filename_db = self.current_path + filename

		sc_path = self.get_sc_path(section)

		sc_file = self.snes_central_path + sc_path + filename

		file_move = self.copy_file(filename_db,sc_file)
		if(file_move):
			print(f"{section} file with id {file_id} moved to SNES Central")

	def check_section(self,section):

		if not section in self.file_sections:
			print(f"error, {section} is not a valid file section")
			exit()

	def check_path(self,path):

		if not os.path.exists(path):
			os.makedirs(path)

	def load_extensions(self,section):

		for extension_temp in self.extensions:
			if extension_temp['section_type'] == section:
				self.extension_options = copy.deepcopy(extension_temp['extensions_def'])


	def grab_filename(self,section):

		self.load_extensions(section)

		ending = False

		while not ending:

			if section == "article_headers": # automatically generate the article files
				# create dummy text file
				filename = "temp.txt"
				f = open(filename, "x")
				f.close()
			else:
				print("Enter file name: ")
				filename = input(">>> ")

			if os.path.isfile(filename) and  filename.lower().endswith(self.extension_options):
				self.filename = filename
				ending = True
			else:
				print("invalid file name")

		self.file_extension = os.path.splitext(self.filename)[1]



	def move_file(self,file1, file2, check='False'):
		proceed = False
		fileexists = os.path.isfile(file2)
		if fileexists:

			filesame = filecmp.cmp(file1,file2)
			if not filesame:
				if check:

					print("File already exists, do you want to overwrite? y/n")
					result = input(">>> ")
					if result == 'y':
						proceed = True

				else:
					proceed = True
		else:
			proceed = True

		if proceed:
			shutil.move(file1,file2)

		return proceed

	def copy_file(self,file1, file2, check='False'):
		proceed = False
		fileexists = os.path.isfile(file2)
		if fileexists:

			filesame = filecmp.cmp(file1,file2)
			if not filesame:
				if check:

					print("File already exists, do you want to overwrite? y/n")
					result = input(">>> ")
					if result == 'y':
						proceed = True

				else:
					proceed = True
		else:
			proceed = True

		if proceed:
			shutil.copyfile(file1,file2)

		return proceed

	def sha256_calc(self,filename):
		BUF_SIZE = 65536
		sha256 = hashlib.sha256()

		full_filename = self.current_path + filename
		with open(full_filename, 'rb') as f:
			while True:
				data = f.read(BUF_SIZE)
				if not data:
				    break
				sha256.update(data)

		return format(sha256.hexdigest())
