import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from titles import titles
from file_handling import file_handling
from shared_values import shared_values

sys.path.append('../contributors')
from contributors import contributor


class boxes:
	def __init__(self, entry_id = ''):

		self.box_data = []

		self.box = {
		  'box_id': '', # Unique number for each box. This is an integer.
		  'region_id' : '', # id for the region of the box
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this box
		  'title_id_values' : [], # pointers to the titles associated with this box
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this box label
		  'has_front_image' : '', # it is possible to not have an image
		  'has_back_image' : '' # it is possible to not have an image
		}


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "boxes.yaml"

		# create a shared values object

		self.shared_object = shared_values()

		# create a titles object

		self.title_object = titles(self.entry_object.entry['id_number'])

		# create contributors object

		self.contributors_object = contributor()


		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])



		# load yaml file containing the dictionary of the box scan entries

		self.largest_box_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.box_data.insert(i, RIP.copy())
					if self.box_data[i]['box_id'] > self.largest_box_id:
						self.largest_box_id = self.box_data[i]['box_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any box scans entered yet\n")



		self.number_of_boxes = i

		# site files
		self.table_file = "box_table.html"
		self.table_temp_file = "temp/" + self.table_file 

		self.check_file = "box_check.txt"
		self.check_temp_file = "temp/" + self.check_file


	def print_box_info(self):
		print(f"boxes for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_boxes > 0:

			# first sort the data

			self.box_data = sorted(self.box_data, key=lambda k: k['box_id']) # sort the entries by id

			for box in self.box_data:

				self.title_object.load_title(box['title_id_values'][0])
				title = self.title_object.title['title']

				self.shared_object.load_country(box['country_id'])
				country = self.shared_object.country_selection['country']

				print(f"{box['box_id']}) {title} ({country}) ({box['serial_code']})")
		else:
			print("There are currently no boxes entered")

	def clear_box(self):
		temp_box = {
		  'box_id': '', # Unique number for each box. This is an integer.
		  'region_id' : '', # id for the region of the box label
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this box
		  'title_id_values' : [], # pointers to the titles associated with this box
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this box label
		  'has_front_image' : '', # it is possible to not have an image
		  'has_back_image' : '' # it is possible to not have an image
		}

		self.box.update(temp_box)

	def load_box(self,box_id):

		found = False
		try: 

			box_id = int(box_id)
			for box in self.box_data:
				if box['box_id'] == box_id:
					self.box.update(box)
					found = True
					break

			if not found:
				print(f"Box id {box_id} does not exit")

		except ValueError:
			print("you must enter an integer")

		return found

	def add_box(self):

		self.clear_box()


		self.largest_box_id += 1

		self.box['box_id'] = self.largest_box_id



		self.box['region_id'] = self.shared_object.return_region()

		# add title

		self.box['title_id_values'], changed = self.title_object.get_id_list()

		if not changed:
			print("It is necessary to add a title")
			sys.exit()


		self.box['country_id'] = self.shared_object.return_country()	
		self.box['serial_code'] = self.shared_object.add_serial_code()		

	
		self.box['manufacturing_id'] = self.shared_object.return_manufacturing_location()
		self.box['company_id_values'] = self.shared_object.return_company()

		# add contributors

		self.box['contributors'], changed = self.contributors_object.add_contributor_list()

		self.box['notes'] = self.shared_object.add_notes()

		print("Do you want to add a front box image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('boxes_front',self.box['box_id'])
			self.box['has_front_image'] = True
		else:
			self.box['has_front_image'] = False

		print("Do you want to add a back box image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('boxes_back',self.box['box_id'])
			self.box['has_back_image'] = True
		else:
			self.box['has_back_image'] = False



		self.box_data.append(self.box)

		self.write_info()

	def edit_box(self, box_id = ''):

		self.print_box_info()

		update = False

		self.clear_box()

		if box_id == '':

			print("enter the box id you want")
			box_id = input(">>> ")

		try:

			box_id = int(box_id)
			found = self.load_box(box_id)

		except ValueError:
			print("error, you must input a number")

		if not found:
			print("could not find the entry, exiting")
			sys.exit()


		print("Do you want to add/change a front box image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('boxes_front',self.box['box_id'])
			self.box['has_front_image'] = True


		print("Do you want to add/change a back box image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('boxes_back',self.box['box_id'])
			self.box['has_back_image'] = True



		
		changed, id_value = self.shared_object.edit_region(self.box['region_id'])
		if changed:
			self.box['region_id'] = id_value
			update = True

		# add title

		id_value, changed = self.title_object.get_id_list(self.box['title_id_values'])
		if changed:
			self.box['title_id_values'] = id_value
			update = True

		changed, id_value = self.shared_object.edit_country(self.box['country_id'])
		if changed:
			self.box['country_id'] = id_value
			update = True


		changed, code = self.shared_object.edit_serial_code(self.box['serial_code'])
		if changed:
			self.box['serial_code'] = code
			update = True

	
		changed, id_value = self.shared_object.edit_manufacturing_location(self.box['manufacturing_id'])
		if changed:
			self.box['manufacturing_id'] = id_value
			update = True


		changed, id_value = self.shared_object.edit_company(self.box['company_id_values'])
		if changed:
			self.box['company_id_values'] = id_value
			update = True


		# add contributors

		id_value, changed = self.contributors_object.add_contributor_list(self.box['contributors'])
		if changed:
			self.box['contributors'] = id_value
			update = True

		notes, changed = self.shared_object.edit_notes(self.box['notes'])
		if changed:
			self.box['notes'] = notes
			update = True

		if update:
			counter = 0
			for box in self.box_data:
				if box['box_id'] == self.box['box_id']:
					self.box_data[counter] = self.box
					break
				else:
					counter += 1

			self.write_info()




	def write_info(self):

		if len(self.box_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.box_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No box information has been added")

	def create_html_files(self):

		myIterator = itertools.cycle([1,2])

		# This goes through the list and creates a summary table with links to individual pages. 
		# It also creates a text file that the site uses to check for valid entries and prevent hacking problems.

		check_file = open(self.check_temp_file, 'w')

		# write the table file
		html_file = open(self.table_temp_file, 'w')


		# first create the base parts of table file

#		html_table = '<p class="name">boxridge label information</p>\n'

		html_table = ""

		html_file.write("		<div class=\"chip_table\">\n")
		html_file.write("			<div class=\"chip_heading\">\n")
		html_file.write("				<div class=\"chip_heading_text\">Box information</div>\n")
		html_file.write("			</div>\n")
		html_file.write("			<div class=\"chip_bulk\">\n")
		html_file.write("			</div>\n")



		html_file.write("		<table class=\"infotable\">\n")
		html_file.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
		html_file.write("				<td width=\"90px\"><b>Region</b></td>\n")
		html_file.write("				<td width=\"110px\" ><b>Country</b></td>\n")
		html_file.write("				<td width=\"140px\" ><b>Serial Code</b></td>\n")
		html_file.write("				<td width=\"240px\" ><b>Notes</b></td>\n")
		html_file.write("				<td width=\"40px\" ><b>Front scan</b></td>\n")
		html_file.write("				<td width=\"40px\" ><b>Back Scan</b></td>\n")
		html_file.write("				<td width=\"40px\" ><b>Link</b></td>\n")
		html_file.write("			</tr>\n")

		# first sort the data
		self.box_data = sorted(self.box_data, key=lambda k: k['serial_code']) # sort the entries by serial code
		self.box_data = sorted(self.box_data, key=lambda k: k['country_id']) # sort the entries by country
		self.box_data = sorted(self.box_data, key=lambda k: k['region_id']) # then sort the entries by region

		row_number = 1

		# loop through all the boxes
		for box in self.box_data:


			self.title_object.load_title(box['title_id_values'][0])

			if box['has_front_image'] or box['has_back_image']:
				check_file.write(str(box['box_id']) +  "\t" + self.title_object.title['title'] + " (" + box['serial_code'] + ")\n")

			# start row of the table

			html_file.write("	<tr class = \"row" + str(next(myIterator)) + "\">\n")

			# region value
			self.shared_object.load_region(box['region_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.region_selection['region'] + "</td>\n")

			# country value
			self.shared_object.load_country(box['country_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.country_selection['country'] + "</td>\n")

			# serial code
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + box['serial_code'] + "</td>\n")

			# notes
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + box['notes'] + "</td>\n")

			if box['has_front_image']:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">yes</td>\n")
			else:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">no</td>\n")

			if box['has_back_image']:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">yes</td>\n")
			else:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">no</td>\n")

			if box['has_front_image'] or box['has_back_image']:

				html_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"box_test.php?id=" + str(self.entry_object.full_id) + "&num=" + str(box['box_id']) + "\">link</a></td>\n")

			else:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
	


			# finish the table row

			html_file.write("	</tr>\n")
			if row_number == 1:
				row_number = 2
			else:
				row_number = 1

			if box['has_front_image'] or box['has_back_image']:

				# move the scan to the site

				if box['has_front_image']:
					self.file_object.move_to_site('boxes_front',box['box_id'])
				if box['has_back_image']:
					self.file_object.move_to_site('boxes_back',box['box_id'])

				# create html page for the scan

				myIterator2 = itertools.cycle([2,1])

				temp_file="temp/page.html"
				page_html_file = open(temp_file, 'w')

				self.title_object.load_title(box['title_id_values'][0])




				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">" + self.title_object.title['title'] + " (" + box['serial_code'] + ") - box information</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")

				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\"><center>Front Scan</center></div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")

				if box['has_front_image']:
					sc_path = self.file_object.get_sc_path('boxes_front')

					page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('boxes_front',box['box_id']) + "\" alt=\"Box Front - " + self.title_object.title['title'] + " (" + box['serial_code'] + ")\" /></center>\n")

				else:

					page_html_file.write("				<center>Box front scan not available</center>\n")
	
				page_html_file.write("			</div>\n")

				page_html_file.write("		</div>\n")

				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\"><center>Back Scan</center></div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")


				if box['has_back_image']:
					sc_path = self.file_object.get_sc_path('boxes_back')

					page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('boxes_back',box['box_id']) + "\" alt=\"Box back - " + self.title_object.title['title'] + " (" + box['serial_code'] + ")\" /></center>\n")

				else:

					page_html_file.write("				<center>Box back scan not available</center>\n")
	
				page_html_file.write("			</div>\n")


				page_html_file.write("		</div>\n")


				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">Box documentation</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")


				page_html_file.write("				<table class=\"infotable2\">\n")
				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td width=\"150px\"><b>Item</b></td>\n")
				page_html_file.write("						<td width=\"550px\" ><b>Description</b></td>\n")
				page_html_file.write("					</tr>\n")

				# titles

				for title_id in box['title_id_values']:
					self.title_object.load_title(title_id)

					language_id, language =  self.title_object.ask_for_language( self.title_object.title['language_id'])

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Title (" + language + ")</td>\n")
					if self.title_object.title['translation']:
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "(" +  self.title_object.title['translation'] + ")</td>\n")
					else: 
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "</td>\n")
					page_html_file.write("					</tr>\n")

				# region value
				self.shared_object.load_region(box['region_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Region</td>\n")
				page_html_file.write("						<td>" + self.shared_object.region_selection['region'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# country value

				self.shared_object.load_country(box['country_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Country</td>\n")
				page_html_file.write("						<td>" + self.shared_object.country_selection['country'] + " (" + self.shared_object.country_selection['region_code'] + ")</td>\n")
				page_html_file.write("					</tr>\n")

				# location manufactured
				
				self.shared_object.load_manufacturing_location(box['manufacturing_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Manufacturing location</td>\n")
				page_html_file.write("						<td>" + self.shared_object.manufacturing_location_selection['manufacturing_location'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# serial code

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Serial code</td>\n")
				page_html_file.write("						<td>" + box['serial_code'] +  "</td>\n")
				page_html_file.write("					</tr>\n")

				# companies
				
				for company_id in box['company_id_values']:

					self.shared_object.load_company(company_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Company</td>\n")
					page_html_file.write("						<td>" +  self.shared_object.company_selection['company'] +  "</td>\n")
					page_html_file.write("					</tr>\n")


				# contributors

				for contributor_id in  box['contributors']:
					self.contributors_object.load_contributor_id(contributor_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Contributor</td>\n")
					page_html_file.write("						<td>" +  self.contributors_object.return_link() +  "</td>\n")
					page_html_file.write("					</tr>\n")

				#notes


				if box['notes']:
					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Notes</td>\n")
					page_html_file.write("						<td>" + box['notes'] +  "</td>\n")
					page_html_file.write("					</tr>\n")

				page_html_file.write("				</table>\n")

				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")

				
				page_html_file.close()

				# move to site


				sc_file = str(box['box_id']) + ".html"
				self.file_object.move_html_to_site(temp_file, sc_file, "boxes")

		# finish off making the table

		html_file.write("</table>\n")

		html_file.write("		</div>\n")

		html_file.write("<p><i>Do you have a label variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1097\">Submission guidelines</a> for boxridge label scans.</p>\n")
		html_file.close()
		self.file_object.move_html_to_site(self.table_temp_file, self.table_file)

		check_file.close()


		self.file_object.move_html_to_site(self.check_temp_file, self.check_file)
# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	box_object = boxes()

	print("Options:")
	print("1) add box entry")
	print("2) edit box entry")
	print("3) Create HTML files for box information")
	print("4) Print boxes in database")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			box_object.add_box()
		elif(entry==2):
			box_object.edit_box()
		elif(entry==3):
			box_object.create_html_files()
		elif(entry==4):
			box_object.print_box_info()
		else:
			print("fail")

	except ValueError:
		print("fail")
