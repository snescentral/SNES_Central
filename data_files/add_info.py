#!/usr/bin/env python3

import sys
import os
from entries import entries
from titles import titles
from rom_image import rom_image

data_entry_types = [

  {'id_number': 0, 'description':'Game Title'},
  {'id_number': 1, 'description':'ROM image'}

]

# set to false when you want to start writing
debugging = True

entrylist = entries()

print ('Start by entering the input id number (or "a" to add a new entry)')
id_number = input('>>> ')

found=False


if id_number == "a":
	print ("input id number: ")
	id_number = input(">>> ")
	print("input title: ")
	title = input(">>> ")

	entrylist.clear_entry()
	success = entrylist.add_entry(id_number,title)

	if success:

		if not debugging:
			entrylist.write_entries()

	else:
		print("Failed to add entry")
		sys.exit()


found = entrylist.load_entry(id_number)



if not found:
	sys.exit()
		
done = False

while not done:
	print(f"\nNow working on: {entrylist.entry['title']} (id: {entrylist.entry['id_number']})\n")
	print(f"Select the type of data you want to add (q to end):")

	for selection in data_entry_types:
		print(f"{selection['id_number']}) {selection['description']}")

	select_value = input(">>> ")

	if select_value == "q":
		done = True
	else:

		try:
			select_value = int(select_value)
			found_select = False
			for selection in data_entry_types:
				if select_value == selection['id_number']:
					found_select = True
					break

			if found_select:
				if select_value == 0:
					title_object = titles()
					title_object.load_titles(entrylist.entry['id_number'])
					title_object.list_titles()
					print("\nType \"a\" to add, \"e\" to edit")
					add_edit = input(">>> ")

					if add_edit == "a":
						title_object.add_titles()
						title_object.write_titles(select_value)
					elif add_edit == "e":
						title_object.edit_title()
						title_object.write_titles(select_value)
					else:
						print("incorrect entry")

				if select_value == 1:
					rom_object = rom_image()
					rom_object.print_rom_image_info(entrylist.entry['id_number'])
					print("\nType \"a\" to add, \"e\" to edit")
					add_edit = input(">>> ")

					if add_edit == "a":
						rom_object.add_rom_image(entrylist.entry['id_number'])
						rom_object.write_rom_image_info()
					elif add_edit == "e":
						changed = rom_object.edit_rom_image(entrylist.entry['id_number'])
						if changed:
							rom_object.write_rom_image_info()
						print("nothing yet")
					else:
						print("incorrect entry")
			else:
				print("\n***** invalid number selection*****")

		except ValueError:
			print("\n***** invalid input *****")




	

