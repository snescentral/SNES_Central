#!/usr/bin/env python3

import yaml
import itertools
import os
import re

class entries:

	def __init__(self):

		self.entry = {

		  'id_number' : '',
		  'title' : '',

		}

		self.entry_loaded = False

		self.all_entries = []
		self.entries_file="entries.yml"

		# load yaml file
		i=0
		try: 
			file = open(self.entries_file, 'r')
			self.datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for load_entries in self.datastore:
				for entries in load_entries:
					self.all_entries.insert(i, entries.copy())
					i+=1
			file.close()
		except FileNotFoundError:
			print(f"{self.entries_file} does not exit yet")


		self.number_of_entries = i

		self.full_id = ''
		self.data_path = ''
		self.sc_data_path = ''


	# loads the information related to the specified id
	def load_entry(self,id_number=''):

		if id_number == '':
			print("enter id number:")
			id_number = input(">>> ")


		found = False

		try:

			id_number = int(id_number)

			for i in range(len(self.all_entries)):

				if id_number == self.all_entries[i]['id_number']:
					self.entry.update(self.all_entries[i])
					found = True
					self.entry_loaded = True

					# set full id number
					self.__path_id()

					# set database path
					self.__path()

					# set SNES Central path
					self.__sc_path()

					break

			if not found:
				print(f"could not find id: {id_number}")
		except ValueError:
			print("you must enter an id number")

		return found, id_number

	# clears entry
	def clear_entry(self):
		
		self.entry_loaded = False

		for item in self.entry:
			self.entry[item] = ''

		self.full_id = ''
		self.data_path = ''
		self.sc_data_path = ''



	# sets the path id number on the SNES Central website
	def __path_id(self):

		try:
			id_number = int(self.entry['id_number'])
			if id_number < 10:
				self.full_id = "000" + str(id_number)
			elif id_number < 100:
				self.full_id = "00" + str(id_number)
			elif id_number < 1000:
				self.full_id = "0" + str(id_number)
			# if there are more than 10000 entries, this will have to be updated
			else:
				self.full_id = str(id_number)

		except ValueError:
			print("error, the id_number in path_Id() must be an integer")
			sys.exit()



	# sets the path where the data are
	def __path(self):

		removed_title = re.sub('[^A-Za-z0-9]+', '_', self.entry['title'])
		self.data_path =  self.full_id[0] + '/' + self.full_id[1] + '/' + self.full_id + '_' + removed_title + '/'



	# sets the path where the data are on the SNES Central website
	def __sc_path(self):

		self.sc_data_path =   self.full_id[0] + '/' + self.full_id[1] + '/' + self.full_id + '/'


	# add/edit the id_number

	def __id_number(self, id_number=''):

		success = False
		if id_number == '':
			print("enter id number:")
			id_number = input(">>> ")


		try: 
			id_number = int(id_number)

			found = False
			for i in range(len(self.all_entries)):
				if id_number == self.all_entries[i]['id_number']:
					found = True
					break
			if found:

				print(f"Error, the id_number: {id_number} already exists!")

			else:
				# add id number

				self.entry['id_number'] = id_number
				success = True

		except ValueError:
			print("not a number, failed")

		return success

	def __title(self, title = ''):

		if title == '':
			print("Enter the title for this entry:")
			self.entry['title'] = input(">>> ")

		else:

			self.entry['title'] = title

	# adds entry if it does not match an existing one
	def add_entry(self, id_number = '', title = ''):

		print("Adding new entry")
		self.clear_entry()
		success = self.__id_number(id_number)

		if success:

			self.__title(title)

			self.all_entries.append(self.entry)
			self.number_of_entries = self.number_of_entries + 1
			self.__write_entries()

			self.entry_loaded = True

		else:
			print("failed to add new entry")


		return success

	def edit_entry(self, id_number = '', title = ''):

		if not self.entry_loaded:
			self.load_entry(id_number)

		original_id = self.entry['id_number']
		change = False

		print("Information on this entry:")
		self.print_entry()

		print("Do you want to change the id_number? (y for yes)")


		response = input(">>> ")

		if response == "y":

			success = self.__id_number()

			if success:
				change = True

		print("Do you want to change the title? (y for yes)")

		response = input(">>> ")

		if response == "y":

			self.__title()

			change = True

		if change:

			for i in range(len(self.all_entries)):

				if self.all_entries[i]['id_number'] == original_id:
					self.all_entries[i].update(self.entry)
					break


			self.__write_entries()

	# print current entry
	def print_entry(self):

		print(f"id number: {self.entry['id_number']}")
		print(f"title: {self.entry['title']}")

	def __write_entries(self):

		self.all_entries = sorted(self.all_entries, key=lambda k: k['id_number']) # sort the entries by number

		file = open(self.entries_file, 'w')
		yaml.dump(self.all_entries, file, default_flow_style=False, allow_unicode=True)
		file.close()

