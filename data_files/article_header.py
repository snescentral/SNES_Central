import yaml
import sys
import os
import itertools
import shutil
import filecmp
from datetime import datetime
from datetime import date
from entries import entries
from file_handling import file_handling


sys.path.append('../contributors')
from contributors import contributor

from other_images import other_images
from screenshots import screenshots

class article_header:
	def __init__(self, entry_id = ''):

		self.all_article_header_data = []

		self.article_header = {
		  'article_header_id': '', # Unique number for each article header. This is an integer.
		  'screenshot_id' : '', # id number for the screenshot used in the article header, uses this or other_image
		  'other_image_id' : '', # id number for the other image used in the article header
		  'description' : '', # short description used for the article header content
		  'last_update' : '', # last update time
		  'contributors' : [], # Contributors for the article
		  'thanks' : '',
		  'article_checksum' : ''
		}




		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)


#		# create a shared values object

#		self.shared_object = shared_values()

		# create contributors object

		self.contributors_object = contributor()

		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])

		# create screenshot object

		self.screenshot_object = screenshots(self.entry_object.entry['id_number'])


		# create other image object

		self.other_image_object = other_images(self.entry_object.entry['id_number'])




		self.data_filename = self.entry_object.data_path + 'article_headers.yaml'

		# load yaml file containing the dictionary of the article entries

		self.largest_article_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.all_article_header_data.insert(i, RIP.copy())
					if self.all_article_header_data[i]['article_header_id'] > self.largest_article_id:
						self.largest_pcb_scan_id = self.all_article_header_data[i]['article_header_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any article headers entered yet\n")

		self.next_article_header_id = i


		self.html_file = "article_header.html"
		self.temp_file = "temp/" + self.html_file

# not needed anymore, since this is loaded in the preamble
#	def load_article_header(self, entry_id=''):

#		found_entry = False
#		entry_object = entries()

#		while not found_entry:

#			found_entry, entry_id = entry_object.load_entry(entry_id)

#			if not found_entry:
#				entry_id = ''
#		self.entry_id = entry_id

#		path = entry_object.path()
#		found = False
#		# load yaml file
#		self.current_header_file = path + self.article_headers_filename

#		i=0
#		if os.path.exists(self.current_header_file):
#			file = open(self.current_header_file, 'r')
#			dataload = yaml.load_all(file, Loader=yaml.FullLoader)
#			for load_entries in dataload:
#				for individual_entries in load_entries:
#					self.all_article_header_data.insert(i,individual_entries)
#					i+=1
#			file.close()
#
#			self.all_article_header_data = sorted(self.all_article_header_data, key=lambda k: k['article_header_id']) # sort the entries by number
#
#			self.total_article_headers = len(self.all_article_header_data)
#
#			self.next_article_header_id = self.all_article_header_data[self.total_article_headers-1]['article_header_id'] + 1
#
#
#			found = True
#
#		else:
#			print(f"id #{entry_id} ({entry_object.entry['title']}) does not have any headers yet.\n")


	def clear_article_header(self):

		for key, item in self.article_header.items():
			if key == 'contributors' :
				self.article_header[key] = []
			else:
				self.article_header[key] = ''
		self.current_article_header = ''

	def load_article_header_info(self, article_header_id=''):


		self.clear_article_header()


		done = False
		found = False
		if not done: 

			if article_header_id == '':

				print(f"Select a number corresponding to the article header you want:")

				article_header_id  = input(">>: ")


			try:
				article_header_id = int(article_header_id)

				if self.check_id(article_header_id):


					for i in range(len(self.all_article_header_data)):

						if article_header_id == self.all_article_header_data[i]['article_header_id']:
							self.article_header.update(self.all_article_header_data[i])
							found = True
							break
					done = True

				else:
					print("error: number out of range")

			except ValueError: 
				print("error: you did not enter an integer")



		self.current_article_header = article_header_id

		return found

	def check_id(self, entry_id):
		found = False
		for article_header in self.all_article_header_data:
			if entry_id == article_header['article_header_id']:
				found=True

		return found

	########################
	# adding article header
	########################

	def add_article_header(self, edit=''):



		if edit:

			self.load_article_header()

		else:

			self.article_header['article_header_id'] = self.next_article_header_id


		# check the screenshot/other_image

		add_image = False

		if self.article_header['screenshot_id']:
			print("This article header has a screenshot associated with it, do you want to change it? (y for yes):\n")
			entry = input(">>> ")
			if entry == "y":
				add_image = True
		elif self.article_header['other_image_id']:
			print("This article header has an 'other_image' associated with it, do you want to change it? (y for yes):\n")
			entry = input(">>> ")
			if entry == "y":
				add_image = True

		else:

			add_image = True

		if add_image:
			not_done = True
			while not_done:
				print("What kind of image do you want to add to the article header?")
				print("1) screenshot")
				print("2) other_image")

				entry = input(">>> ")

				if entry == "1":

					found = self.screenshot_object.load_screenshot_info()

					if found:
						self.article_header['other_image_id'] = self.screenshot_object.screenshot["screenshot_id"]
						not_done = False

				elif entry == "2":

					found = self.other_image_object.load_other_image()

					if found:
						self.article_header['other_image_id'] = self.other_image_object.other_image["other_image_id"]
						not_done = False

				else:
					print("invalid entry\n")


		# add descriptions

		add_description = False
		if self.article_header['description']:
			self.print_description()

			print("Do you want to change the description? (y for yes):\n")
			entry = input(">>> ")
			if entry == "y":
				add_description = True

		else:

			add_description = True

		if add_description:
			self.add_description()



		# add contributors

		if self.article_header['contributors']:

			self.contributors_object.print_contributor_list(self.article_header['contributors'])

			print("options (enter for no changes):\n")
			print("1) add contributor\n")
			print("2) delete contributor\n")
			entry = input(">>> ")
			if entry == "1":

				id_value, changed = self.contributors_object.add_contributor_list(self.article_header['contributors'])

			elif entry == "2":
				id_value, changed = self.contributors_object.delete_contributor_list(self.article_header['contributors'])


			if changed:
				self.article_header['contributors'] = id_value


		else:
			self.article_header['contributors'], changed = self.contributors_object.add_contributor_list()

		# add thanks

		add_thanks = False
		if self.article_header['thanks']:

			self.print_thanks()
			
			print("Update thanks text? (y for yes):\n")
			entry = input(">>> ")
			if entry == "y":
				add_thanks = True
		else:
				add_thanks = True

		if add_thanks:
			self.add_thanks()


		# check if the article text exists or is modfied


		changed = self.check_article_file()


		if edit:
			counter = 0
			for article in self.all_article_header_data:
				if article['other_image_id'] == self.article_header['other_image_id']:
					self.all_article_header_data[counter] = self.article_header
					break
				else:
					counter += 1
		else:



			self.all_article_header_data.append(self.article_header)

			self.next_article_header_id += 1

		print(self.article_header)

		self.write_article_header_info()

	def write_article_header_info(self):

		if len(self.all_article_header_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.all_article_header_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No article headers information has been added")

	##############################################
	##### Methods associated with the article text file #####
	##############################################

	def check_article_file(self):

		# this does not work if no article header is loaded
		changed = False
		if self.article_header['article_header_id'] != "":


			fileexists, filename = self.file_object.check_file('article_headers',self.article_header['article_header_id'])
			if not fileexists:
				self.file_object.add_file('article_headers',self.article_header['article_header_id'])
				filename = self.file_object.get_filename('article_headers',self.article_header['article_header_id'])

			print(filename)
			sha256 = self.file_object.sha256_calc(filename)

			if self.article_header['article_checksum'] != sha256:
				# update 

				changed = True
				self.article_header['article_checksum'] = sha256
				self.update_time()

		else:
			print("Warning, no article header is loaded, so the article file check has not been done.\n")

		return changed

	##############################################
	##### Methods associated with the update time #####
	##############################################

	def update_time(self):

		dateval = date.today()
		self.article_header['last_update'] = dateval

	def date_format(self):

		dateval = datetime.__format__(self.article_header['last_update'],"%B %-d, %Y")

		return dateval

	def print_time(self):

		dateval = self.date_format()

		print(dateval)


	#############################
	##### Methods for description #####
	#############################

	def add_description(self):

		print("\nEnter the description:")
		description_value = input(">>> ")
		self.article_header['description'] = description_value 

	def print_description(self):
		print(f"\ndescription: {self.article_header['description']}")


	#############################
	##### Methods for thanks #####
	#############################

	def add_thanks(self):

		print("\nEnter the thanks text:")
		thanks_value = input(">>> ")
		self.article_header['thanks'] = thanks_value 

	def print_thanks(self):
		print(f"\Thanks text: {self.article_header['thanks']}")



	######################################
	##### Methods for HTML file #####
	######################################

	def create_html_files(self):

		if self.entry_id <0:
			self.load_article_header(entry_id)

		self.load_article_header_info(article_header_id)

		contributor_object = contributor()

		screenshot_object = screenshots()

		screenshot_object.load_screenshots(self.entry_id)

		myIterator = itertools.cycle([1,2])
		
		html_file = open(self.temp_file, 'w')



		html_all =            "		<div class=\"chip_table\">\n"

		html_all = html_all + "			<div class=\"chip_bulk\">\n"



		found_screenshot = screenshot_object.load_screenshot_info(self.article_header['screenshot_id'])
		if found_screenshot:
			sc_file = screenshot_object.return_sc_path(screenshot_object.screenshot['screenshot_id'])
			html_all = html_all + "				<div class=\"screen_image\"><img src = \"" + sc_file + "\"></div>\n"
		else:
			html_all = html_all + "				<div class=\"screen_image\"><img src = \"icon/sneslogo_256x209.png\"></div>\n"


		html_all = html_all + "				<div class=\"chip_info\">\n"

		html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\">" + str(self.article_header['description']) + "</div></div>\n"




		html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>By:</b>: " 
		comma = ""
		for contributor_id in self.article_header['contributors']:
			found = contributor_object.load_contributor_id(contributor_id)

			html_all = html_all + comma + contributor_object.return_link()
			comma = ", "

		html_all = html_all + "</div></div>\n"

		html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\">Last updated: " + self.date_format() + "</div></div>\n"

		if self.article_header['thanks']:
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\">Thanks to:\n"

			for thanks in  self.article_header['thanks']:

				html_all = html_all + "<br><br>" + thanks + "\n"

			html_all = html_all + "</div></div>\n"

		html_all = html_all + "				</div>\n"
		html_all = html_all + "			</div>\n"
		html_all = html_all + "		</div>\n"

		html_file.write(html_all)
		html_file.close()

# let's run this

if __name__ == "__main__":



	article_object = article_header()

	print("Options:")
	print("1) add article entry")
	print("2) edit article entry")
	print("3) Create HTML files for articles")
	print("4) Print artciles in database")


	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			article_object.add_article_header()
		elif(entry==2):
			article_object.add_article_header(edit=True)
		elif(entry==3):
			article_object.create_html_files()
		elif(entry==4):
			article_object.print_article_info()
		else:
			print("fail")

	except ValueError:
		print("fail")

