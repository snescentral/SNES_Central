import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from titles import titles
from file_handling import file_handling
from shared_values import shared_values

sys.path.append('../contributors')
from contributors import contributor


class other_images:
	def __init__(self, entry_id = ''):

		self.other_image_data = []

		self.other_image = {
		  'other_image_id': '', # Unique number for each other_image. This is an integer.
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this other_image label
		  'has_image' : '', # it is possible to not have an image
		  'show_in_list' : '' # whether or not to show the image in the list, preferable for images used only in articles
		}


		# load the entry value

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "other_images.yaml"

		# create a shared values object

		self.shared_object = shared_values()



		# create contributors object

		self.contributors_object = contributor()


		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])



		# load yaml file containing the dictionary of the other_image scan entries

		self.largest_other_image_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.other_image_data.insert(i, RIP.copy())
					if self.other_image_data[i]['other_image_id'] > self.largest_other_image_id:
						self.largest_other_image_id = self.other_image_data[i]['other_image_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any other_image scans entered yet\n")



		self.number_of_other_images = i

		# site files
		self.table_file = "other_image_table.html"
		self.table_temp_file = "temp/" + self.table_file 

		self.check_file = "other_image_check.txt"
		self.check_temp_file = "temp/" + self.check_file


	def print_other_image_info(self):
		print(f"Other images for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_other_images > 0:

			# first sort the data

			self.other_image_data = sorted(self.other_image_data, key=lambda k: k['other_image_id']) # sort the entries by id

			for other_image in self.other_image_data:

				print(f"{other_image['other_image_id']}) {other_image['notes']} ")
		else:
			print("There are currently no other images entered")

	def clear_other_image(self):
		temp_other_image = {
		  'other_image_id': '', # Unique number for each other_image. This is an integer.
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this other_image label
		  'has_image' : '', # it is possible to not have an image
		  'show_in_list' : '' # whether or not to show the image in the list, preferable for images used only in articles
		}

		self.other_image.update(temp_other_image)

	def load_other_image(self,other_image_id):

		self.clear_other_image()

		if not other_image_id:
			self.print_other_image_info()
			print("enter the other_image id you want")
			other_image_id = input(">>> ")

		found = False
		try: 

			other_image_id = int(other_image_id)
			for other_image in self.other_image_data:
				if other_image['other_image_id'] == other_image_id:
					self.other_image.update(other_image)
					found = True
					break

			if not found:
				print(f"other_image id {other_image_id} does not exit")

		except ValueError:
			print("you must enter an integer")

		return found

	def add_other_image(self):

		self.clear_other_image()


		self.largest_other_image_id += 1

		self.other_image['other_image_id'] = self.largest_other_image_id


		# add contributors

		self.other_image['contributors'], changed = self.contributors_object.add_contributor_list()

		self.other_image['notes'] = self.shared_object.add_notes()

		print("Do you want to add an image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('other_images',self.other_image['other_image_id'])
			self.other_image['has_image'] = True
		else:
			self.other_image['has_image'] = False

		print("Do you want to show the image in the main page list? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.other_image['show_in_list'] = True
		else:
			self.other_image['show_in_list'] = False


		self.other_image_data.append(self.other_image)

		self.write_info()

	def edit_other_image(self, other_image_id = ''):

		self.print_other_image_info()

		update = False

		self.clear_other_image()

		if other_image_id == '':

			print("enter the other_image id you want")
			other_image_id = input(">>> ")

		try:

			other_image_id = int(other_image_id)
			found = self.load_other_image(other_image_id)

		except ValueError:
			print("error, you must input a number")

		if not found:
			print("could not find the entry, exiting")
			sys.exit()



		print("Do you want to add/change the image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('other_images',self.other_image['other_image_id'])
			self.other_image['has_image'] = True
			update = True


		



		if self.other_image['show_in_list']:
			print("Currently, the image is set to show up in the main page list. Do you want to change that? (y for yes)")
			entry = input(">>> ")
			if entry == "y":
				self.other_image['show_in_list'] = False
				update = True
		else:
			print("Currently, the image is set to not show up in the main page list. Do you want to change that? (y for yes)")
			entry = input(">>> ")
			if entry == "y":
				self.other_image['show_in_list'] = True
				update = True


		# add contributors

		id_value, changed = self.contributors_object.add_contributor_list(self.other_image['contributors'])
		if changed:
			self.other_image['contributors'] = id_value
			update = True

		notes, changed = self.shared_object.edit_notes(self.other_image['notes'])
		if changed:
			self.other_image['notes'] = notes
			update = True

		if update:
			counter = 0
			for other_image in self.other_image_data:
				if other_image['other_image_id'] == self.other_image['other_image_id']:
					self.other_image_data[counter] = self.other_image
					break
				else:
					counter += 1

			self.write_info()




	def write_info(self):

		if len(self.other_image_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.other_image_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No other_image information has been added")

	def create_html_files(self):



		myIterator = itertools.cycle([1,2])

		# This goes through the list and creates a summary table with links to individual pages. 
		# It also creates a text file that the site uses to check for valid entries and prevent hacking problems.

		# Need check if there are entries to add

		table_entries = False




		check_file = open(self.check_temp_file, 'w')

		# write the table file
		html_file = open(self.table_temp_file, 'w')


		# first create the base parts of table file

#		html_table = '<p class="name">Other images and scans</p>\n'

		html_table = ""

		html_file.write("		<div class=\"chip_table\">\n")
		html_file.write("			<div class=\"chip_heading\">\n")
		html_file.write("				<div class=\"chip_heading_text\">Other images and scans information</div>\n")
		html_file.write("			</div>\n")
		html_file.write("			<div class=\"chip_bulk\">\n")
		html_file.write("			</div>\n")



		html_file.write("		<table class=\"infotable\">\n")
		html_file.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
		html_file.write("				<td width=\"630px\"><b>Notes</b></td>\n")
		html_file.write("				<td width=\"70px\" ><b>Scans</b></td>\n")
		html_file.write("			</tr>\n")



		row_number = 1

		# loop through all the other_images
		for other_image in self.other_image_data:




			if other_image['has_image']:
				check_file.write(str(other_image['other_image_id']) +  "\t" + self.entry_object.entry['title'] + "\n")

				# move the scan to the site
				self.file_object.move_to_site('other_images',other_image['other_image_id'])


			# start row of the table

			if other_image['show_in_list']:
				table_entries = True

				html_file.write("	<tr class = \"row" + str(next(myIterator)) + "\">\n")



				# notes
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + other_image['notes'] + "</td>\n")

				if other_image['has_image']:

					html_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"other_image_test.php?id=" + str(self.entry_object.full_id) + "&num=" + str(other_image['other_image_id']) + "\">link</a></td>\n")

				else:
					html_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
		


				# finish the table row

				html_file.write("	</tr>\n")
				if row_number == 1:
					row_number = 2
				else:
					row_number = 1

				if other_image['has_image']:




					# create html page for the scan

					myIterator2 = itertools.cycle([2,1])

					temp_file="temp/page.html"
					page_html_file = open(temp_file, 'w')






					page_html_file.write("		<div class=\"chip_table\">\n")
					page_html_file.write("			<div class=\"chip_heading\">\n")
					page_html_file.write("				<div class=\"chip_heading_text\">" + self.entry_object.entry['title'] + " - Other Image Information</div>\n")
					page_html_file.write("			</div>\n")
					page_html_file.write("			<div class=\"chip_bulk\">\n")
					sc_path = self.file_object.get_sc_path('other_images')

					page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('other_images',other_image['other_image_id']) + "\" alt=\"" + self.entry_object.entry['title'] + "\" /></center>\n")
					page_html_file.write("			</div>\n")
					page_html_file.write("		</div>\n")


					page_html_file.write("		<div class=\"chip_table\">\n")
					page_html_file.write("			<div class=\"chip_heading\">\n")
					page_html_file.write("				<div class=\"chip_heading_text\">other image documentation</div>\n")
					page_html_file.write("			</div>\n")
					page_html_file.write("			<div class=\"chip_bulk\">\n")


					page_html_file.write("				<table class=\"infotable2\">\n")
					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td width=\"150px\"><b>Item</b></td>\n")
					page_html_file.write("						<td width=\"550px\" ><b>Description</b></td>\n")
					page_html_file.write("					</tr>\n")




					# contributors

					for contributor_id in  other_image['contributors']:
						self.contributors_object.load_contributor_id(contributor_id)

						page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
						page_html_file.write("						<td>Contributor</td>\n")
						page_html_file.write("						<td>" +  self.contributors_object.return_link() +  "</td>\n")
						page_html_file.write("					</tr>\n")

					#notes


					if other_image['notes']:
						page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
						page_html_file.write("						<td>Notes</td>\n")
						page_html_file.write("						<td>" + other_image['notes'] +  "</td>\n")
						page_html_file.write("					</tr>\n")

					page_html_file.write("				</table>\n")

					page_html_file.write("			</div>\n")
					page_html_file.write("		</div>\n")

					
					page_html_file.close()

					# move to site


					sc_file = str(other_image['other_image_id']) + ".html"
					self.file_object.move_html_to_site(temp_file, sc_file, "other_images")

		# finish off making the table

		html_file.write("</table>\n")

		html_file.write("		</div>\n")


		html_file.close()
		self.file_object.move_html_to_site(self.table_temp_file, self.table_file)

		check_file.close()


		self.file_object.move_html_to_site(self.check_temp_file, self.check_file)
# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	other_image_object = other_images()

	print("Options:")
	print("1) add other_image entry")
	print("2) edit other_image entry")
	print("3) Create HTML files for other_image information")
	print("4) Print other images in database")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			other_image_object.add_other_image()
		elif(entry==2):
			other_image_object.edit_other_image()
		elif(entry==3):
			other_image_object.create_html_files()
		elif(entry==4):
			other_image_object.print_other_image_info()
		else:
			print("fail")

	except ValueError:
		print("fail")
