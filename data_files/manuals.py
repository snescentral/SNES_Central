import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from titles import titles
from file_handling import file_handling
from shared_values import shared_values

sys.path.append('../contributors')
from contributors import contributor


class manuals:
	def __init__(self, entry_id = ''):

		self.manual_data = []

		self.manual = {
		  'manual_id': '', # Unique number for each manual. This is an integer.
		  'region_id' : '', # id for the region of the manual label
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this manual
		  'title_id_values' : [], # pointers to the titles associated with this manual
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this manual label
		  'has_image' : '' # it is possible to not have an image
		}


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "manuals.yaml"

		# create a shared values object

		self.shared_object = shared_values()

		# create a titles object

		self.title_object = titles(self.entry_object.entry['id_number'])

		# create contributors object

		self.contributors_object = contributor()


		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])



		# load yaml file containing the dictionary of the manual scan entries

		self.largest_manual_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.manual_data.insert(i, RIP.copy())
					if self.manual_data[i]['manual_id'] > self.largest_manual_id:
						self.largest_manual_id = self.manual_data[i]['manual_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any manual scans entered yet\n")



		self.number_of_manuals = i

		# site files
		self.table_file = "manual_table.html"
		self.table_temp_file = "temp/" + self.table_file 

		self.check_file = "manual_check.txt"
		self.check_temp_file = "temp/" + self.check_file


	def print_manual_info(self):
		print(f"manuals for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_manuals > 0:

			# first sort the data

			self.manual_data = sorted(self.manual_data, key=lambda k: k['manual_id']) # sort the entries by id

			for manual in self.manual_data:

				self.title_object.load_title(manual['title_id_values'][0])
				title = self.title_object.title['title']

				self.shared_object.load_country(manual['country_id'])
				country = self.shared_object.country_selection['country']

				print(f"{manual['manual_id']}) {title} ({country}) ({manual['serial_code']})")
		else:
			print("There are currently no manuals entered")

	def clear_manual(self):
		temp_manual = {
		  'manual_id': '', # Unique number for each manual. This is an integer.
		  'region_id' : '', # id for the region of the manual label
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this manual
		  'title_id_values' : [], # pointers to the titles associated with this manual
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this manual label
		  'has_image' : '' # it is possible to not have an image
		}

		self.manual.update(temp_manual)

	def load_manual(self,manual_id):

		found = False
		try: 

			manual_id = int(manual_id)
			for manual in self.manual_data:
				if manual['manual_id'] == manual_id:
					self.manual.update(manual)
					found = True
					break

			if not found:
				print(f"Manual id {manual_id} does not exit")

		except ValueError:
			print("you must enter an integer")

		return found

	def add_manual(self):

		self.clear_manual()


		self.largest_manual_id += 1

		self.manual['manual_id'] = self.largest_manual_id



		self.manual['region_id'] = self.shared_object.return_region()

		# add title

		self.manual['title_id_values'], changed = self.title_object.get_id_list()

		if not changed:
			print("It is necessary to add a title")
			sys.exit()


		self.manual['country_id'] = self.shared_object.return_country()	
		self.manual['serial_code'] = self.shared_object.add_serial_code()		

	
		self.manual['manufacturing_id'] = self.shared_object.return_manufacturing_location()
		self.manual['company_id_values'] = self.shared_object.return_company()

		# add contributors

		self.manual['contributors'], changed = self.contributors_object.add_contributor_list()

		self.manual['notes'] = self.shared_object.add_notes()

		print("Do you want to add an image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('manuals',self.manual['manual_id'])
			self.manual['has_image'] = True
		else:
			self.manual['has_image'] = False


		self.manual_data.append(self.manual)

		self.write_info()

	def edit_manual(self, manual_id = ''):

		self.print_manual_info()

		update = False

		self.clear_manual()

		if manual_id == '':

			print("enter the manual id you want")
			manual_id = input(">>> ")

		try:

			manual_id = int(manual_id)
			found = self.load_manual(manual_id)

		except ValueError:
			print("error, you must input a number")

		if not found:
			print("could not find the entry, exiting")
			sys.exit()



		print("Do you want to add/change the image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('manuals',self.manual['manual_id'])
			self.manual['has_image'] = True
			update = True

		
		changed, id_value = self.shared_object.edit_region(self.manual['region_id'])
		if changed:
			self.manual['region_id'] = id_value
			update = True

		# add title

		id_value, changed = self.title_object.get_id_list(self.manual['title_id_values'])
		if changed:
			self.manual['title_id_values'] = id_value
			update = True

		changed, id_value = self.shared_object.edit_country(self.manual['country_id'])
		if changed:
			self.manual['country_id'] = id_value
			update = True


		changed, code = self.shared_object.edit_serial_code(self.manual['serial_code'])
		if changed:
			self.manual['serial_code'] = code
			update = True

	
		changed, id_value = self.shared_object.edit_manufacturing_location(self.manual['manufacturing_id'])
		if changed:
			self.manual['manufacturing_id'] = id_value
			update = True


		changed, id_value = self.shared_object.edit_company(self.manual['company_id_values'])
		if changed:
			self.manual['company_id_values'] = id_value
			update = True


		# add contributors

		id_value, changed = self.contributors_object.add_contributor_list(self.manual['contributors'])
		if changed:
			self.manual['contributors'] = id_value
			update = True

		notes, changed = self.shared_object.edit_notes(self.manual['notes'])
		if changed:
			self.manual['notes'] = notes
			update = True

		if update:
			counter = 0
			for manual in self.manual_data:
				if manual['manual_id'] == self.manual['manual_id']:
					self.manual_data[counter] = self.manual
					break
				else:
					counter += 1

			self.write_info()




	def write_info(self):

		if len(self.manual_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.manual_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No manual information has been added")

	def create_html_files(self):

		myIterator = itertools.cycle([1,2])

		# This goes through the list and creates a summary table with links to individual pages. 
		# It also creates a text file that the site uses to check for valid entries and prevent hacking problems.

		check_file = open(self.check_temp_file, 'w')

		# write the table file
		html_file = open(self.table_temp_file, 'w')


		# first create the base parts of table file

#		html_table = '<p class="name">Manuals information</p>\n'

		html_table = ""

		html_file.write("		<div class=\"chip_table\">\n")
		html_file.write("			<div class=\"chip_heading\">\n")
		html_file.write("				<div class=\"chip_heading_text\">Manuals information</div>\n")
		html_file.write("			</div>\n")
		html_file.write("			<div class=\"chip_bulk\">\n")
		html_file.write("			</div>\n")



		html_file.write("		<table class=\"infotable\">\n")
		html_file.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
		html_file.write("				<td width=\"90px\"><b>Region</b></td>\n")
		html_file.write("				<td width=\"110px\" ><b>Country</b></td>\n")
		html_file.write("				<td width=\"140px\" ><b>Serial Code</b></td>\n")
		html_file.write("				<td width=\"290px\" ><b>Notes</b></td>\n")
		html_file.write("				<td width=\"70px\" ><b>Scans</b></td>\n")
		html_file.write("			</tr>\n")

		# first sort the data
		self.manual_data = sorted(self.manual_data, key=lambda k: k['serial_code']) # sort the entries by serial code
		self.manual_data = sorted(self.manual_data, key=lambda k: k['country_id']) # sort the entries by country
		self.manual_data = sorted(self.manual_data, key=lambda k: k['region_id']) # then sort the entries by region

		row_number = 1

		# loop through all the manuals
		for manual in self.manual_data:


			self.title_object.load_title(manual['title_id_values'][0])

			if manual['has_image']:
				check_file.write(str(manual['manual_id']) +  "\t" + self.title_object.title['title'] + " (" + manual['serial_code'] + ")\n")

			# start row of the table

			html_file.write("	<tr class = \"row" + str(next(myIterator)) + "\">\n")

			# region value
			self.shared_object.load_region(manual['region_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.region_selection['region'] + "</td>\n")

			# country value
			self.shared_object.load_country(manual['country_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.country_selection['country'] + "</td>\n")

			# serial code
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + manual['serial_code'] + "</td>\n")

			# notes
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + manual['notes'] + "</td>\n")

			if manual['has_image']:

				html_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"manual_test.php?id=" + str(self.entry_object.full_id) + "&num=" + str(manual['manual_id']) + "\">link</a></td>\n")

			else:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
	


			# finish the table row

			html_file.write("	</tr>\n")
			if row_number == 1:
				row_number = 2
			else:
				row_number = 1

			if manual['has_image']:

				# move the scan to the site

				self.file_object.move_to_site('manuals',manual['manual_id'])

				# create html page for the scan

				myIterator2 = itertools.cycle([2,1])

				temp_file="temp/page.html"
				page_html_file = open(temp_file, 'w')

				self.title_object.load_title(manual['title_id_values'][0])




				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">" + self.title_object.title['title'] + " (" + manual['serial_code'] + ") - Manual Information</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")
				sc_path = self.file_object.get_sc_path('manuals')

				page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('manuals',manual['manual_id']) + "\" alt=\"" + self.title_object.title['title'] + " (" + manual['serial_code'] + ")\" /></center>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")


				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">Manual documentation</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")


				page_html_file.write("				<table class=\"infotable2\">\n")
				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td width=\"150px\"><b>Item</b></td>\n")
				page_html_file.write("						<td width=\"550px\" ><b>Description</b></td>\n")
				page_html_file.write("					</tr>\n")

				# titles

				for title_id in manual['title_id_values']:
					self.title_object.load_title(title_id)

					language_id, language =  self.title_object.ask_for_language( self.title_object.title['language_id'])

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Title (" + language + ")</td>\n")
					if self.title_object.title['translation']:
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "(" +  self.title_object.title['translation'] + ")</td>\n")
					else: 
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "</td>\n")
					page_html_file.write("					</tr>\n")

				# region value
				self.shared_object.load_region(manual['region_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Region</td>\n")
				page_html_file.write("						<td>" + self.shared_object.region_selection['region'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# country value

				self.shared_object.load_country(manual['country_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Country</td>\n")
				page_html_file.write("						<td>" + self.shared_object.country_selection['country'] + " (" + self.shared_object.country_selection['region_code'] + ")</td>\n")
				page_html_file.write("					</tr>\n")

				# location manufactured
				
				self.shared_object.load_manufacturing_location(manual['manufacturing_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Manufacturing location</td>\n")
				page_html_file.write("						<td>" + self.shared_object.manufacturing_location_selection['manufacturing_location'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# serial code

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Serial code</td>\n")
				page_html_file.write("						<td>" + manual['serial_code'] +  "</td>\n")
				page_html_file.write("					</tr>\n")

				# companies
				
				for company_id in manual['company_id_values']:

					self.shared_object.load_company(company_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Company</td>\n")
					page_html_file.write("						<td>" +  self.shared_object.company_selection['company'] +  "</td>\n")
					page_html_file.write("					</tr>\n")


				# contributors

				for contributor_id in  manual['contributors']:
					self.contributors_object.load_contributor_id(contributor_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Contributor</td>\n")
					page_html_file.write("						<td>" +  self.contributors_object.return_link() +  "</td>\n")
					page_html_file.write("					</tr>\n")

				#notes


				if manual['notes']:
					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Notes</td>\n")
					page_html_file.write("						<td>" + manual['notes'] +  "</td>\n")
					page_html_file.write("					</tr>\n")

				page_html_file.write("				</table>\n")

				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")

				
				page_html_file.close()

				# move to site


				sc_file = str(manual['manual_id']) + ".html"
				self.file_object.move_html_to_site(temp_file, sc_file, "manuals")

		# finish off making the table

		html_file.write("</table>\n")

		html_file.write("		</div>\n")

		html_file.write("<p><i>For SNES Central documentation, I am only including the front page of each manual. If you want to find full scans of the manuals, you should visit Peebs' <a href=\"http://SNESManuals.com\">SNESManuals.com</a> project. He is collecting scans of every manual. Also check out <a href=\"http://evilbadman.com/\">Kirkland's archive</a>. They have scanned every US SNES manual in high resolution (available also on <a href=\"https://archive.org/details/kirklands_manual_labor_-_super_nintendo_-_usa_-_2k_version/\">Archive.org</a>). Anything that I post here should be available in one of those two places.</i></p>\n")
		html_file.close()
		self.file_object.move_html_to_site(self.table_temp_file, self.table_file)

		check_file.close()


		self.file_object.move_html_to_site(self.check_temp_file, self.check_file)
# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	manual_object = manuals()

	print("Options:")
	print("1) add manual entry")
	print("2) edit manual entry")
	print("3) Create HTML files for manual information")
	print("4) Print manuals in database")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			manual_object.add_manual()
		elif(entry==2):
			manual_object.edit_manual()
		elif(entry==3):
			manual_object.create_html_files()
		elif(entry==4):
			manual_object.print_manual_info()
		else:
			print("fail")

	except ValueError:
		print("fail")
