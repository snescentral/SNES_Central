import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from file_handling import file_handling
from shared_values import shared_values
from rom_image import rom_image
from screenshots import screenshots

sys.path.append('../contributors')
from contributors import contributor

sys.path.append('../pcb')
from pcb import pcb

sys.path.append('../chiptype')
from chips import chips


class pcb_scans:
	def __init__(self, entry_id = ''):

		self.pcb_data = []

		self.pcb_scan = {
		  'pcb_scan_id': '', # Unique number for each pcb scan. This is an integer.
		  'pcb_id' : '', # id for the pcb
		  'chip_data' : [], # information with all the chips
		  'rom_id' : '', # ROM image id
		  'serial_code' : '', # the serial code (because there are a few edge cases where it differs from the ROM image)
		  'contributors' : [], # who submitted the scan
		  'notes_front' : '',           # notes for the pcb front image
		  'notes_back' : '',           # notes for the pcb front image
		  'has_front_image' : '', # it is possible to not have an image
		  'has_back_image' : '', # it is possible to not have an image
		  'manufacturing_id' : '', # Location where the PCB is manufactured
		  'company_id_values' : []  # manufacturing company of the PCB, usually Nintendo
		}


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + 'pcb.yaml'

		# create a shared values object

		self.shared_object = shared_values()

		# create a rom image object

		self.rom_object = rom_image(self.entry_object.entry['id_number'])

		# create contributors object

		self.contributors_object = contributor()

		# create pcb object

		self.pcb_object = pcb()

		# create pcb object

		self.chips_object = chips()

		# create screenshot object

		self.screenshot_object = screenshots(self.entry_object.entry['id_number'])


		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])



		# load yaml file containing the dictionary of the pcb scan entries

		self.largest_pcb_scan_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.pcb_data.insert(i, RIP.copy())
					if self.pcb_data[i]['pcb_scan_id'] > self.largest_pcb_scan_id:
						self.largest_pcb_scan_id = self.pcb_data[i]['pcb_scan_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any pcb scans entered yet\n")

		self.number_of_pcbs = i

		# site files
		self.table_file = "pcb_table.html"
		self.table_temp_file = "temp/" + self.table_file 

		self.check_file = "pcb_check.txt"
		self.check_temp_file = "temp/" + self.check_file

		self.chip_slot = {'slot_number' : '', 'chip_info' : ''}


	def print_pcb_info(self):
		print(f"pcbs for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_pcbs > 0:

			# first sort the data

			self.pcb_data = sorted(self.pcb_data, key=lambda k: k['pcb_scan_id']) # sort the entries by id

			for pcb in self.pcb_data:

				self.rom_object.load_rom_image_info(pcb['rom_id'])
				if self.rom_object.rom_info['region']:
					found, region = self.rom_object.find_region_name(self.rom_object.rom_info['region'])
				else:
					found = False
				if not found:
					region = "unknown"

				date_values = []

				for chip in pcb['chip_data']:
					self.chips_object.clear_entry()
					self.chips_object.chip.update(chip)
					if(self.chips_object.chip['chiptype_id'] == 0):
						date_values.append(self.chips_object.return_four_date())

				number_dates = len(date_values)
				date_string = ""
				if number_dates > 0:
					counter = 1
					for date_value in date_values:
						date_string = date_string + date_value
						if counter < number_dates:
							date_string = date_string + ", "
						counter = counter + 1


				found = self.pcb_object.load_pcb_id(pcb['pcb_id'])

				print(f"{pcb['pcb_scan_id']}) {pcb['serial_code']} ({region}) ({self.pcb_object.pcb['serial_code']}) ({date_string})")
		else:
			print("There are currently no pcbs entered")

	def clear_pcb(self):
		temp_pcb = {
		  'pcb_scan_id': '', # Unique number for each screenshot. This is an integer.
		  'pcb_id' : '', # id for the pcb
		  'chip_data' : [], # information with all the chips
		  'rom_id' : '', # ROM image id
		  'serial_code' : '', # the serial code (because there are a few edge cases where it differs from the ROM image)
		  'contributors' : [], # who submitted the scan
		  'notes_front' : '',           # notes for the pcb front image
		  'notes_back' : '',           # notes for the pcb front image
		  'has_front_image' : '', # it is possible to not have an image
		  'has_back_image' : '', # it is possible to not have an image
		  'manufacturing_id' : '', # Location where the PCB is manufactured
		  'company_id_values' : []  # manufacturing company of the PCB, usually Nintendo
		}

		self.pcb_scan.update(temp_pcb)

	def load_pcb(self,pcb_scan_id):

		self.clear_pcb()
		found = False
		try: 

			pcb_scan_id = int(pcb_scan_id)
			for pcb in self.pcb_data:
				if pcb['pcb_scan_id'] == pcb_scan_id:
					self.pcb_scan.update(pcb)
					found = True
					break

			if not found:
				print(f"pcb scan id {pcb_scan_id} does not exit")

		except ValueError:
			print("you must enter an integer")

		return found

	def add_pcb(self):

		self.clear_pcb()


		self.largest_pcb_scan_id += 1

		self.pcb_scan['pcb_scan_id'] = self.largest_pcb_scan_id

		# add pcb type

		found = self.pcb_object.load_pcb()

		if not found:
			print("could not find PCB, exiting")
			sys.exit()

		self.pcb_scan['pcb_id'] = self.pcb_object.pcb['id_number']

		print(f"{self.pcb_object.pcb['pcbID']} has {self.pcb_object.pcb['chipSlots']} chip slots")

		# adding chip information
		self.adding_chips()

		self.pcb_scan['serial_code'] = self.shared_object.add_serial_code()		

		print("PCB manufacturer:")
		self.pcb_scan['manufacturing_id'] = self.shared_object.return_manufacturing_location()
		print("PCB company:")
		self.pcb_scan['company_id_values'] = self.shared_object.return_company()

		# add ROM image

		print("Do you want to enter a ROM image associated with this PCB (y for yes):\n")

		entry = input(">>> ")

		if entry == "y":

			found = self.rom_object.load_rom_image()
			self.pcb_scan['rom_id'] = self.rom_object.rom_info['rom_id']
		else:
			self.pcb_scan['rom_id'] = ""




		print("Do you want to add a front pcb image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('pcbs_front',self.pcb_scan['pcb_scan_id'])
			self.pcb_scan['notes_front'] = self.shared_object.add_notes()
			self.pcb_scan['has_front_image'] = True
		else:
			self.pcb_scan['has_front_image'] = False

		print("Do you want to add a back pcb image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('pcbs_back',self.pcb_scan['pcb_scan_id'])
			self.pcb_scan['notes_back'] = self.shared_object.add_notes()
			self.pcb_scan['has_back_image'] = True
		else:
			self.pcb_scan['has_back_image'] = False

		# add contributors

		self.pcb_scan['contributors'], changed = self.contributors_object.add_contributor_list()



		self.pcb_data.append(self.pcb_scan)

		self.write_info()

	def adding_chips(self):

		# assumes the pcb_id has already been loaded
		for chip in self.pcb_object.pcb['chipSlotType']:
			self.pcb_object.chip_object.clear_entry()

			found, chiptype = self.pcb_object.chip_object.return_chiptype(chip['chiptype_id'])
			print(f"U{chip['slot_number']}) {chiptype}")
			self.pcb_object.chip_object.select_chip(chip['chiptype_id'])
			self.pcb_object.chip_object.read_chip_info()

			chip_slot = self.chip_slot.copy()

			chip_slot['slot_number'] = chip['slot_number']
			chip_slot['chip_info'] = self.pcb_object.chip_object.chip.copy()
			self.pcb_scan['chip_data'].append(chip_slot)

	def edit_pcb(self, pcb_scan_id = ''):

		self.print_pcb_info()

		update = False

		self.clear_pcb()

		if pcb_scan_id == '':

			print("enter the pcb scan id you want to edit")
			pcb_scan_id = input(">>> ")

		try:

			pcb_scan_id = int(pcb_scan_id)
			found = self.load_pcb(pcb_scan_id)

		except ValueError:
			print("error, you must input a number")

		if not found:
			print("could not find the entry, exiting")
			sys.exit()



		edit_chip = False
		found = self.pcb_object.load_pcb_id(self.pcb_scan['pcb_id'])

		print(f"The current PCB type is ({self.pcb_object.pcb['serial_code']}), do you want to change it? (y for yes)")

		entry = input(">>> ")

		if entry == "y":

			found = self.pcb_object.load_pcb()

			if not found:
				print("could not find PCB, exiting")
				sys.exit()

			edit_chip = True
			update = True

		else:
			print("Do you want to change the chip information (note, this will require reentry of everything)? (y for yes)")

			entry2 = input(">>> ")

			if entry2 == "y":

				edit_chip = True

		if edit_chip:
			self.adding_chips()
			update = True



		changed, self.pcb_scan['serial_code'] = self.shared_object.edit_serial_code(self.pcb_scan['serial_code'])
		if changed:
			update = True

		print("PCB manufacturer:")
		changed, self.pcb_scan['manufacturing_id'] = self.shared_object.edit_manufacturing_location(self.pcb_scan['manufacturing_id'])
		if changed:
			update = True

		print("PCB company:")
		changed, self.pcb_scan['company_id_values'] = self.shared_object.edit_company(self.pcb_scan['company_id_values'])
		if changed:
			update = True


		# edit ROM image

		if self.pcb_scan['rom_id']:
			self.rom_object.load_rom_image_info(self.pcb_scan['rom_id'])
			print("ROM image info:")
			self.rom_object.print_current_rom_image_info()
			print("Do you change the ROM image associated with this PCB (y for yes):\n")
		else:
			print("Do you add a ROM image associated with this PCB (y for yes):\n")

		entry = input(">>> ")

		if entry == "y":
			self.rom_object.clear_entry()
			found = self.rom_object.load_rom_image()
			self.pcb_scan['rom_id'] = self.rom_object.rom_info['rom_id']
			update = True

		# images

		print("Do you want to add/change a front pcb image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('pcbs_front',self.pcb_scan['pcb_scan_id'])
			self.pcb_scan['has_front_image'] = True
			update = True

		self.pcb_scan['notes_front'], changed = self.shared_object.edit_notes(self.pcb_scan['notes_front'])

		if changed:
			update = True

		print("Do you want to add/change a back pcb image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('pcbs_back',self.pcb_scan['pcb_scan_id'])
			self.pcb_scan['has_back_image'] = True
			update = True


		self.pcb_scan['notes_back'], changed = self.shared_object.edit_notes(self.pcb_scan['notes_back'])

		if changed:
			update = True

		# add contributors

		id_value, changed = self.contributors_object.add_contributor_list(self.pcb_scan['contributors'])
		if changed:
			self.pcb_scan['contributors'] = id_value
			update = True
		

		if update:
			counter = 0
			for pcb in self.pcb_data:
				if pcb['pcb_scan_id'] == self.pcb_scan['pcb_scan_id']:
					self.pcb_data[counter] = self.pcb_scan
					break
				else:
					counter += 1

			self.write_info()




	def write_info(self):

		if len(self.pcb_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.pcb_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No PCB information has been added")

	def create_html_files(self):

		myIterator = itertools.cycle([1,2])

		# This goes through the list and creates a summary table with links to individual pages. 
		# It also creates a text file that the site uses to check for valid entries and prevent hacking problems.

		check_file = open(self.check_temp_file, 'w')

		# write the table file
		html_file = open(self.table_temp_file, 'w')


		# first create the base parts of table file

#		html_table = '<p class="name">PCB information</p>\n'

		html_table = ""

		html_file.write("		<div class=\"chip_table\">\n")
		html_file.write("			<div class=\"chip_heading\">\n")
		html_file.write("				<div class=\"chip_heading_text\">PCB Information</div>\n")
		html_file.write("			</div>\n")


#700

		html_file.write("			<table class=\"infotable\">\n")
		html_file.write("				<tr class=\"row" + str(next(myIterator)) + "\">\n")
		html_file.write("					<td width=\"160px\"><b>Region</b></td>\n")
		html_file.write("					<td width=\"160px\" ><b>ROM chip ID</b></td>\n")
		html_file.write("					<td width=\"160px\" ><b>ROM Chip Dates</b></td>\n")
		html_file.write("					<td width=\"160px\" ><b>PCB Board ID</b></td>\n")
		html_file.write("					<td width=\"60px\" ><b>Link</b></td>\n")
		html_file.write("				</tr>\n")



		# create the list for the table
		table_list = []
		date_dict_list = []
		for pcb in self.pcb_data:

			self.rom_object.load_rom_image_info(pcb['rom_id'])
			if self.rom_object.rom_info['region']:
				found, region = self.rom_object.find_region_name(self.rom_object.rom_info['region'])
			else:
				found = False
			if not found:
				region = "unknown"
			date_values = []
			for chip in pcb['chip_data']:
				self.chips_object.clear_entry()
				self.chips_object.chip.update(chip['chip_info'])
				if(self.chips_object.chip['chiptype_id'] == 0):
					date_values.append(self.chips_object.return_four_date())

			number_dates = len(date_values)
			date_string = ""
			if number_dates > 0:
				counter = 1
				for date_value in date_values:
					date_string = date_string + date_value
					if counter < number_dates:
						date_string = date_string + ", "
					counter = counter + 1

			self.pcb_object.load_pcb_id( pcb['pcb_id'])
			pcb_serial = self.pcb_object.pcb['serial_code']
			pcb_link=self.pcb_object.pcb_link(pcb['pcb_id'])

			date_dict_list.append({'region': region, 'rom_serial_code':  pcb['serial_code'], 'date_string': date_string, 'pcb_serial':  pcb_serial, 'pcb_scan_id': pcb['pcb_scan_id'], 'pcb_link' : pcb_link, 'rom_id': pcb['rom_id'] })


		# first sort the data
		date_dict_list = sorted(date_dict_list, key=lambda k: k['date_string']) # sort the entries by date
		date_dict_list = sorted(date_dict_list, key=lambda k: k['rom_serial_code']) # sort the entries by region
		date_dict_list = sorted(date_dict_list, key=lambda k: k['pcb_serial']) # sort the entries by region
		date_dict_list = sorted(date_dict_list, key=lambda k: k['region']) # sort the entries by region


		row_number = 1

		# loop through all the pcbs
		for pcb_item in date_dict_list:



			# start row of the table



			html_file.write("				<tr class = \"row" + str(next(myIterator)) + "\">\n")

			# region value

			html_file.write("					<td style=\"text-align: left; vertical-align: top;\">" + pcb_item['region'] + "</td>\n")

			# ROM chip ID value
			
			html_file.write("					<td style=\"text-align: left; vertical-align: top;\">" + pcb_item['rom_serial_code'] + "</td>\n")

			# ROM Chip Date value
			html_file.write("					<td style=\"text-align: left; vertical-align: top;\">" + pcb_item['date_string'] + "</td>\n")

			# PCB Board ID value
			html_file.write("					<td style=\"text-align: left; vertical-align: top;\">" + pcb_item['pcb_link'] + "</td>\n")

			# link
			html_file.write("					<td style=\"text-align: left; vertical-align: top;\"><a href=\"pcb_test.php?id=" + str(self.entry_object.full_id) + "&num=" + str(pcb_item['pcb_scan_id']) + "\">link</a></td>\n")


	


			# finish the table row

			html_file.write("				</tr>\n")
			if row_number == 1:
				row_number = 2
			else:
				row_number = 1

		# finish off making the table

		html_file.write("			</table>\n")

		html_file.write("		</div>\n")

		html_file.write("		<p><i>Do you have a label variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1097\">Submission guidelines</a> for pcb scans.</p>\n")
		html_file.close()


		# finished the table



		# create pcb pages
		# not done TODO
		for pcb in self.pcb_data:

			if pcb['rom_id']:
				self.rom_object.load_rom_image_info(pcb['rom_id'])

				self.rom_object.title_object.load_title(self.rom_object.rom_info['title_id_values'][0])
				title = self.rom_object.title_object.title['title']


			else:
				title =  self.rom_object.title_object.title['title']
				

			if pcb['serial_code']:
				serial_code_text = " (" + pcb['serial_code'] + ")"
				pcb_link=self.pcb_object.pcb_link(pcb['pcb_id'])
			else:
				serial_code_text = ""
				pcb_link=""

			check_file.write(str(pcb['pcb_scan_id']) +  "\t" + title + serial_code_text + "\n")



			if pcb['has_front_image'] or pcb['has_back_image']:

				# move the scan to the site

				if pcb['has_front_image']:
					self.file_object.move_to_site('pcbs_front',pcb['pcb_scan_id'])
				if pcb['has_back_image']:
					self.file_object.move_to_site('pcbs_back',pcb['pcb_scan_id'])

				# create html page for the scan

				myIterator2 = itertools.cycle([2,1])

				temp_file="temp/page.html"
				page_html_file = open(temp_file, 'w')
				

				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">" + title + " (" + pcb['serial_code'] + ") - PCB Information</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")

				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\"><center>Front Scan</center></div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")

				if pcb['has_front_image']:
					sc_path = self.file_object.get_sc_path('pcbs_front')

					page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('pcbs_front',pcb['pcb_scan_id']) + "\" alt=\"pcb Front - " + title + " (" + pcb['serial_code'] + ")\" /></center>\n")





				else:

					page_html_file.write("				<center>pcb front scan not available</center>\n")
	
				page_html_file.write("			</div>\n")

				if pcb['notes_front']:
					page_html_file.write("			<table class=\"infotable2\">\n")
					page_html_file.write("				<tr class=\"row1>\n")
					page_html_file.write("					<td>Notes</td>\n")
					page_html_file.write("					<td>" + pcb['notes_front'] +  "</td>\n")
					page_html_file.write("				</tr>\n")
					page_html_file.write("			</table>\n")

				page_html_file.write("		</div>\n")




				if pcb['has_back_image']:
					sc_path = self.file_object.get_sc_path('pcbs_back')


					page_html_file.write("		<div class=\"chip_table\">\n")
					page_html_file.write("			<div class=\"chip_heading\">\n")
					page_html_file.write("				<div class=\"chip_heading_text\"><center>Back Scan</center></div>\n")
					page_html_file.write("			</div>\n")
					page_html_file.write("			<div class=\"chip_bulk\">\n")

					page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('pcbs_back',pcb['pcb_scan_id']) + "\" alt=\"pcb back - " + title + " (" + pcb['serial_code'] + ")\" /></center>\n")




	
					page_html_file.write("			</div>\n")

					if pcb['notes_back']:
						page_html_file.write("			<table class=\"infotable2\">\n")
						page_html_file.write("				<tr class=\"row1>\n")
						page_html_file.write("					<td>Notes</td>\n")
						page_html_file.write("					<td>" + pcb['notes_back'] +  "</td>\n")
						page_html_file.write("				</tr>\n")
						page_html_file.write("			</table>\n")





				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">PCB Documentation</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")


				page_html_file.write("				<table class=\"infotable2\">\n")
				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td width=\"150px\"><b>Item</b></td>\n")
				page_html_file.write("						<td width=\"550px\" ><b>Description</b></td>\n")
				page_html_file.write("					</tr>\n")



				# PCB Serial

				if pcb_link:
					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>PCB Board</td>\n")
					page_html_file.write("						<td>" + pcb_link + "</td>\n")
					page_html_file.write("					</tr>\n")



				# location manufactured
				
				self.shared_object.load_manufacturing_location(pcb['manufacturing_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Manufacturing location</td>\n")
				page_html_file.write("						<td>" + self.shared_object.manufacturing_location_selection['manufacturing_location'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# serial code

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Serial code</td>\n")
				page_html_file.write("						<td>" + pcb['serial_code'] +  "</td>\n")
				page_html_file.write("					</tr>\n")

				# companies
				
				for company_id in pcb['company_id_values']:

					self.shared_object.load_company(company_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Company</td>\n")
					page_html_file.write("						<td>" +  self.shared_object.company_selection['company'] +  "</td>\n")
					page_html_file.write("					</tr>\n")


				# contributors

				for contributor_id in  pcb['contributors']:
					self.contributors_object.load_contributor_id(contributor_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Contributor</td>\n")
					page_html_file.write("						<td>" +  self.contributors_object.return_link() +  "</td>\n")
					page_html_file.write("					</tr>\n")

				#notes



				page_html_file.write("				</table>\n")

				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")


				page_html_file.write(self.screenshot_object.rom_info_html(self.rom_object.rom_info))
				
				# chips

				for chip in self.pcb_object.pcb['chipSlotType']:
					self.pcb_object.chip_object.clear_entry()

					found, chiptype = self.pcb_object.chip_object.return_chiptype(chip['chiptype_id'])


					for each_chip in pcb['chip_data']:

						if each_chip['slot_number'] == chip['slot_number']:
							self.chips_object.clear_entry()
							self.chips_object.chip.update(each_chip['chip_info'])

					page_html_file.write(self.chips_object.write_html_chip_page(slot_number=chip['slot_number']))




				page_html_file.close()

				# move to site


				sc_file = str(pcb['pcb_scan_id']) + ".html"
				self.file_object.move_html_to_site(temp_file, sc_file, "pcbs")


		self.file_object.move_html_to_site(self.table_temp_file, self.table_file)

		check_file.close()


		self.file_object.move_html_to_site(self.check_temp_file, self.check_file)
# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	pcb_object = pcb_scans()

	print("Options:")
	print("1) add pcb entry")
	print("2) edit pcb entry")
	print("3) Create HTML files for pcb information")
	print("4) Print pcbs in database")


	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			pcb_object.add_pcb()
		elif(entry==2):
			pcb_object.edit_pcb()
		elif(entry==3):
			pcb_object.create_html_files()
		elif(entry==4):
			pcb_object.print_pcb_info()
		else:
			print("fail")

	except ValueError:
		print("fail")
