import sys
import os
import shutil
import filecmp
from entries import entries
from titles import titles
from rom_image import rom_image


def move_file(file1, file2):
	exists = os.path.isfile(file2)
	if not exists:
		print(f"new file: {file2}")
		shutil.copyfile(file1,file2)
	else:
		if not filecmp.cmp(file1,file2):
			print(f"file change: {file2}")
			shutil.copyfile(file1,file2)

if len(sys.argv) != 2:
	print("You have to enter the entry number as a command line option")
	exit()

entry_number = sys.argv[1]

try:
	entry_number = int(entry_number)
except ValueError:
	print("you have to enter a valid entry number")
	exit()

entry_object = entries()

found = entry_object.load_entry(entry_number)
if not found:
	print("You have to enter an existing database entry number!")

path = entry_object.short_path()

SCpath = "../SNES_Central/article_files/" + path



if not os.path.exists(SCpath):
	os.makedirs(SCpath)

#################
# make title file
#################

title_file = "title.txt"
temp_title_file = 'temp/' + title_file
sc_title_file=SCpath + title_file

title_file_open = open(temp_title_file, 'w')

title_file_open.write(entry_object.entry['title'])

title_file_open.close()

move_file(temp_title_file, sc_title_file) 

###################
#### write titles table and write to file
###################

title_object = titles()

title_object.load_titles(entry_number)

title_object.create_html_file()

sc_titles_file = SCpath + title_object.html_file
move_file(title_object.temp_file, sc_titles_file)

###################
#### write ROM images table and write to file
###################

rom_image_object = rom_image()
rom_image_object.create_html_file(entry_number)

sc_rom_image_file = SCpath + rom_image_object.html_file

move_file(rom_image_object.temp_file, sc_rom_image_file)

