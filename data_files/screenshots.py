import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from file_handling import file_handling
from rom_image import rom_image

sys.path.append('../contributors')
from contributors import contributor

class screenshots:
	def __init__(self, entry_id = ''):

		self.screenshot_data = []

		self.screenshot = {
		  'screenshot_id': '', # Unique number for each screenshot. This is an integer.
		  'rom_image_id': '', # unique number corresponding to the ROM image
		  'description' : '', # description of the screenshot
		  'title_screen' : False, # default is false, change to true if it is the title screen,
		  'contributors' : [], # might as well add this
		}


		self.total_screenshots = 0
		self.current_screenshot = ''

		self.next_screenshot_id = 0
		
		self.screenshot_filename = ''


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "screenshots.yaml"
		self.folder_filename = self.entry_object.data_path + "screenshots"

		# create a ROM image object

		self.rom_object = rom_image(self.entry_object.entry['id_number'])

		# create contributors object

		self.contributors_object = contributor()

		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])

		# load yaml file containing the dictionary of the screenshot entries

		self.largest_screenshot_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.screenshot_data.insert(i, RIP.copy())
					if self.screenshot_data[i]['screenshot_id'] > self.largest_screenshot_id:
						self.largest_screenshot_id = self.screenshot_data[i]['screenshot_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any screenshots entered yet\n")



		self.number_of_screenshots = i


		self.rom_html_file = "rom_images.html"
		self.rom_temp_file = "temp/" + self.rom_html_file


	def clear_screenshot(self):

		for key, item in self.screenshot.items():
			if key == 'title_screen':
				self.screenshot[key] = False
			else:
				self.screenshot[key] = ''
		self.current_screenshot = ''


	def load_screenshot_info(self, screenshot_id=''):


		self.clear_screenshot()


		done = False
		found = False
		if not done: 

			if screenshot_id == '':

				print(f"Select a number corresponding to the screenshot you want:")

				screenshot_id  = input(">>: ")


			try:
				screenshot_id = int(screenshot_id)

				if self.check_id(screenshot_id):


					for i in range(len(self.screenshot_data)):

						if screenshot_id == self.screenshot_data[i]['screenshot_id']:
							self.screenshot.update(self.screenshot_data[i])
							found = True
							break
					done = True

				else:
					print("error: number out of range")

			except ValueError: 
				print("error: you did not enter an integer")



		self.current_screenshot = screenshot_id

		return found

	def check_id(self, entry_id):
		found = False
		for screenshot in self.screenshot_data:
			if entry_id == screenshot['screenshot_id']:
				found=True

		return found


	########################
	# adding screenshot
	########################

	def add_screenshot(self, rom_id=''):


		# connect with a rom image
		found = self.rom_object.load_rom_image(rom_id)

		if not found:
			print("cannot proceed")
			sys.exit()

		self.largest_screenshot_id += 1
		self.screenshot['screenshot_id'] = self.largest_screenshot_id



		self.screenshot['rom_image_id'] = self.rom_object.rom_info['rom_id']

		# move file

		self.file_object.add_file('screenshots',self.screenshot['screenshot_id'])


		# add descriptions

		self.add_description()

		# add contributors

		self.screenshot['contributors'] = self.contributors_object.add_contributor_list()


		self.designate_title_screen()

		self.screenshot_data.append(self.screenshot)

		self.total_screenshots += 1

		self.write_screenshot_info()

	def write_screenshot_info(self):

		if len(self.screenshot_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.screenshot_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No screenshot information has been added")

	########################
	# editing screenshot
	########################

	def edit_screenshot(self, screenshot_id=''):

		changed = False

		self.load_screenshot_info(screenshot_id)

		# connect with a rom image
		found = self.rom_object.load_rom_image(self.screenshot['rom_image_id'])

		self.rom_object.print_current_rom_image_info()

		print("Do you want to change the ROM image? (y to change)")

		yes_no = input(">>> ")

		if yes_no == 'y':
			found = self.rom_object.load_rom_image()
			if found:
				self.screenshot['rom_image_id'] = self.rom_object.rom_info['rom_id']
				changed = True
		

		# edit descriptions

		self.print_description()

		print("Do you want to change the description? (y to change)")

		yes_no = input(">>> ")

		if yes_no == 'y':
			self.add_description()
			changed = True

		# change contributors

		self.contributors_object.print_contributor_list(self.screenshot['contributors'])

		print("\nContributors, add (a), or delete (d) (enter to continue)")

		option = input(">>> ")

		if option == "a":
			self.screenshot['contributors'] = self.contributors_object.add_contributor_list(self.screenshot['contributors'])
			changed = True
		elif option == "d":
			self.screenshot['contributors'] = self.contributors_object.delete_contributor_list(self.screenshot['contributors'])
			changed = True

		test_var = self.screenshot['title_screen']
		self.designate_title_screen()

		if test_var != self.screenshot['title_screen']:
			changed = True

		if changed:
			counter = 0
			for screenshot in self.screenshot_data:
				if screenshot['screenshot_id'] == self.screenshot['screenshot_id']:
					self.screenshot_data[counter] = self.screenshot
					break
				else:
					counter += 1

			self.write_screenshot_info()

	#############################
	##### Methods for description #####
	#############################

	def add_description(self):

		print("\nEnter the description:")
		description_value = input(">>> ")
		self.screenshot['description'] = description_value 

	def print_description(self):
		print(f"\ndescription: {self.screenshot['description']}")

	#############################
	##### Methods for title screen #####
	#############################

	def designate_title_screen(self):

		if self.screenshot['title_screen']:
			print(f"Title screen id {self.screenshot['screenshot_id']} is currently designated as a title screen.\nChange? (y for yes):")
			yes_no = input(">>> ")
			if yes_no == 'y':
				self.screenshot['title_screen'] = False
		else:
			print(f"Title screen id {self.screenshot['screenshot_id']} is currently not designated as a title screen.\nChange? (y for yes):")
			yes_no = input(">>> ")
			if yes_no == 'y':
				self.screenshot['title_screen'] = True


	def find_title_screen(self,rom_id):

		found = False
		for i in range(len(self.screenshot_data)):
			self.load_screenshot_info(i)
			if self.screenshot['rom_image_id'] == rom_id and self.screenshot['title_screen']:
				found = True
				break

		return found

	######################################################
	##### Method for getting the link on the website #####
	######################################################

	def return_screenshot_sc_path(self,screenshot_id=''):

		self.load_screenshot_info(screenshot_id)

		filename = self.file_object.get_filename('screenshots',self.screenshot['screenshot_id'])
		sc_path = self.file_object.get_sc_path('screenshots')

		full_filename = sc_path + filename

		return full_filename

	######################################################
	##### Method for moving screenshot files to site #####
	######################################################

	def move_screenshots_to_site(self):

		for i in range(len(self.screenshot_data)):
			self.load_screenshot_info(i)

			self.file_object.move_to_site('screenshots',self.screenshot['screenshot_id'])


	#################################################
	##### Method for creating ROM image listing #####
	#################################################

	def rom_info_html(self, rom_image):

		myIterator = itertools.cycle([1,2])
		# find region
		found2, region = self.rom_object.find_region_name(rom_image['region'])
		version_number = rom_image['version_number']
		
		# I am really hoping that the first title is always the most relevant
		self.rom_object.title_object.load_title(rom_image['title_id_values'][0])
		title = self.rom_object.title_object.title['title']




		html_all =            "		<div class=\"chip_table\">\n"
		html_all = html_all + "			<div class=\"chip_heading\">\n"
		html_all = html_all + "				<div class=\"chip_heading_text\">" + title + " (" + region + ") (" + version_number +  ")</div>\n"
		html_all = html_all + "			</div>\n"
		html_all = html_all + "			<div class=\"chip_bulk\">\n"


		found_screenshot = self.find_title_screen(rom_image['rom_id'])
		if found_screenshot:
			sc_file = self.return_screenshot_sc_path(self.screenshot['screenshot_id'])
			html_all = html_all + "				<div class=\"screen_image\"><img src = \"" + sc_file + "\"></div>\n"
		else:
			html_all = html_all + "				<div class=\"screen_image\"><img src = \"icon/sneslogo_256x209.png\"></div>\n"


		html_all = html_all + "				<div class=\"chip_info\">\n"

		# output information

		

		for title_id in rom_image['title_id_values']:
			self.rom_object.title_object.load_title(title_id)
			title = self.rom_object.title_object.title['title']

			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Title</b>: " + title

			html_all = html_all + "</div></div>\n"



		html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Region</b>: " + region +"</div></div>\n"
		html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Version Number</b>: " + version_number +"</div></div>\n"

		# serial code
		if rom_image.get('serial_code'):
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Serial Code</b>: " + rom_image['serial_code'] +"</div></div>\n"

		# release types
		if rom_image.get('release_type_id'):
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Release Type</b>: " 

			comma = ""
			for release_type_id in rom_image['release_type_id']:
				release = self.rom_object.get_release_status(release_type_id)
				html_all = html_all + comma + release
				comma = ", "


			html_all = html_all + "</div></div>\n"



		# PCB types
		if rom_image.get('PCB_id_values'):

			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>PCB boards/Mapping</b>: " 

			comma = ""
			for pcb_id in rom_image['PCB_id_values']:
				link = self.rom_object.pcb_object.pcb_link(pcb_id)

				html_all = html_all + comma + link
				comma = ", "


			html_all = html_all + "</div></div>\n"

		# crc32
		if rom_image.get('crc32'):
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>CRC32</b>: " + rom_image['crc32'] +"</div></div>\n"
		# sha256
		if rom_image.get('sha256'):
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>SHA256</b>: <div class=\"breakword\">" + rom_image['sha256'] +"</div></div></div>\n"

		if rom_image.get('contributors'):

			if len(rom_image['contributors']) > 1:
				add_s = "s"
			else:
				add_s = ""
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Contributor" + add_s + "</b>: " 
			comma = ""
			for contributor_id in rom_image['contributors']:
				found = self.contributors_object.load_contributor_id(contributor_id)

				html_all = html_all + comma + self.contributors_object.return_link()
				comma = ", "

			html_all = html_all + "</div></div>\n"

		if rom_image.get('notes'):
			html_all = html_all + "					<div class=\"screen_cell" + str(next(myIterator)) + "\"><div class=\"chip_text\"><b>Notes</b>: " + rom_image['notes'] +"</div></div>\n"
		

		html_all = html_all + "				</div>\n"
		html_all = html_all + "			</div>\n"
		html_all = html_all + "		</div>\n"

		return html_all

	def create_rom_image_html_file(self):

		
		html_file = open(self.rom_temp_file, 'w')

		html_all = "		<p class=\"name\">ROM images</p>\n"


		number_rom_images = len(self.rom_object.rom_image_archive)

		if number_rom_images > 0:

			for rom_image in self.rom_object.rom_image_archive:

				html_all = html_all + self.rom_info_html(rom_image)

		else:
			html_all = html_all + "	<p>No ROM images associated with this entry</p>\n"

		html_file.write(html_all)
		html_file.close()

		self.file_object.move_html_to_site(self.rom_temp_file, self.rom_html_file)


# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	screenshot_object = screenshots()

	print("Options:")
	print("1) add screenshot entry")
	print("2) edit screenshot entry")
	print("3) move all screenshots to SNES Central")
	print("4) Create HTML file for ROM image information")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			screenshot_object.add_screenshot()
		elif(entry==2):
			screenshot_object.edit_screenshot()
		elif(entry==3):
			screenshot_object.move_screenshots_to_site()
		elif(entry==4):
			screenshot_object.create_rom_image_html_file()
		else:
			print("fail")

	except ValueError:
		print("fail")

