#!/usr/bin/env python

import yaml
import sys
import os
import itertools
from entries import entries
from file_handling import file_handling

class titles:

	def __init__(self, entry_id = ''):

		self.title_data = []

		self.title = {
		  'title_id': '', # Unique number for each title. This is an integer.
		  'language_id' : '', # ID number to point to self.standard_languages to get the language information. This is an integer.
		  'title': '', # title
		  'translation' : '' # translation of the title to English, if it is necessary
		}

		self.standard_languages = [
		  {'language_id':0, 'language': 'English'},
		  {'language_id':1, 'language': 'French'},
		  {'language_id':2, 'language': 'German'},
		  {'language_id':3, 'language': 'Portuguese'},
		  {'language_id':4, 'language': 'Italian'},
		  {'language_id':5, 'language': 'Spanish'},
		  {'language_id':6, 'language': 'Japanese - kanji'},
		  {'language_id':7, 'language': 'Japanese - kana'},
		  {'language_id':8, 'language': 'Japanese - romaji'},
		  {'language_id':9, 'language': 'Chinese - hanzi'},
		  {'language_id':10, 'language': 'Chinese - pinyin'},
		  {'language_id':11, 'language': 'Korean - hangul'},
		  {'language_id':12, 'language': 'Korean - revised romanization'}
		]


		# load the entry value so that we can load the titles for the game

		entry_object = entries()
		entry_object.load_entry(entry_id)

		# create file handling object

		self.file_object = file_handling(entry_object.entry['id_number'])

		# load the titles file

		self.title_file = entry_object.data_path + "titles.yml"

		self.total_titles = 0
		self.next_id = 0

		i = 0
		if os.path.exists(self.title_file):
			file = open(self.title_file, 'r')
			dataload = yaml.load_all(file, Loader=yaml.FullLoader)
			for load_entries in dataload:
				for individual_entries in load_entries:
					self.title_data.insert(i,individual_entries)
					i+=1

					if individual_entries['title_id'] > self.next_id:
						self.next_id = individual_entries['title_id']
			file.close()

			self.next_id = self.next_id + 1

			self.total_titles = len(self.title_data)


		else:
			print(f"id #{entry_object.entry['id_number']} ({entry_object.entry['title']}) does not have any title entries yet.\n")


		self.current_entry = ''

		self.html_file = "titles.html"
		self.temp_file = "temp/" + self.html_file

	##############################################################################
	# clears entry
	##############################################################################

	def clear_entry(self):

		for item in self.title:
			self.title[item] = ''
		self.current_entry = ''

	##############################################################################
	# takes the title data from title_id and puts it into self.title
	##############################################################################

	def load_title(self, title_id):

		self.clear_entry()

		found = False
		if self.total_titles > 0:

			if title_id == '':

				print("enter title id number:")
				title_id = input(">>> ")


			try:
				title_id = int(title_id)

			except ValueError: 
				print("error: you did not enter an integer")
				sys.exit()


			found, current_entry = self.check_id(title_id)


			if found:
				self.current_entry = current_entry

			else:
				print(f"Could not find title with title_id: {title_id}")

		else:
			print("no tiles to entered yet!")

		return found

	##############################################################################
	# check if the id value exists
	##############################################################################

	def check_id(self, title_id):

		found = False
		current_entry = -1
		try:
			title_id = int(title_id)

			for i in range(len(self.title_data)):


				if self.title_data[i]['title_id'] == title_id:

					self.title.update(self.title_data[i])
					found = True
					current_entry = title_id
		except ValueError: 
			found = False


		return found, current_entry


	##############################################################################
	# prints all of the titles
	##############################################################################

	def print_titles(self):

		print("\nCurrently documented titles:")

		if self.total_titles > 0:
			for item in self.title_data:

				self.load_title(item['title_id'])
				self.print_current_title()

		else:
			print("\nno entries found")

	##############################################################################
	# prints selected of the titles
	##############################################################################

	def print_selected_titles(self, title_array):

		print("\nTitles associated with this:")

		if self.total_titles > 0:
			for item in title_array:

				self.load_title(item)
				self.print_current_title()

		else:
			print("\nno entries found")

	##############################################################################
	# Returns an list of title id values
	##############################################################################

	def get_id_list(self, id_list = []):

		print("\nEnter the titles you want associated with this.")
		done = False
		changed = False
		while not done:

			self.print_titles()

			if id_list:
				print(f"\n id numbers already selected: {id_list}")
			print("\nSelect id number (repeat selected number to delete, enter for done):")
			title_id = input(">>> ")

			found, current_entry = self.check_id(title_id)

			if found:

				changed = True
				remove = False
				if id_list:
					for id_value in id_list:
						if id_value == current_entry:
							remove = True
				if remove:
					id_list.remove(current_entry)
				else:
					id_list.append(current_entry)
			else:
				done = True

			print("\n")

		return id_list, changed


	##############################################################################
	# Prints a list of the languages that are available
	##############################################################################

	def print_languages(self):

		for language in self.standard_languages:

			print(f"{language['language_id']}) {language['language']}")
			
		print("\n")

	##############################################################################
	# asks for language_id if not given, and checks to make sure it is valid
	##############################################################################

	def ask_for_language(self,language_id = ''):

		if language_id == '':
			self.print_languages()
			print("Enter the language id value")
			language_id = input(">>> ")

		try:
			language_id = int(language_id)

		except ValueError: 
			print("error: you did not enter an integer, exiting")
			sys.exit()

		found = False

		for item in self.standard_languages:

			if item['language_id'] == language_id:
				language = item['language']
				found = True

		if not found:
			print("Invalid language id value, exiting")
			sys.exit()

		return language_id, language

	##############################################################################
	# asks for the language_id, and puts it into self.title
	##############################################################################

	def enter_language(self,language_id = ''):

		language_id, language = self.ask_for_language(language_id)

		self.title['language_id'] = language_id

	##############################################################################
	# asks to enter the title, and puts it into self.title
	##############################################################################

	def enter_title(self,title = ''):

		if title == '':

			print("Enter the title")

			title = input(">>> ")

		if title == '':

			print("error: you did not enter a title, exiting")
			sys.exit()

		self.title['title'] = title

	##############################################################################
	# asks to enter the translation, and puts it into self.title
	##############################################################################

	def enter_translation(self,translation = ''):

		if translation == '':

			print("Enter the translation (enter if none desired)")

			translation = input(">>> ")

		if translation != '':

			self.title['translation'] = translation

	##############################################################################
	# Adds a new title
	##############################################################################

	def add_title(self):

		print("adding new title\n")

		self.clear_entry()

		self.enter_language()

		self.enter_title()



		if self.title['language_id'] != 0:
			self.enter_translation()

		
		self.title['title_id'] = self.next_id

		self.title_data.append(self.title)
		self.total_titles += 1
		self.next_id = self.next_id + 1

		self.__write_entries()



	##############################################################################
	# Edit a title
	##############################################################################

	def edit_title(self, title_id = ''):

		change = False

		self.load_title(title_id)

		print("Editing tile: ")

		self.print_current_title()

		# edit the language
		language_id, language = self.ask_for_language(self.title['language_id'])
		done_language = False
		while not done_language:
			print(f"Current language set to ({language}), do you want to change it y/n:")
			option = input(">>> ")
			if option == "y":
				self.enter_language()
				done_language = True
				change = True
			elif option == "n":
				done_language = True
			else:
				print("invalid option")

		# edit the title
		done_title = False
		while not done_title:
			print(f"Current title set to ({self.title['title']}), do you want to change it y/n:")
			option = input(">>> ")
			if option == "y":
				self.enter_title()
				done_title = True
				change = True
			elif option == "n":
				done_title = True
			else:
				print("invalid option")

		# edit the translation

		if self.title['language_id'] != 0:
			done_translation = False
			while not done_translation:
				print(f"Current translation set to ({self.title['translation']}), do you want to change it y/n:")
				option = input(">>> ")
				if option == "y":
					self.enter_translation()
					done_translation = True
					change = True
				elif option == "n":
					done_translation = True
				else:
					print("invalid option")
		else:
			self.title['translation'] = ""
		if change:
			self.title_data[self.current_entry].update(self.title)

			self.__write_entries()

	##############################################################################
	# writes to file
	##############################################################################

	def __write_entries(self):

		# this will ensure that the file doesn't change too much between commits
		self.title_data = sorted(self.title_data, key=lambda k: k['title_id']) # sort the entries by number

		file = open(self.title_file, 'w')
		yaml.dump(self.title_data, file, default_flow_style=False, allow_unicode=True)
		file.close()
		

	##############################################################################
	# prints the information in the currently loaded title
	##############################################################################

	def print_current_title(self):

		language_id, language = self.ask_for_language(self.title['language_id'])
		print(f"{self.title['title_id']}) {self.title['title']} ({language})" )


	##############################################################################
	# Writes an HTML table with the title data
	##############################################################################

	def create_html_file(self):

		table_created = True

		if self.total_titles > 0:

			myIterator = itertools.cycle([1,2])
			html_file = open(self.temp_file, 'w')

			html_file.write("		<p class=\"name\">Game Titles</p>\n")

			html_file.write("		<table class=\"infotable2\">\n")
			html_file.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
			html_file.write("				<td width=\"150px\"><b>Language</b></td>\n")
			html_file.write("				<td width=\"500px\" ><b>Title information</b></td>\n")
			html_file.write("			</tr>\n")

			for language in self.standard_languages:
				for title in self.title_data:

					self.clear_entry()
					self.title.update(title)

					if self.title['language_id'] == language['language_id']:

						rowcount = str(next(myIterator))


						html_file.write(f"			<tr class=\"row" + rowcount + "\">\n")
						html_file.write(f"				<td width=\"100px\">{language['language']}</td>\n")
						if self.title['translation'] == '':
							html_file.write(f"				<td width=\"550px\">{self.title['title']}</td>\n")
						else:
							html_file.write(f"				<td width=\"550px\">{self.title['title']} ({self.title['translation']})</td>\n")

						html_file.write("			</tr>\n")
			html_file.write("		</table>")

			html_file.close()

			self.file_object.move_html_to_site(self.temp_file, self.html_file)

		else:

			print("No titles have been entered")

			table_created = False

		return table_created

# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	titles_object = titles()

	print("Options:")
	print("1) add title entry")
	print("2) edit title entry")
	print("3) Create HTML file for title information")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			titles_object.add_title()
		elif(entry==2):
			titles_object.edit_title()
		elif(entry==3):
			titles_object.create_html_file()
		else:
			print("fail")

	except ValueError:
		print("fail")

