#!/usr/bin/env python

class shared_values: 

	def __init__(self):

		# general regions
		self.region = [
			  {'id':0, 'region':'Americas'}, 
		        {'id':1, 'region':'Asia'}, 
		        {'id':2, 'region':'PAL'},
		        {'id':3, 'region':'NTSC'},
		        {'id':4, 'region':'unknown'}
			 ]

		self.region_selection = {'id': '', 'region': ''}

		# country codes used on packaging
		self.country = [
			  {'id':0, 'region_code':'USA', 'country':'USA'},
			  {'id':1, 'region_code':'CAN', 'country':'Canada'}, 
			  {'id':2, 'region_code':'LTN', 'country':'Latin America'},
			  {'id':3, 'region_code':'-', 'country':'Brazil'},  
		        {'id':4, 'region_code':'JPN',  'country':'Japan'},
		        {'id':5, 'region_code':'KOR',  'country':'South Korea'},
		        {'id':6, 'region_code':'HKG',  'country':'Hong Kong'},
		        {'id':7, 'region_code':'ROC',  'country':'Taiwan (Republic of China)'},
		        {'id':8, 'region_code':'EUR',  'country':'Europe'},
		        {'id':9, 'region_code':'UKV',  'country':'United Kingdom'},
		        {'id':10, 'region_code':'FRA',  'country':'France'},
		        {'id':11, 'region_code':'FAH',  'country':'France, Belgium, Netherlands, Luxembourg'},
		        {'id':12, 'region_code':'NOE',  'country':'Germany (Nintendo of Europe)'},
		        {'id':13, 'region_code':'FRG',  'country':'Switzerland, Austria'},
		        {'id':14, 'region_code':'ITA',  'country':'Italy'},
		        {'id':15, 'region_code':'HOL',  'country':'Netherlands'},
		        {'id':16, 'region_code':'ESP',  'country':'Spain'},
		        {'id':17, 'region_code':'SCN',  'country':'Scandinavia'},
		        {'id':18, 'region_code':'EEC',  'country':'European Economic Community'},
		        {'id':19, 'region_code':'AUS',  'country':'Australia'}
			 ]

		self.country_selection = {'id':'', 'region_code':'',  'country':''}


		# this is the country code that is the fourth character of a serial code (usually language), taken from the SNES development manual
		self.country_code = [
			  {'id':0, 'destination':'Japan', 'code':'J'},
			  {'id':1, 'destination':'North America (USA and Canada)', 'code':'E'},
			  {'id':2, 'destination':'Europe', 'code':'P'},
			  {'id':3, 'destination':'Scandinavia', 'code':'W'},
			  {'id':6, 'destination':'Europe (French)', 'code':'F'},
			  {'id':7, 'destination':'Dutch', 'code':'H'},
			  {'id':8, 'destination':'Spanish', 'code':'S'},
			  {'id':9, 'destination':'German', 'code':'D'},
			  {'id':10, 'destination':'Italian', 'code':'I'},
			  {'id':11, 'destination':'Chinese', 'code':'C'},
			  {'id':13, 'destination':'Korean', 'code':'K'},
			  {'id':14, 'destination':'Common', 'code':'A'},
			  {'id':15, 'destination':'Canada', 'code':'N'},
			  {'id':16, 'destination':'Brazil', 'code':'B'},
			  {'id':17, 'destination':'Australia', 'code':'U'}
			 ]

		self.country_code_selection = {'id':'', 'destination':'', 'code':''}

		# companies
		self.company = [
			  {'id':0, 'company':'Nintendo'},
			  {'id':1, 'company':'Capcom'},
			  {'id':2, 'company':'Square'},
			  {'id':3, 'company':'Konami'},
			  {'id':4, 'company':'Electronic Arts'},
			  {'id':5, 'company':'Mattel'}
			 ]

		self.company_selection = {'id':'', 'company':''}

		# manufacturing location

		self.manufacturing_location = [
			  {'id':0, 'manufacturing_location':'Made in Japan'},
			  {'id':1, 'manufacturing_location':'Made in Mexico'},
			  {'id':2, 'manufacturing_location':'Assembled in Mexico'},
			  {'id':3, 'manufacturing_location':'Made in Puerto Rico'},
			  {'id':4, 'manufacturing_location':'Brazil (Playtronic)'},
			  {'id':5, 'manufacturing_location':'Brazil (Gradiente)'},
			  {'id':6, 'manufacturing_location':'unknown/not stated'}
			 ]

		self.manufacturing_location_selection =  {'id':'', 'manufacturing_location':''}

	#################################
	##### Methods for region id #####
	#################################

	def print_region(self):

		print("List of regions:")

		for region in self.region:
			
			print(f"{region['id']})\t{region['region']}")

	def return_region(self):

		self.print_region()

		done = False

		while not done:
			print("Enter region id")
			id_value = input(">>> ")

			try:
				id_value = int(id_value)

				for region in self.region:
					
					if region['id'] == id_value:
						done = True
						break

				if not done:
					print("invalid number")

			except ValueError:

				print("you must enter a number")

		return id_value

	def edit_region(self,id_value):
		
		changed = False
		self.load_region(id_value)

		print(f"The region is currently set to ({self.region_selection['region']}), do you want to change? y for yes")

		option = input(">>> ")

		if option == 'y':
			id_value = self.return_region()
			changed = True

		return changed, id_value

	def load_region(self, id_value):

		try:
			id_value = int(id_value)

			for region in self.region:

				if region['id'] == id_value:
					self.region_selection.update(region)

		except ValueError:

			print("invalid id number")

	#################################
	##### Methods for countries #####
	#################################

	def print_country(self):

		print("List of countries:")

		for codes in self.country:
			
			print(f"{codes['id']})\t{codes['region_code']}\t[{codes['country']}]")

	def return_country(self):

		self.print_country()

		done = False

		while not done:
			print("Enter country id")
			id_value = input(">>> ")

			try:
				id_value = int(id_value)

				for country in self.country:
					
					if country['id'] == id_value:
						done = True
						break

				if not done:
					print("invalid number")

			except ValueError:

				print("you must enter a number")

		return id_value

	def edit_country(self,id_value):
		
		changed = False
		self.load_country(id_value)

		print(f"The country is currently set to ({self.country_selection['region_code']}), do you want to change? y for yes")

		option = input(">>> ")

		if option == 'y':
			id_value = self.return_country()
			changed = True

		return changed, id_value

	def load_country(self, id_value):

		try:
			id_value = int(id_value)

			for country in self.country:

				if country['id'] == id_value:
					self.country_selection.update(country)

		except ValueError:

			print("invalid id number")

	####################################
	##### Methods for country code #####
	####################################

	def print_country_code(self):

		print("List of country codes:")

		for codes in self.country_code:
			
			print(f"{codes['id']})\t{codes['code']}\t[{codes['destination']}]")

	def return_country_code(self):

		self.print_country_code()

		done = False

		while not done:
			print("Enter country code id")
			id_value = input(">>> ")

			try:
				id_value = int(id_value)

				for country in self.country_code:
					
					if country['id'] == id_value:
						done = True
						break

				if not done:
					print("invalid number")

			except ValueError:

				print("you must enter a number")

		return id_value

	def edit_country_code(self,id_value):
		
		changed = False
		self.load_country_code(id_value)

		print(f"The country code is currently set to ({self.country_code_selection['destination']}, {self.country_code_selection['code']}), do you want to change? y for yes")

		option = input(">>> ")

		if option == 'y':
			id_value = self.return_country_code()
			changed = True

		return changed, id_value

	def load_country_code(self, id_value):

		try:
			id_value = int(id_value)

			for country in self.country_code:

				if country['id'] == id_value:
					self.country_code_selection.update(country)

		except ValueError:

			print("invalid id number")


	#################################
	##### Methods for companies #####
	#################################

	def print_companies(self):

		print("List of companies:")

		for company in self.company:
			
			print(f"{company['id']})\t{company['company']}")

	def return_company(self, id_list = []):

		if not id_list:
			id_list = []

		self.print_companies()

		done = False

		question_text = "Enter company id (enter for done)"
		while not done:


			print(question_text)
			id_value = input(">>> ")

			if id_value:
				try:
					id_value = int(id_value)


					found = False
					for company in self.company:

						question_text = "Enter another company id (enter for done)"			
						if company['id'] == id_value:
							id_list.append(id_value)
							found = True
							break

					if not found:
						print("invalid number")

				except ValueError:

					print("you must enter a number")

			else:
				done = True


		# check if there are duplicates

		self.remove_duplicates(id_list)
			

		return id_list


	def edit_company(self,id_value_list):
		
		changed = False

		if id_value_list:
			print("Currently assigned companies:")

			for id_value in id_value_list:

				self.load_company(id_value)

				print(f"{self.company_selection['id']})\t{self.company_selection['company']}")

			print(f"Type a to add another company, or r to remove one")

		else:
			print(f"There are currently no companies assigned, do you want to add? a for add")

		option = input(">>> ")

		if option == 'a':
			id_value_list = self.return_company(id_value_list)

			changed = True
		elif option == 'r':
			print("enter the id number of the company you want to remove (enter to abort)")
			try:
				id_value = input(">>> ")
				id_value = int(id_value)

				counter = 0
				for id_list_value in id_value_list:
					print(f"{id_list_value}, {id_value}")
					if id_list_value == id_value:
						del id_value_list[counter]
						changed = True
						break
					else:
						counter += 1
			except ValueError:
				print("you must enter an integer")

		return changed, id_value_list

	def load_company(self, id_value):

		try:
			id_value = int(id_value)

			for company in self.company:

				if company['id'] == id_value:
					self.company_selection.update(company)

		except ValueError:

			print("invalid id number")

	##############################################
	##### Methods for manufacturing location #####
	##############################################

	def print_manufacturing_locations(self):

		print("List of manufacturing locations:")

		for manufacturer in self.manufacturing_location:
			
			print(f"{manufacturer['id']})\t{manufacturer['manufacturing_location']}")


	def return_manufacturing_location(self):

		self.print_manufacturing_locations()

		done = False

		while not done:
			print("Enter manufacturing location id ")
			id_value = input(">>> ")

			try:
				id_value = int(id_value)

				for manufacturing in self.manufacturing_location:
					
					if manufacturing['id'] == id_value:
						done = True
						break

				if not done:
					print("invalid number")

			except ValueError:

				print("you must enter a number")

		return id_value

	def edit_manufacturing_location(self,id_value):
		
		changed = False
		self.load_manufacturing_location(id_value)

		print(f"The country is currently set to ({self.manufacturing_location_selection['manufacturing_location']}), do you want to change? y for yes")

		option = input(">>> ")

		if option == 'y':
			id_value = self.return_manufacturing_location()
			changed = True

		return changed, id_value

	def load_manufacturing_location(self, id_value):

		try:
			id_value = int(id_value)

			for manufacturing_location in self.manufacturing_location:

				if manufacturing_location['id'] == id_value:
					self.manufacturing_location_selection.update(manufacturing_location)

		except ValueError:

			print("invalid id number")


	#################
	##### notes #####
	#################

	def add_notes(self):

		print("Add any notes")
		notes = input(">>> ")
		return notes

	def edit_notes(self, notes = ''):


		changed = False
		if notes:
			print("The current notes:")
			print(notes)
			print("Do you want to change the notes (y for yes)?")


		else:
			print("There are currently no notes entered, do you want to enter some (y for yes)")

		entry = input(">>> ")

		if entry == 'y':
			print("Add any notes")
			notes = input(">>> ")
			changed = True

		return notes, changed

	#################
	##### serial code #####
	#################

	def add_serial_code(self):

		print("Add the serial code")
		serial_code = input(">>> ")
		return serial_code

	def edit_serial_code(self, serial_code = ''):

		print(f"The serial code is currently \"{serial_code}\", do you want to change it (y for yes)?")
		answer = input(">>> ")
		if answer == 'y':
			print("Enter the serial code")
			serial_code = input(">>> ")
			changed = True
		else:
			changed = False
		return changed, serial_code

	#####################
	##### utilities #####
	#####################

	def remove_duplicates(self, id_list):

		number_items = len(id_list)

		if number_items > 1:
		

			counter = 0
			while counter < number_items-1:
				if id_list[counter] == id_list[counter+1]:
					del id_list[counter+1]
					number_items -= 1
				else:
					counter += 1

		return id_list




if __name__ == "__main__":

	shared_object = shared_values()
	list_val = [0,1]

	print(shared_object.edit_manufacturing_location(''))


