import yaml
import sys
import os
import itertools
import shutil
import filecmp
from entries import entries
from titles import titles
from file_handling import file_handling
from shared_values import shared_values

sys.path.append('../contributors')
from contributors import contributor


class carts:
	def __init__(self, entry_id = ''):

		self.cart_data = []

		self.cart = {
		  'cart_id': '', # Unique number for each cart. This is an integer.
		  'region_id' : '', # id for the region of the cart label
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this cart
		  'title_id_values' : [], # pointers to the titles associated with this cart
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this cart label
		  'has_image' : '' # it is possible to not have an image
		}


		# load the entry value so that we can load the titles for the game

		self.entry_object = entries()
		self.entry_object.load_entry(entry_id)

		self.data_filename = self.entry_object.data_path + "carts.yaml"

		# create a shared values object

		self.shared_object = shared_values()

		# create a titles object

		self.title_object = titles(self.entry_object.entry['id_number'])

		# create contributors object

		self.contributors_object = contributor()


		# create file handling object

		self.file_object = file_handling(self.entry_object.entry['id_number'])



		# load yaml file containing the dictionary of the cart scan entries

		self.largest_cart_id = -1
		i=0
		try: 
			file = open(self.data_filename, 'r')
			datastore = yaml.load_all(file, Loader=yaml.FullLoader)

			for loadRIPs in datastore:
				for RIP in loadRIPs:
					self.cart_data.insert(i, RIP.copy())
					if self.cart_data[i]['cart_id'] > self.largest_cart_id:
						self.largest_cart_id = self.cart_data[i]['cart_id']
					i+=1

			file.close()
		except FileNotFoundError:
			print(f"Entry: {self.entry_object.entry['title']}\ndoes not have any cart scans entered yet\n")



		self.number_of_carts = i

		# site files
		self.table_file = "cart_table.html"
		self.table_temp_file = "temp/" + self.table_file 

		self.check_file = "cart_check.txt"
		self.check_temp_file = "temp/" + self.check_file


	def print_cart_info(self):
		print(f"Carts for entry id {self.entry_object.entry['id_number']}:\n")
		found_one = False

		if self.number_of_carts > 0:

			# first sort the data

			self.cart_data = sorted(self.cart_data, key=lambda k: k['cart_id']) # sort the entries by id

			for cart in self.cart_data:

				self.title_object.load_title(cart['title_id_values'][0])
				title = self.title_object.title['title']

				self.shared_object.load_country(cart['country_id'])
				country = self.shared_object.country_selection['country']

				print(f"{cart['cart_id']}) {title} ({country}) ({cart['serial_code']})")
		else:
			print("There are currently no carts entered")

	def clear_cart(self):
		temp_cart = {
		  'cart_id': '', # Unique number for each cart. This is an integer.
		  'region_id' : '', # id for the region of the cart label
		  'serial_code' : '', # the serial code
		  'country_id' : '', # the country id value
		  'manufacturing_id' : '', # the id for the manufacturing location
		  'company_id_values' : [], # pointers to the companies listed on this cart
		  'title_id_values' : [], # pointers to the titles associated with this cart
		  'contributors' : [], # who submitted the scan
		  'notes' : '',           # notes for this cart label
		  'has_image' : '' # it is possible to not have an image
		}

		self.cart.update(temp_cart)

	def load_cart(self,cart_id):

		found = False
		try: 

			cart_id = int(cart_id)
			for cart in self.cart_data:
				if cart['cart_id'] == cart_id:
					self.cart.update(cart)
					found = True
					break

			if not found:
				print(f"Cart id {cart_id} does not exit")

		except ValueError:
			print("you must enter an integer")

		return found

	def add_cart(self):

		self.clear_cart()


		self.largest_cart_id += 1

		self.cart['cart_id'] = self.largest_cart_id



		self.cart['region_id'] = self.shared_object.return_region()

		# add title

		self.cart['title_id_values'], changed = self.title_object.get_id_list()

		if not changed:
			print("It is necessary to add a title")
			sys.exit()


		self.cart['country_id'] = self.shared_object.return_country()	
		self.cart['serial_code'] = self.shared_object.add_serial_code()		

	
		self.cart['manufacturing_id'] = self.shared_object.return_manufacturing_location()
		self.cart['company_id_values'] = self.shared_object.return_company()

		# add contributors

		self.cart['contributors'], changed = self.contributors_object.add_contributor_list()

		self.cart['notes'] = self.shared_object.add_notes()

		print("Do you want to add an image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('carts',self.cart['cart_id'])
			self.cart['has_image'] = True
		else:
			self.cart['has_image'] = False


		self.cart_data.append(self.cart)

		self.write_info()

	def edit_cart(self, cart_id = ''):

		self.print_cart_info()

		update = False

		self.clear_cart()

		if cart_id == '':

			print("enter the cart id you want")
			cart_id = input(">>> ")

		try:

			cart_id = int(cart_id)
			found = self.load_cart(cart_id)

		except ValueError:
			print("error, you must input a number")

		if not found:
			print("could not find the entry, exiting")
			sys.exit()



		print("Do you want to add/change the image? (y for yes)")

		entry = input(">>> ")

		if entry == "y":
			self.file_object.add_file('carts',self.cart['cart_id'])
			self.cart['has_image'] = True
			update = True

		
		changed, id_value = self.shared_object.edit_region(self.cart['region_id'])
		if changed:
			self.cart['region_id'] = id_value
			update = True

		# add title

		id_value, changed = self.title_object.get_id_list(self.cart['title_id_values'])
		if changed:
			self.cart['title_id_values'] = id_value
			update = True

		changed, id_value = self.shared_object.edit_country(self.cart['country_id'])
		if changed:
			self.cart['country_id'] = id_value
			update = True


		changed, code = self.shared_object.edit_serial_code(self.cart['serial_code'])
		if changed:
			self.cart['serial_code'] = code
			update = True

	
		changed, id_value = self.shared_object.edit_manufacturing_location(self.cart['manufacturing_id'])
		if changed:
			self.cart['manufacturing_id'] = id_value
			update = True


		changed, id_value = self.shared_object.edit_company(self.cart['company_id_values'])
		if changed:
			self.cart['company_id_values'] = id_value
			update = True


		# add contributors

		id_value, changed = self.contributors_object.add_contributor_list(self.cart['contributors'])
		if changed:
			self.cart['contributors'] = id_value
			update = True

		notes, changed = self.shared_object.edit_notes(self.cart['notes'])
		if changed:
			self.cart['notes'] = notes
			update = True

		if update:
			counter = 0
			for cart in self.cart_data:
				if cart['cart_id'] == self.cart['cart_id']:
					self.cart_data[counter] = self.cart
					break
				else:
					counter += 1

			self.write_info()




	def write_info(self):

		if len(self.cart_data) > 0:
			
			file = open(self.data_filename,'w')
			yaml.dump(self.cart_data, file, default_flow_style=False, allow_unicode=True)
			file.close()
		else:
			print("No cart information has been added")

	def create_html_files(self):

		myIterator = itertools.cycle([1,2])

		# This goes through the list and creates a summary table with links to individual pages. 
		# It also creates a text file that the site uses to check for valid entries and prevent hacking problems.

		check_file = open(self.check_temp_file, 'w')

		# write the table file
		html_file = open(self.table_temp_file, 'w')


		# first create the base parts of table file

#		html_table = '<p class="name">Cartridge label information</p>\n'

		html_table = ""

		html_file.write("		<div class=\"chip_table\">\n")
		html_file.write("			<div class=\"chip_heading\">\n")
		html_file.write("				<div class=\"chip_heading_text\">Cartridge label information</div>\n")
		html_file.write("			</div>\n")
		html_file.write("			<div class=\"chip_bulk\">\n")
		html_file.write("			</div>\n")



		html_file.write("		<table class=\"infotable\">\n")
		html_file.write("			<tr class=\"row" + str(next(myIterator)) + "\">\n")
		html_file.write("				<td width=\"90px\"><b>Region</b></td>\n")
		html_file.write("				<td width=\"110px\" ><b>Country</b></td>\n")
		html_file.write("				<td width=\"140px\" ><b>Serial Code</b></td>\n")
		html_file.write("				<td width=\"290px\" ><b>Notes</b></td>\n")
		html_file.write("				<td width=\"70px\" ><b>Scans</b></td>\n")
		html_file.write("			</tr>\n")

		# first sort the data
		self.cart_data = sorted(self.cart_data, key=lambda k: k['serial_code']) # sort the entries by serial code
		self.cart_data = sorted(self.cart_data, key=lambda k: k['country_id']) # sort the entries by country
		self.cart_data = sorted(self.cart_data, key=lambda k: k['region_id']) # then sort the entries by region

		row_number = 1

		# loop through all the carts
		for cart in self.cart_data:


			self.title_object.load_title(cart['title_id_values'][0])

			if cart['has_image']:
				check_file.write(str(cart['cart_id']) +  "\t" + self.title_object.title['title'] + " (" + cart['serial_code'] + ")\n")

			# start row of the table

			html_file.write("	<tr class = \"row" + str(next(myIterator)) + "\">\n")

			# region value
			self.shared_object.load_region(cart['region_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.region_selection['region'] + "</td>\n")

			# country value
			self.shared_object.load_country(cart['country_id'])
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + self.shared_object.country_selection['country'] + "</td>\n")

			# serial code
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + cart['serial_code'] + "</td>\n")

			# notes
			html_file.write("		<td style=\"text-align: left; vertical-align: top;\">" + cart['notes'] + "</td>\n")

			if cart['has_image']:

				html_file.write("		<td style=\"text-align: left; vertical-align: top;\"><a href=\"cart_test.php?id=" + str(self.entry_object.full_id) + "&num=" + str(cart['cart_id']) + "\">link</a></td>\n")

			else:
				html_file.write("		<td style=\"text-align: left; vertical-align: top;\">scan needed</td>\n")
	


			# finish the table row

			html_file.write("	</tr>\n")
			if row_number == 1:
				row_number = 2
			else:
				row_number = 1

			if cart['has_image']:

				# move the scan to the site

				self.file_object.move_to_site('carts',cart['cart_id'])

				# create html page for the scan

				myIterator2 = itertools.cycle([2,1])

				temp_file="temp/page.html"
				page_html_file = open(temp_file, 'w')

				self.title_object.load_title(cart['title_id_values'][0])




				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">" + self.title_object.title['title'] + " (" + cart['serial_code'] + ") - Cart Information</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")
				sc_path = self.file_object.get_sc_path('carts')

				page_html_file.write("				<center><img src=\"" + sc_path + self.file_object.get_filename('carts',cart['cart_id']) + "\" alt=\"" + self.title_object.title['title'] + " (" + cart['serial_code'] + ")\" /></center>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")


				page_html_file.write("		<div class=\"chip_table\">\n")
				page_html_file.write("			<div class=\"chip_heading\">\n")
				page_html_file.write("				<div class=\"chip_heading_text\">Cart label documentation</div>\n")
				page_html_file.write("			</div>\n")
				page_html_file.write("			<div class=\"chip_bulk\">\n")


				page_html_file.write("				<table class=\"infotable2\">\n")
				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td width=\"150px\"><b>Item</b></td>\n")
				page_html_file.write("						<td width=\"550px\" ><b>Description</b></td>\n")
				page_html_file.write("					</tr>\n")

				# titles

				for title_id in cart['title_id_values']:
					self.title_object.load_title(title_id)

					language_id, language =  self.title_object.ask_for_language( self.title_object.title['language_id'])

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Title (" + language + ")</td>\n")
					if self.title_object.title['translation']:
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "(" +  self.title_object.title['translation'] + ")</td>\n")
					else: 
						page_html_file.write("						<td>" +  self.title_object.title['title'] + "</td>\n")
					page_html_file.write("					</tr>\n")

				# region value
				self.shared_object.load_region(cart['region_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Region</td>\n")
				page_html_file.write("						<td>" + self.shared_object.region_selection['region'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# country value

				self.shared_object.load_country(cart['country_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Country</td>\n")
				page_html_file.write("						<td>" + self.shared_object.country_selection['country'] + " (" + self.shared_object.country_selection['region_code'] + ")</td>\n")
				page_html_file.write("					</tr>\n")

				# location manufactured
				
				self.shared_object.load_manufacturing_location(cart['manufacturing_id'])

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Manufacturing location</td>\n")
				page_html_file.write("						<td>" + self.shared_object.manufacturing_location_selection['manufacturing_location'] + "</td>\n")
				page_html_file.write("					</tr>\n")

				# serial code

				page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
				page_html_file.write("						<td>Serial code</td>\n")
				page_html_file.write("						<td>" + cart['serial_code'] +  "</td>\n")
				page_html_file.write("					</tr>\n")

				# companies
				
				for company_id in cart['company_id_values']:

					self.shared_object.load_company(company_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Company</td>\n")
					page_html_file.write("						<td>" +  self.shared_object.company_selection['company'] +  "</td>\n")
					page_html_file.write("					</tr>\n")


				# contributors

				for contributor_id in  cart['contributors']:
					self.contributors_object.load_contributor_id(contributor_id)

					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Contributor</td>\n")
					page_html_file.write("						<td>" +  self.contributors_object.return_link() +  "</td>\n")
					page_html_file.write("					</tr>\n")

				#notes


				if cart['notes']:
					page_html_file.write("					<tr class=\"row" + str(next(myIterator2)) + "\">\n")
					page_html_file.write("						<td>Notes</td>\n")
					page_html_file.write("						<td>" + cart['notes'] +  "</td>\n")
					page_html_file.write("					</tr>\n")

				page_html_file.write("				</table>\n")

				page_html_file.write("			</div>\n")
				page_html_file.write("		</div>\n")

				
				page_html_file.close()

				# move to site


				sc_file = str(cart['cart_id']) + ".html"
				self.file_object.move_html_to_site(temp_file, sc_file, "carts")

		# finish off making the table

		html_file.write("</table>\n")

		html_file.write("		</div>\n")

		html_file.write("<p><i>Do you have a label variant that is not listed above or a better quality copy?</i> Read the <a href=\"article.php?id=1097\">Submission guidelines</a> for cartridge label scans.</p>\n")
		html_file.close()
		self.file_object.move_html_to_site(self.table_temp_file, self.table_file)

		check_file.close()


		self.file_object.move_html_to_site(self.check_temp_file, self.check_file)
# let's run this

if __name__ == "__main__":

	print("Enter the entry ID number")

	cart_object = carts()

	print("Options:")
	print("1) add cart entry")
	print("2) edit cart entry")
	print("3) Create HTML files for cart information")
	print("4) Print carts in database")
	#print("3) create HTML pages (this also runs after running 1 and 2)")

	entry = input(">>> ")

	try:
		entry = int(entry)

		if(entry == 1):
			cart_object.add_cart()
		elif(entry==2):
			cart_object.edit_cart()
		elif(entry==3):
			cart_object.create_html_files()
		elif(entry==4):
			cart_object.print_cart_info()
		else:
			print("fail")

	except ValueError:
		print("fail")
