#!/usr/bin/env python3

import sys
import os


if len(sys.argv) > 2 or len(sys.argv) < 2:
	print("Warning: you must enter the four digit id code \nof the game/article you are interested in")
	sys.exit()

if len(sys.argv[1]) > 4 or len(sys.argv[1]) < 4:
	print("Warning: you must enter the four digit id code \nof the game/article you are interested in")
	sys.exit()

game_id = sys.argv[1]


# detect if the game entry is there
game_path = "data_files/" + game_id[0] + '/' + game_id[1] + '/' + game_id[2] + '/' + game_id



if not os.path.isdir(game_path):
	print("This entry does not exist yet, would you like to add it?\n")

	entry = "f"

	while (entry != "y" and entry != "n"):
		entry = input('(y/n): ')


	if(entry == "n"):
		print("exiting")
		sys.exit()

	print("making game directory")

	os.makedirs(game_path)

game_info_file = game_path + "/game_info.json"
pcb_file =  game_path + "/pcb.json"
cart_file =  game_path + "/cart.json"
box_file =  game_path + "/box.json"
rom_image_file =  game_path + "/rom_image.json"
prototype_file =  game_path + "/prototype.json"
article_file =  game_path + "/article.json"
bibliography_file =  game_path + "/bibliography.json"
screenshots_file = game_path + "/screenshots.json"



looping = True

while (looping):


	is_game = False
	if os.path.isfile(game_info_file):
		is_game = True

	is_pcb = False
	if os.path.isfile(pcb_file):
		is_pcb = True

	is_cart = False
	if os.path.isfile(cart_file):
		is_cart = True

	is_box = False
	if os.path.isfile(box_file):
		is_box = True

	is_rom = False
	if os.path.isfile(rom_image_file):
		is_rom = True

	is_prototype = False
	if os.path.isfile(prototype_file):
		is_prototype = True

	is_article = False
	if os.path.isfile(article_file):
		is_article = True

	is_bibliography = False
	if os.path.isfile(bibliography_file):
		is_bibliography = True

	is_screenshots = False
	if os.path.isfile(screenshots_file):
		is_screenshots = True


	print("File status:")
	print("1) Game information: %s" % str(is_game))
	print("2) PCB:              %s" % str(is_pcb))
	print("3) Carts:            %s" % str(is_cart))
	print("4) Box:              %s" % str(is_box))
	print("5) ROM Image info:   %s" % str(is_rom))
	print("6) Prototypes:       %s" % str(is_prototype))
	print("7) Articles:         %s" % str(is_article))
	print("8) Bibliography:     %s" % str(is_bibliography))
	print("9) Screenshots:      %s" % str(is_screenshots))

	print("If you would like to add one of these files, type the number. \"q\" to quit")
	entry = input('>>: ')

	if(entry == "q"):
		looping = False
	else:


		if(entry == "1" and not is_game):
			file = open(game_info_file, 'w+')
			file.close()
		elif(entry == "2" and not is_pcb):
			file = open(pcb_file, 'w+')
			file.close()
		elif(entry == "3" and not is_cart):
			file = open(cart_file, 'w+')
			file.close()
		elif(entry == "4" and not is_box):
			file = open(box_file, 'w+')
			file.close()
		elif(entry == "5" and not is_rom):
			file = open(rom_image_file, 'w+')
			file.close()
		elif(entry == "6" and not is_prototype):
			file = open(prototype_file, 'w+')
			file.close()
		elif(entry == "7" and not is_article):
			file = open(article_file, 'w+')
			file.close()
		elif(entry == "8" and not is_bibliography):
			file = open(bibliography_file, 'w+')
			file.close()
		elif(entry == "9" and not is_screenshots):
			file = open(screenshots_file, 'w+')
			file.close()
		else:
			print("invalid entry")



